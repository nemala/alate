/*
 * GetTimeString.h
 *
 *  Created on: Oct 8, 2018
 *      Author: dave
 */

#ifndef _ALATE_UTILS_PRINT_TIME_H_
#define _ALATE_UTILS_PRINT_TIME_H_

#include <boost/date_time/posix_time/posix_time.hpp>

namespace AlateUtils {

inline std::string GetTimeString()
{
	boost::posix_time::ptime rawTime(boost::date_time::microsec_clock<boost::posix_time::ptime>::universal_time());
	std::string timeStamp(boost::posix_time::to_simple_string(rawTime));
	return timeStamp + ": ";
}

}/* namespace AlateUtils */

#endif /* _ALATE_UTILS_PRINT_TIME_H_ */

/*
 * GetDistance.h
 *
 *  Created on: Nov 28, 2021
 *      Author: dave
 */

#ifndef _ALATE_UTILS_GET_DISTANCE_H_
#define _ALATE_UTILS_GET_DISTANCE_H_

#include <math.h>

namespace AlateUtils {

// Return the absolute value of dX (1 dimension distance)
inline double GetDistance(double dX)
{
	return fabs(dX);
}

// Return the square root of dX^2+dY^2 (2 dimension distance)
inline double GetDistance(double dX, double dY)
{
	return sqrt(pow(dX,2) + pow(dY,2));
}

// Return the square root of dX^2+dY^2+dZ^2 (3 dimension distance)
inline double GetDistance(double dX, double dY, double dZ)
{
	return sqrt(pow(dX,2) + pow(dY,2) + pow(dZ,2));
}

}/* namespace AlateUtils */

#endif /* _ALATE_UTILS_GET_DISTANCE_H_ */

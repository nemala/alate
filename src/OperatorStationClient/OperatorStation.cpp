/*
 * OperatorStation.cpp
 *
 *  Created on: Mar 21, 2018
 *      Author: dave
 */

#include "OperatorStation.h"
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <StringMessage.h>
#include <GetTimeString.h>
#include <OpMessage.h>
#include <FloatMessage.h>

//--------------------------------------------------------------------------------

OperatorStation::OperatorStation(std::string strServer, std::string strPort, int nUid,
		unsigned int unAllowedTimeouts, NeMALA::Publisher* pPublisherOperatorCommand,
		NeMALA::Publisher* pPublisherDirectionCommand, NeMALA::Publisher* pPublisherPayload) :
			m_strServer(strServer), m_strPort(strPort), m_nUid(nUid), m_work(m_io_service), m_resolver(m_io_service),
			m_socket(m_io_service), m_unCounterHttpFailedLocks(0), m_unAllowedTimeouts(unAllowedTimeouts),
			m_pPublisherOperatorCommand(pPublisherOperatorCommand), m_pPublisherDirectionCommand(pPublisherDirectionCommand),
			m_pPublisherPayload(pPublisherPayload)
{
	m_pThreadHttp  = new boost::thread(boost::bind(&boost::asio::io_service::run,&m_io_service));

	if(NULL == m_pThreadHttp)
	{
		throw std::runtime_error("OperatorStation:: could not create http thread");
	}
}

//--------------------------------------------------------------------------------

OperatorStation::~OperatorStation()
{
	m_io_service.stop();
	if(m_pThreadHttp)
	{
		m_pThreadHttp->join();
		std::cout << "OperatorStation: http thread joined." << std::endl;
		delete m_pThreadHttp;
		m_pThreadHttp = NULL;
	}
}

//--------------------------------------------------------------------------------

void OperatorStation::SetMcState(McMessage msg)
{
	m_mutexAgentStatusMessage.lock();
	m_AgentStatusMessage.SetMcState(msg.GetName());
	m_mutexAgentStatusMessage.unlock();
	// if Error state then lock the http and publish.
	if (McMessage::McStateError == msg.GetValue())
	{
		m_mutexHttpRequest.lock();
		m_io_service.post(boost::bind(&OperatorStation::Publish, this));
		m_mutexHttpRequest.unlock();
	}
}

//--------------------------------------------------------------------------------

void OperatorStation::SetHlcState(std::string strName)
{
	m_mutexAgentStatusMessage.lock();
	m_AgentStatusMessage.SetHlcState(strName);
	m_mutexAgentStatusMessage.unlock();
}

//--------------------------------------------------------------------------------

void OperatorStation::SetLlcMessage(std::string strMessage)
{
	m_mutexAgentStatusMessage.lock();
	m_AgentStatusMessage.SetLlcMessage(strMessage);
	m_mutexAgentStatusMessage.unlock();
}

//--------------------------------------------------------------------------------

void OperatorStation::SetLlcStatus(LlcStatusMessage::LlcStateStruct stLlcStatus)
{
	//std::cout << "OperatorStation::SetLlcStatus" << std::endl;
	m_mutexAgentStatusMessage.lock();
	m_AgentStatusMessage.SetLlcStatus(stLlcStatus);
	m_mutexAgentStatusMessage.unlock();
	//std::cout << "OperatorStation::SetLlcStatus Trying Lock" << std::endl;
	if (m_mutexHttpRequest.try_lock())
	{
		//std::cout << "OperatorStation::SetLlcStatus POSTing" << std::endl;
		m_io_service.post(boost::bind(&OperatorStation::Publish, this));
		m_mutexHttpRequest.unlock();
		m_unCounterHttpFailedLocks = 0;
	}
	else
	{
		//std::cout << "OperatorStation::SetLlcStatus Not Posting" << std::endl;
		if(++m_unCounterHttpFailedLocks > m_unAllowedTimeouts)
		{
			m_unCounterHttpFailedLocks = 0;
			GoHome();
			std::cout << AlateUtils::GetTimeString() << "OperatorStation::SetLlcStatus - " << m_unAllowedTimeouts << " Timeouts - GO HOME!!" << std::endl;
		}
	}
}

//--------------------------------------------------------------------------------

void OperatorStation::SetFeedback(std::string strFeedback)
{
	m_mutexAgentStatusMessage.lock();
	m_AgentStatusMessage.SetFeedback(strFeedback);
	m_mutexAgentStatusMessage.unlock();
}

//--------------------------------------------------------------------------------

void OperatorStation::Publish()
{
	m_mutexHttpRequest.lock();
	boost::posix_time::ptime rawTime(boost::date_time::microsec_clock<boost::posix_time::ptime>::universal_time());
	std::string timeStamp(boost::posix_time::to_iso_extended_string(rawTime));
	m_mutexAgentStatusMessage.lock();
	m_AgentStatusMessage.SetHeader(timeStamp,m_nUid);
	std::string strContent(m_AgentStatusMessage.GetAsStringJson());
	m_mutexAgentStatusMessage.unlock();

	m_strRequest = "POST /agent/ HTTP/1.1\r\nHost: ";
	m_strRequest += m_strServer +  "\r\nContent-Type: application/x-www-form-urlencoded \r\nContent-Length: ";
	m_strRequest += boost::lexical_cast<std::string>(strContent.length() + 5);
	m_strRequest += "\r\nAccept: */*\r\nConnection: close\r\n\r\nroot=";
	m_strRequest += strContent;

    // Start an asynchronous resolve to translate the server and service names
    // into a list of endpoints.
    boost::asio::ip::tcp::resolver::query query(m_strServer, m_strPort);
    m_resolver.async_resolve(query,
        boost::bind(&OperatorStation::HandleResolve, this,
          boost::asio::placeholders::error,
          boost::asio::placeholders::iterator));
}

//--------------------------------------------------------------------------------

void OperatorStation::HandleResolve(const boost::system::error_code& err, boost::asio::ip::tcp::resolver::iterator endpoint_iterator)
{
	if (!err)
	{
		// Attempt a connection to each endpoint in the list until we
		// successfully establish a connection.
		boost::asio::async_connect(m_socket, endpoint_iterator, boost::bind(&OperatorStation::HandleConnect,
				this, boost::asio::placeholders::error));
	}
	else
	{
		UnlockHttp("OperatorStation::HandleResolve Error: ",err.message(),true);
	}
}

//--------------------------------------------------------------------------------

void OperatorStation::HandleConnect(const boost::system::error_code& err)
{
	if (!err)
	{
		// The connection was successful.
		std::ostream request_stream(&m_request);

	    //std::cout << "OperatorStation::HandleConnect called with:" << std::endl << m_strRequest << std::endl << std::endl;

		request_stream << m_strRequest;

		//Send the request.
		boost::asio::async_write(m_socket, m_request, boost::bind(&OperatorStation::HandleResponse,
				this, boost::asio::placeholders::error));
    }
    else
    {
    	UnlockHttp("OperatorStation::HandleConnect Error: ",err.message(),true);
    }
}

//--------------------------------------------------------------------------------

void OperatorStation::HandleResponse(const boost::system::error_code& err)
{
    if (!err)
    {
      // Read the response status line. The response_ streambuf will
      // automatically grow to accommodate the entire line. The growth may be
      // limited by passing a maximum size to the streambuf constructor.
      boost::asio::async_read_until(m_socket, m_response, "\r\n",
          boost::bind(&OperatorStation::HandleReadLine, this,
            boost::asio::placeholders::error));
    }
    else
    {
    	UnlockHttp("OperatorStation::HandleResponse Error: ",err.message(),true);
    }
}

//--------------------------------------------------------------------------------

void OperatorStation::HandleReadLine(const boost::system::error_code& err)
{
    if (!err)
    {
      // Check that response is OK.
      std::istream response_stream(&m_response);
      std::string http_version;
      response_stream >> http_version;
      unsigned int status_code;
      response_stream >> status_code;
      std::string status_message;
      std::getline(response_stream, status_message);
      if (!response_stream || http_version.substr(0, 5) != "HTTP/")
      {
    	  UnlockHttp("OperatorStation::HandleReadLine Invalid response: ",status_message,true);
      }
      else if (status_code != 200)
      {
    	  UnlockHttp("OperatorStation::HandleReadLine Response returned with status code ", std::to_string(status_code) ,true);
      }
      else
      {
    	  // Read the response headers, which are terminated by a blank line.
    	  boost::asio::async_read_until(m_socket, m_response, "\r\n\r\n",
          boost::bind(&OperatorStation::HandleReadHeaders, this, boost::asio::placeholders::error));
      }
    }
    else
    {
    	UnlockHttp("OperatorStation::HandleReadHeaders Error: ",err.message(),true);
    }
}

//--------------------------------------------------------------------------------

void OperatorStation::HandleReadHeaders(const boost::system::error_code& err)
{
    if (!err)
    {
      // Process the response headers.
      std::istream response_stream(&m_response);
      std::string header;
      while (std::getline(response_stream, header) && header != "\r")
      {
    	//  std::cout << "OperatorStation::HandleReadHeaders: " << header << "\n";
      }

      // Start reading remaining data until EOF.
      boost::asio::async_read(m_socket, m_response,
          boost::asio::transfer_at_least(1),
          boost::bind(&OperatorStation::HandleReadContent, this,
            boost::asio::placeholders::error));
    }
    else
    {
    	UnlockHttp("OperatorStation::HandleReadHeaders Error: ",err.message(),true);
    }
}

//--------------------------------------------------------------------------------

void OperatorStation::HandleReadContent(const boost::system::error_code& err)
{
	boost::property_tree::ptree pt;
	std::istream incomingMsg(&m_response);
	int nUid;
	//std::cout << "OperatorStation::HandleReadContent" <<std::endl;
    if (!err)
    {
    	//std::cout << "OperatorStation::HandleReadContent Continuing, : " << m_response.size() <<std::endl;
      // Continue reading remaining data until EOF.
      boost::asio::async_read(m_socket, m_response,
          boost::asio::transfer_at_least(1),
          boost::bind(&OperatorStation::HandleReadContent, this,
            boost::asio::placeholders::error));
    }
    else
    {
    	//std::cout << "OperatorStation::HandleReadContent Stopping" <<std::endl;
    	if (err != boost::asio::error::eof)
    	{
    		UnlockHttp("OperatorStation::HandleReadContent Error: ",err.message(),true);
    	}
    	else // (err == boost::asio::error::eof)
    	{
    		// Write all of the data that has been read so far.
    		boost::property_tree::read_json(incomingMsg, pt);
    		nUid = pt.get<int>("id");
    		if (nUid != m_nUid)
    		{
    			m_nUid = nUid;
    			std::cout << "OperatorStation::HandleReadContent - New Agent ID: " << m_nUid << std::endl;
    		}
    		FloatMessage msgDirection(-1);
    		OpMessage msgRc((OpMessage::OpCommandEnum)pt.get<double>("command"));
    		switch (msgRc.GetValue())
    		{
    		case OpMessage::OpCommandTakeoff:
    		case OpMessage::OpCommandLand:
    		case OpMessage::OpCommandGoHome:
    			m_pPublisherOperatorCommand->Publish(msgRc);
    			break;
    		case OpMessage::OpCommandSetDirection:
    			msgDirection.SetValue(pt.get<double>("direction"));
    			break;
    		case OpMessage::OpCommandNone:
    		default:
    			break;
    		}
    		m_pPublisherDirectionCommand->Publish(msgDirection);

    		boost::property_tree::ptree ptPayload = pt.get_child("payload");
    		std::ostringstream os;
    		boost::property_tree::json_parser::write_json(os, ptPayload, false);
    		StringMessage msgPayload(os.str());

    		m_pPublisherPayload->Publish(msgPayload);
    		UnlockHttp("OperatorStation::HandleReadContent SUCCESS!!!! ",err.message(),false,false);
    	}
    }
}

//--------------------------------------------------------------------------------

void OperatorStation::UnlockHttp(std::string strMessage, std::string strErr, bool bVerbose, bool bCritical)
{
	if(bCritical)
	{
		GoHome();
		std::cout << AlateUtils::GetTimeString() << "OperatorStation::UnlockHttp - GO HOME!!" << std::endl;
	}
	if (bVerbose)
	{
		std::cout << AlateUtils::GetTimeString() << strMessage << strErr << std::endl;
	}
	m_mutexHttpRequest.unlock();
}

//--------------------------------------------------------------------------------

void OperatorStation::GoHome()
{
	OpMessage msg(OpMessage::OpCommandGoHome);
	m_pPublisherOperatorCommand->Publish(msg);
}

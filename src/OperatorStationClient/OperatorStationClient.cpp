/*
 * OperatorStationClient.cpp
 *
 *  Created on: Mar 21, 2018
 *      Author: dave
 */

#include <Config.h>
#include "Handlers/McHandler.h"
#include "Handlers/HlcHandler.h"
#include "Handlers/LlcMsgHandler.h"
#include "Handlers/FeedbackHandler.h"
#include "Handlers/LlcStatusHandler.h"
#include <NeMALA/Dispatcher.h>
#include <NeMALA/Publisher.h>
#include <iostream>
#include <boost/property_tree/json_parser.hpp>
#include "OperatorStation.h"

int main (int argc, char *argv[])
{
	boost::property_tree::ptree pt;
	bool bFailed(false);
	NeMALA::Dispatcher* pDispatcher(NULL);
	NeMALA::Publisher* pPublisherOperatorCommand(NULL);
	NeMALA::Publisher* pPublisherDirectionCommand(NULL);
	NeMALA::Publisher* pPublisherOperatorPayload(NULL);
	McHandler* pMcHandler(NULL);
	HlcHandler* pHlcHandler(NULL);
	LlcMsgHandler* pLlcMsgHandler(NULL);
	FeedbackHandler* pFeedbackHandler(NULL);
	LlcStatusHandler* pLlcStatusHandler(NULL);
	OperatorStation* pOpStation(NULL);
	std::string strProxyName;

	std::cout << std::endl << argv[0] << std::endl;

	if ( 3 != argc)
	{
		std::cout << "Usage:" << std::endl;
		std::cout << argv[0] << " proxy_name config.json" << std::endl;
		std::cout << "See documentation." << std::endl;

		bFailed = true;
	}
	else
	{
		strProxyName = "proxies.";
		strProxyName += argv[1];
	}

	if (!bFailed)
	{
		// Check input
		try
		{
			boost::property_tree::read_json(argv[2],pt);
		}
		catch (std::exception& e)
		{
			std::cerr << "Error in OSC input: " << e.what() << std::endl;
			bFailed = true;
		}
	}

	// Set up Dispatcher
	if (!bFailed)
	{
		try
		{
			pDispatcher = new NeMALA::Dispatcher(NEMALA_ALATE_VERSION_MAJOR, NEMALA_ALATE_VERSION_MINOR, pt.get<unsigned int>("nodes.operator_station_client"),
								pt.get<std::string>(strProxyName + ".subscribers"));
		}
		catch (std::exception& e)
		{
			std::cerr << "Failed to initialize OSC: " << e.what() << std::endl;
			bFailed = true;
		}
		if (NULL == pDispatcher)
		{
			std::cerr << "Failed to Create OSC dispatcher." << std::endl;
			bFailed = true;
		}
	}

	// Set up Publishers
	if (!bFailed)
	{
		pDispatcher->PrintVersion();

		std::string strEndPoint(pt.get<std::string>(strProxyName + ".publishers"));
		unsigned int unTopicOpCmd(pt.get<unsigned int>("topics.operator_command"));
		unsigned int unTopicDirectionCommand(pt.get<unsigned int>("topics.direction_command"));
		unsigned int unTopicOperatorPayload(pt.get<unsigned int>("topics.operator_payload"));

		try
		{
			pPublisherOperatorCommand = new NeMALA::Publisher(strEndPoint.c_str(),unTopicOpCmd);
			pPublisherDirectionCommand = new NeMALA::Publisher(strEndPoint.c_str(),unTopicDirectionCommand);
			pPublisherOperatorPayload = new NeMALA::Publisher(strEndPoint.c_str(),unTopicOperatorPayload);
		}
		catch (std::exception& e)
		{
			std::cerr << "OSC could not create Publishers(" << strEndPoint
					<< " , " << unTopicOpCmd
					<< " , " << unTopicDirectionCommand
					<< " , " << unTopicOperatorPayload
					<< "): "
					<< e.what() << std::endl;
			bFailed = true;
		}

		if ((NULL == pPublisherOperatorCommand)||(NULL == pPublisherDirectionCommand) || (NULL == pPublisherOperatorPayload))
		{
			std::cerr << "OSC failed to Create Publishers." << std::endl;
			bFailed = true;
		}
	}

	// Set up http client
	if (!bFailed)
	{
		int nUid(pt.get<int>("operator_station.client_id"));
		std::string strServer(pt.get<std::string>("operator_station.server"));
		std::string strPort(pt.get<std::string>("operator_station.port"));
		unsigned int unAllowedTimeouts(pt.get<unsigned int>("operator_station.allowed_timeouts"));

		try
		{
			pOpStation = new OperatorStation(strServer, strPort, nUid, unAllowedTimeouts, pPublisherOperatorCommand,
					pPublisherDirectionCommand, pPublisherOperatorPayload);
		}
		catch (std::exception& e)
		{
			std::cerr << "OSC could not create Http Client(" << strServer
					<< ":" << strPort
					<< " , " << unAllowedTimeouts
					<< "): "
					<< e.what() << std::endl;
			bFailed = true;
		}
		if (NULL == pOpStation)
		{
			std::cerr << "OSC failed to Create Http Client." << std::endl;
			bFailed = true;
		}
	}

	// Set up Handlers
	if (!bFailed)
	{
		pMcHandler = new McHandler(pOpStation);
		pHlcHandler = new HlcHandler(pOpStation);
		pLlcMsgHandler = new LlcMsgHandler(pOpStation);
		pFeedbackHandler = new FeedbackHandler(pOpStation);
		pLlcStatusHandler = new LlcStatusHandler(pOpStation);


		if ((NULL == pMcHandler)
			||(NULL == pHlcHandler)
			||(NULL == pLlcMsgHandler)
			||(NULL == pFeedbackHandler)
			||(NULL == pLlcStatusHandler)
			)
		{
			std::cerr << "OSC Failed to Create Handlers." << std::endl;
			bFailed = true;
		}

	}

	if (!bFailed)
	{
		// Add the publishers to the dispatcher
		pDispatcher->AddPublisher(pPublisherOperatorCommand);
		pDispatcher->AddPublisher(pPublisherDirectionCommand);
		pDispatcher->AddPublisher(pPublisherOperatorPayload);
		// Add the handlers to the dispatcher
		pDispatcher->AddHandler(pt.get<unsigned int>("topics.mcm_state"),pMcHandler);
		pDispatcher->AddHandler(pt.get<unsigned int>("topics.hlc_state"),pHlcHandler);
		pDispatcher->AddHandler(pt.get<unsigned int>("topics.hlc_error"),pLlcMsgHandler);
		pDispatcher->AddHandler(pt.get<unsigned int>("topics.feedback"),pFeedbackHandler);
		pDispatcher->AddHandler(pt.get<unsigned int>("topics.hlc_telemetry"),pLlcStatusHandler);

		try
		{
			pDispatcher->Dispatch();
		}
		catch (std::exception& e)
		{
			std::cerr << "Error in OSC: " << e.what() << std::endl;
		}

		delete pMcHandler;
		delete pHlcHandler;
		delete pLlcMsgHandler;
		delete pFeedbackHandler;
		delete pLlcStatusHandler;
		delete pOpStation;
		delete pPublisherOperatorCommand;
		delete pPublisherDirectionCommand;
		delete pPublisherOperatorPayload;
		delete pDispatcher;

		std::cout << "OSC shutting down" << std::endl;
	}

    return 0;
}

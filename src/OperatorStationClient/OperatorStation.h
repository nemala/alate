/*
 * OperatorStation.h
 *
 *  Created on: Mar 21, 2018
 *      Author: dave
 */

#ifndef OPERATORSTATIONCLIENT_OPERATORSTATION_H_
#define OPERATORSTATIONCLIENT_OPERATORSTATION_H_

#include <NeMALA/Publisher.h>
#include <AgentStatusMessage.h>
#include <boost/thread/thread.hpp>
#include <boost/asio.hpp>
#include <string>
#include <mutex>

/*
 * As far as the agent is concerned, this is the operator station.
 */
class OperatorStation
{
public:

	//-------------- Methods --------------------

	OperatorStation(std::string strServer, std::string strPort, int nUid,
						unsigned int unAllowedTimeouts, NeMALA::Publisher* pPublisherOperatorCommand,
						NeMALA::Publisher* pPublisherDirectionCommand,
						NeMALA::Publisher* pPublisherPayload);
	~OperatorStation();

	void SetMcState(const McMessage msg);
	void SetHlcState(std::string strName);
	void SetLlcMessage(std::string strMessage);
	void SetLlcStatus(LlcStatusMessage::LlcStateStruct stLlcStatus);
	void SetFeedback(std::string strFeedback);

private:

	//-------------- Variables --------------------

	std::string m_strServer;
	std::string m_strPort;
	int m_nUid;
	AgentStatusMessage m_AgentStatusMessage;

	boost::asio::io_service m_io_service;
	boost::asio::io_service::work m_work;
	boost::thread* m_pThreadHttp;

	std::string m_strRequest;
	boost::asio::ip::tcp::resolver m_resolver;
	boost::asio::ip::tcp::socket m_socket;
	boost::asio::streambuf m_request;
	boost::asio::streambuf m_response;

	std::mutex m_mutexAgentStatusMessage;
	std::mutex m_mutexHttpRequest;
	unsigned int m_unCounterHttpFailedLocks;
	unsigned int m_unAllowedTimeouts;

	NeMALA::Publisher* m_pPublisherOperatorCommand;
	NeMALA::Publisher* m_pPublisherDirectionCommand;
	NeMALA::Publisher* m_pPublisherPayload;

	//-------------- Methods --------------------
	void Publish();
	void HandleResolve(const boost::system::error_code& err, boost::asio::ip::tcp::resolver::iterator endpoint_iterator);
	void HandleConnect(const boost::system::error_code& err);
	void HandleResponse(const boost::system::error_code& err);
	void HandleReadLine(const boost::system::error_code& err);
	void HandleReadHeaders(const boost::system::error_code& err);
	void HandleReadContent(const boost::system::error_code& err);

	void UnlockHttp(std::string strMessage, std::string strErr, bool bVerbose, bool bCritical=true);
	void GoHome();
};

#endif /* OPERATORSTATIONCLIENT_OPERATORSTATION_H_ */

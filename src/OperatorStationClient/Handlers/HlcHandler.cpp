#include "HlcHandler.h"
#include <HlcStateMessage.h>

//--------------------------------------------------------------------------------

void HlcHandler::Handle(NeMALA::Proptree pt)
{
	HlcStateMessage msg(pt);
	m_pOperatorStation->SetHlcState(msg.GetName());
}

//--------------------------------------------------------------------------------


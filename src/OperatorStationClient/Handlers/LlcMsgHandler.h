/*
 * LlcMsgHandler.h
 *
 *  Created on: Mar 26, 2018
 *      Author: dave
 */

#ifndef _OSC_LLC_MESSAGE_HANDLER_H_
#define _OSC_LLC_MESSAGE_HANDLER_H_

#include <NeMALA/Handler.h>
#include "../OperatorStation.h"

/*
 * Handle LLC error messages.
 */
class LlcMsgHandler : public NeMALA::Handler
{
public:

	//-------------- Methods --------------------
	LlcMsgHandler(OperatorStation* pOperatorStation):m_pOperatorStation(pOperatorStation){}
	~LlcMsgHandler(){}

	/*
	 * Handle a message.
	 */
	void Handle(NeMALA::Proptree pt);

private:
	OperatorStation* m_pOperatorStation;
};

#endif // _OSC_LLC_MESSAGE_HANDLER_H_

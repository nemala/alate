/*
 * FeedbackHandler.h
 *
 *  Created on: 6 Jun 2021
 *      Author: dave
 */

#ifndef _OSC_FEEDBACK_HANDLER_H_
#define _OSC_FEEDBACK_HANDLER_H_

#include <NeMALA/Handler.h>
#include "../OperatorStation.h"

/*
 * Handle Operator Feedback
 */
class FeedbackHandler : public NeMALA::Handler
{
public:

	//-------------- Methods --------------------
	FeedbackHandler(OperatorStation* pOperatorStation):m_pOperatorStation(pOperatorStation){}
	~FeedbackHandler(){}

	/*
	 * Handle a message.
	 */
	void Handle(NeMALA::Proptree pt);

private:
	OperatorStation* m_pOperatorStation;
};

#endif /* _OSC_FEEDBACK_HANDLER_H_ */

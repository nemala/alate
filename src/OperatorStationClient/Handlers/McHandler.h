/*
 * McHandler.h
 *
 *  Created on: Mar 21, 2018
 *      Author: dave
 */

#ifndef _OSC_MC_HANDLER_H_
#define _OSC_MC_HANDLER_H_

#include <NeMALA/Handler.h>
#include "../OperatorStation.h"

/*
 * Handle MCM State messages.
 */
class McHandler : public NeMALA::Handler
{
public:

	//-------------- Methods --------------------
	McHandler(OperatorStation* pOperatorStation):m_pOperatorStation(pOperatorStation){}
	~McHandler(){}

	/*
	 * Handle a message.
	 */
	void Handle(NeMALA::Proptree pt);

private:
	OperatorStation* m_pOperatorStation;
};

#endif // _OSC_MC_HANDLER_H_

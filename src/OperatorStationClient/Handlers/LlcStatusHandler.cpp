#include "LlcStatusHandler.h"
#include <LlcStatusMessage.h>

//--------------------------------------------------------------------------------

void LlcStatusHandler::Handle(NeMALA::Proptree pt)
{
	LlcStatusMessage msg(pt);
	m_pOperatorStation->SetLlcStatus(msg.GetLlcStatus());
}

//--------------------------------------------------------------------------------


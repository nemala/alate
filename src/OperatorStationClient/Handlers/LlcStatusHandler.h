/*
 * LlcStatusHandler.h
 *
 *  Created on: Mar 26, 2018
 *      Author: dave
 */

#ifndef _OSC_LLC_STATUS_HANDLER_H_
#define _OSC_LLC_STATUS_HANDLER_H_

#include <NeMALA/Handler.h>
#include "../OperatorStation.h"

/*
 * Handle LLC status messages.
 */
class LlcStatusHandler : public NeMALA::Handler
{
public:

	//-------------- Methods --------------------
	LlcStatusHandler(OperatorStation* pOperatorStation):m_pOperatorStation(pOperatorStation){}
	~LlcStatusHandler(){}

	/*
	 * Handle a message.
	 */
	void Handle(NeMALA::Proptree pt);

private:
	OperatorStation* m_pOperatorStation;
};

#endif // _OSC_LLC_STATUS_HANDLER_H_

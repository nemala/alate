/*
 * HlcHandler.h
 *
 *  Created on: Mar 26, 2018
 *      Author: dave
 */

#ifndef _OSC_HLC_HANDLER_H_
#define _OSC_HLC_HANDLER_H_

#include <NeMALA/Handler.h>
#include "../OperatorStation.h"

/*
 * Handle HLC State messages.
 */
class HlcHandler : public NeMALA::Handler
{
public:

	//-------------- Methods --------------------
	HlcHandler(OperatorStation* pOperatorStation):m_pOperatorStation(pOperatorStation){}
	~HlcHandler(){}

	/*
	 * Handle a message.
	 */
	void Handle(NeMALA::Proptree pt);

private:
	OperatorStation* m_pOperatorStation;
};

#endif // _OSC_HLC_HANDLER_H_

#include "LlcMsgHandler.h"
#include <StringMessage.h>

//--------------------------------------------------------------------------------

void LlcMsgHandler::Handle(NeMALA::Proptree pt)
{
	StringMessage msg(pt);
	m_pOperatorStation->SetLlcMessage(msg.GetContent());
}

//--------------------------------------------------------------------------------


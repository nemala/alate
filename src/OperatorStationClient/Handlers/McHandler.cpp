#include "McHandler.h"
#include <McMessage.h>

//--------------------------------------------------------------------------------

void McHandler::Handle(NeMALA::Proptree pt)
{
	McMessage msg(pt);
	m_pOperatorStation->SetMcState(msg);
}

//--------------------------------------------------------------------------------


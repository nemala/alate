/*
 * FeedbackHandler.cpp
 *
 *  Created on: 6 Jun 2021
 *      Author: dave
 */

#include "FeedbackHandler.h"
#include <StringMessage.h>
#include <boost/property_tree/json_parser.hpp>
#include <iostream>

//--------------------------------------------------------------------------------

void FeedbackHandler::Handle(NeMALA::Proptree pt)
{
	boost::property_tree::ptree ptPayload;
	// Build a message from the property tree received.
	StringMessage msg(pt);
	m_pOperatorStation->SetFeedback(msg.GetContent());
}

//--------------------------------------------------------------------------------

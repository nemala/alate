cmake_minimum_required (VERSION 3.3)
project (MissionControl)

################################# Targets #############################

add_executable(mc MissionControl.cpp MissionControlStateMachine.cpp ${PROJECT_SOURCE_DIR}/Handlers/OpHandler.cpp ${PROJECT_SOURCE_DIR}/Handlers/HlcStateHandler.cpp)
target_link_libraries (mc ${EXTRA_LIBS})

#########################################################################

install (TARGETS mc DESTINATION .)

#########################################################################


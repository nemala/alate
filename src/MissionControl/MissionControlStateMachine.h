/*
 * MissionControlStateMachine.h
 *
 *  Created on: Jul 13, 2017
 *      Author: dave
 */

#ifndef _MISSION_CONTROL_STATE_MACHINE_H_
#define _MISSION_CONTROL_STATE_MACHINE_H_

#include <boost/msm/back/state_machine.hpp>
#include <boost/msm/front/state_machine_def.hpp>
#include <boost/msm/front/functor_row.hpp>
#include <NeMALA/Publisher.h>
#include <McMessage.h>
#include <HlcStateMessage.h>
#include <GetTimeString.h>
#include <OpMessage.h>

#include <iostream>


/*
 * A class defining a state machine for the Mission Control Module (MCM)
 */

class MissionControlStateMachine
{
public:

	// ----------------------------------- Procedures -------------------------------------

	MissionControlStateMachine(NeMALA::Publisher* pPublisher);
	~MissionControlStateMachine(){m_pBackend->stop();}

	void HandleOpEvent(OpMessage::OpCommandEnum eCommand);
	void HandleHlcStateEvent(HlcStateMessage::HlcStateEnum eState);

	// ------------------------ Constants and Type Declarations ---------------------------

	/*
	 * All states in the MCM state machine publish the current state upon entry
	 */
	class MissionControlState : public boost::msm::front::state<>
    {
		public:

			// -------------------- Procedures -------------------------------


			MissionControlState():m_eState(McMessage::McStateNone){}
			virtual ~MissionControlState(){}

			void SetState(McMessage::McStateEnum eState){m_eState = eState;}

			template <class Event,class FSM>
			void on_entry(Event const&,FSM& fsm)
			{
				McMessage msg(m_eState);
				std::cout << AlateUtils::GetTimeString() << "MissionControl entering state: " << msg.GetName() << std::endl;
				fsm.Publish(msg);
			}

			template <class Event,class FSM>
			void on_exit(Event const&,FSM& )
			{
				McMessage msg(m_eState);
				std::cout << AlateUtils::GetTimeString() << "MissionControl exiting state: " << msg.GetName() << std::endl;
			}

    	private:

			// -------------------- Variables -------------------------------

			McMessage::McStateEnum m_eState;
    };


	////////////////////////////////////////////////////////////////////
	// ------------------------ Events -------------------------------//
	////////////////////////////////////////////////////////////////////

	/*
	 * Events generated from the High Level Control State Machine
	 */

	class EventHlcState
	{
		public:
			EventHlcState(HlcStateMessage::HlcStateEnum eState):m_eState(eState){}
    		~EventHlcState(){}

    		HlcStateMessage::HlcStateEnum GetEvent() const {return m_eState;}

		private:
    		HlcStateMessage::HlcStateEnum m_eState;
	};

	/*
	 * Events generated from the Remote Control Module
	 */

    class EventRc
	{
		public:
    		EventRc(OpMessage::OpCommandEnum eCommand):m_eCommand(eCommand){}
    		~EventRc(){}

    		OpMessage::OpCommandEnum GetEvent() const {return m_eCommand;}

		private:
    		OpMessage::OpCommandEnum m_eCommand;
	};

    /*
     * A front-end definition for the FSM structure
     */

    class c_front_end : public boost::msm::front::state_machine_def<c_front_end,MissionControlState>
    {
    	public:

    		// ---------------------------- Procedures -------------------------------
    		c_front_end(NeMALA::Publisher* pPublisher):m_pPublisher(pPublisher){}
    		~c_front_end(){}

    		void Publish(NeMALA::BaseMessage &msg){m_pPublisher->Publish(msg);}

    		template <class Event,class FSM>
    		void on_entry(Event const& ,FSM&)
    		{
    			std::cout << AlateUtils::GetTimeString() << "entering: Mission Control" << std::endl;
    		}

    		template <class Event,class FSM>
    		void on_exit(Event const&,FSM& )
    		{
    			std::cout << AlateUtils::GetTimeString() << "leaving: Mission Control" << std::endl;
    		}

    		// ------------------- Constants and Type Declarations -------------------

    		////////////////////////////////////////////////////////////////////
    		// ------------------------ States -------------------------------//
    		////////////////////////////////////////////////////////////////////

    		// ------------------------------------- NotError --------------------------------------------

    		class NotError : public MissionControlState
    		{
    		public:
    			NotError(){SetState(McMessage::McStateNoError);}
    			~NotError(){}
    		};

    		// ------------------------------------- ErrorState --------------------------------------------

    		class ErrorState : public boost::msm::front::terminate_state<>
    		{
    		public:
    			ErrorState(){}
    			~ErrorState(){}

    			template <class Event,class FSM>
    			void on_entry(Event const&,FSM& fsm)
    			{
    				McMessage msg(McMessage::McStateError);
    				std::cout << AlateUtils::GetTimeString() << "MissionControl entering state: " << msg.GetName() << std::endl;
    				fsm.Publish(msg);
    			}
    		};


    		// ------------------------------------- Init --------------------------------------------

    		class Init : public MissionControlState
            {
    			public:
    				Init(){SetState(McMessage::McStateInit);}
    				~Init(){}
            };

    		// ------------------------------------- Standby --------------------------------------------

    		class Standby : public MissionControlState
            {
    			public:
    				Standby(){SetState(McMessage::McStateStandby);}
    				~Standby(){}
            };

    		// ------------------------------------- Takeoff --------------------------------------------

    		class Takeoff : public MissionControlState
            {
    			public:
    				Takeoff(){SetState(McMessage::McStateTakingoff);}
    				~Takeoff(){}
            };

    		// ------------------------------------- PerformingMission --------------------------------------------

    		class PerformingMission : public MissionControlState
            {
    			public:
    				PerformingMission(){SetState(McMessage::McStatePerformingMission);}
    				~PerformingMission(){}
            };

    		// ------------------------------------- Landing --------------------------------------------

    		class Landing : public MissionControlState
            {
    			public:
    				Landing(){SetState(McMessage::McStateLanding);}
    				~Landing(){}
            };

    		// ------------------------------------- Manual --------------------------------------------

    		class Manual : public MissionControlState
            {
    			public:
    				Manual(){SetState(McMessage::McStateManual);}
    				~Manual(){}
            };

    		// --------------------------------- Return to Launch ---------------------------------------

    		class RTL : public MissionControlState
            {
    			public:
    				RTL(){SetState(McMessage::McStateReturnToLaunch);}
    				~RTL(){}
            };

    		// -----------------------------------------------------------------------------------------

    		// boost::msm requires an initial_state to be defined
    		typedef boost::mpl::vector<Init,NotError> initial_state;

    		// -----------------------------------------------------------------------------------------

    		////////////////////////////////////////////////////////////////////
    		// ------------------------ Guard Conditions ---------------------//
    		////////////////////////////////////////////////////////////////////

    		// ---------------------------------------------------------------------------------
    		// ------------------------------------- HLC ---------------------------------------
    		// ---------------------------------------------------------------------------------

            struct IsHlcStateInit
            {
    			template <class EVT,class FSM,class SourceState,class TargetState>
    			bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
				{
    				return (HlcStateMessage::HlcStateInit == evt.GetEvent());
				}
            };

    		// ---------------------------------------------------------------------------------

            struct IsHlcStateWaitingForMc
            {
    			template <class EVT,class FSM,class SourceState,class TargetState>
    			bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
				{
    				return (HlcStateMessage::HlcStateWaitingForMc == evt.GetEvent());
				}
            };

    		// ---------------------------------------------------------------------------------

            struct IsHlcStateError
            {
    			template <class EVT,class FSM,class SourceState,class TargetState>
    			bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
				{
    				return (HlcStateMessage::HlcStateLlcDown == evt.GetEvent());
				}
            };

    		// ---------------------------------------------------------------------------------

            struct IsHlcStateReady
            {
    			template <class EVT,class FSM,class SourceState,class TargetState>
    			bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
				{
    				return (HlcStateMessage::HlcStateReady == evt.GetEvent());
				}
            };

    		// ---------------------------------------------------------------------------------

            struct IsHlcStateAirborne
            {
    			template <class EVT,class FSM,class SourceState,class TargetState>
    			bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
				{
    				return (HlcStateMessage::HlcStateAirborne == evt.GetEvent());
				}
            };

    		// ---------------------------------------------------------------------------------

            struct IsHlcStateLanding
            {
    			template <class EVT,class FSM,class SourceState,class TargetState>
    			bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
				{
    				return (HlcStateMessage::HlcStateLanding == evt.GetEvent());
				}
            };

    		// ---------------------------------------------------------------------------------

            struct IsHlcStateManual
            {
    			template <class EVT,class FSM,class SourceState,class TargetState>
    			bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
				{
    				return (HlcStateMessage::HlcStateManual == evt.GetEvent());
				}
            };

    		// ---------------------------------------------------------------------------------

            struct IsHlcStateLowBattery
            {
    			template <class EVT,class FSM,class SourceState,class TargetState>
    			bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
				{
    				return (HlcStateMessage::HlcStateLowBattery == evt.GetEvent());
				}
            };

    		// ---------------------------------------------------------------------------------

            struct IsHlcStateLandingOrLowBattery
            {
    			template <class EVT,class FSM,class SourceState,class TargetState>
    			bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
				{
    				return ((HlcStateMessage::HlcStateLanding == evt.GetEvent())||(HlcStateMessage::HlcStateLowBattery == evt.GetEvent()));
				}
            };

    		// ---------------------------------------------------------------------------------
    		// ------------------------------------- RC ----------------------------------------
    		// ---------------------------------------------------------------------------------

            struct IsRcTakeoff
            {
    			template <class EVT,class FSM,class SourceState,class TargetState>
    			bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
				{
    				return (OpMessage::OpCommandTakeoff == evt.GetEvent());
				}
            };

    		// ---------------------------------------------------------------------------------

            struct IsRcLand
            {
    			template <class EVT,class FSM,class SourceState,class TargetState>
    			bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
				{
    				return (OpMessage::OpCommandLand == evt.GetEvent());
				}
            };

    		// ---------------------------------------------------------------------------------

            struct IsRcGoHome
            {
    			template <class EVT,class FSM,class SourceState,class TargetState>
    			bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
				{
    				return (OpMessage::OpCommandGoHome == evt.GetEvent());
				}
            };

    		// ---------------------------------------------------------------------------------

            struct IsRcLandOrGoHome
            {
    			template <class EVT,class FSM,class SourceState,class TargetState>
    			bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
				{
    				return ((OpMessage::OpCommandLand == evt.GetEvent())||(OpMessage::OpCommandGoHome == evt.GetEvent()));
				}
            };

    		////////////////////////////////////////////////////////////////////
    		// ------------------------ Transition Table ---------------------//
    		////////////////////////////////////////////////////////////////////

    		// Replaces the default no-transition response.
    		template <class FSM,class Event>
    		void no_transition(Event const& e, FSM&,int state)
    		{
    			std::cout << AlateUtils::GetTimeString() << "MCM State Machine:: no transition from state " << state + 1
    					<< " on event " << typeid(e).name() << std::endl;
    		}

            // Transition Table
            struct transition_table : boost::mpl::vector<
            //					    Start    		 	Event     			  	  Next  			    Action				 	Guard
            //-----------------------------------------+--------------------+-------------------+-------------------------+--------------------+
			boost::msm::front::Row < NotError  			, EventHlcState		, ErrorState		, boost::msm::front::none , IsHlcStateError	>,
            //-----------------------------------------+--------------------+-------------------+-------------------------+--------------------+
			boost::msm::front::Row < Init 	 			, EventHlcState		, Standby 			, boost::msm::front::none , IsHlcStateReady	>,
			boost::msm::front::Row < Init 	 			, EventHlcState		, Init	 			, boost::msm::front::none , IsHlcStateInit>,
			boost::msm::front::Row < Init 	 			, EventHlcState		, Init	 			, boost::msm::front::none , IsHlcStateWaitingForMc>,
            //-----------------------------------------+--------------------+-------------------+-------------------------+--------------------+
			boost::msm::front::Row < Standby 			, EventRc			, Takeoff			, boost::msm::front::none , IsRcTakeoff >,
            //-----------------------------------------+--------------------+-------------------+-------------------------+--------------------+
			boost::msm::front::Row < Takeoff 			, EventHlcState		, Standby	 		, boost::msm::front::none , IsHlcStateReady>,
			boost::msm::front::Row < Takeoff 			, EventHlcState		, PerformingMission	, boost::msm::front::none , IsHlcStateAirborne>,
			boost::msm::front::Row < Takeoff 			, EventHlcState		, Landing			, boost::msm::front::none , IsHlcStateLandingOrLowBattery>,
			boost::msm::front::Row < Takeoff 			, EventRc			, Landing			, boost::msm::front::none , IsRcLandOrGoHome>,
			boost::msm::front::Row < Takeoff 			, EventHlcState		, Manual			, boost::msm::front::none , IsHlcStateManual>,
            //-----------------------------------------+--------------------+-------------------+-------------------------+--------------------+
			boost::msm::front::Row < PerformingMission 	, EventRc			, Landing			, boost::msm::front::none , IsRcLand >,
			boost::msm::front::Row < PerformingMission	, EventHlcState		, Manual			, boost::msm::front::none , IsHlcStateManual>,
			boost::msm::front::Row < PerformingMission	, EventHlcState		, RTL				, boost::msm::front::none , IsHlcStateLowBattery>,
			boost::msm::front::Row < PerformingMission	, EventRc			, RTL				, boost::msm::front::none , IsRcGoHome>,
            //-----------------------------------------+--------------------+-------------------+-------------------------+--------------------+
			boost::msm::front::Row < Landing 			, EventHlcState		, Standby			, boost::msm::front::none , IsHlcStateReady>,
			boost::msm::front::Row < Landing 			, EventHlcState		, Manual			, boost::msm::front::none , IsHlcStateManual>,
            //-----------------------------------------+--------------------+-------------------+-------------------------+--------------------+
			boost::msm::front::Row < RTL	 			, EventHlcState		, Standby			, boost::msm::front::none , IsHlcStateReady>,
			boost::msm::front::Row < RTL	 			, EventHlcState		, Manual			, boost::msm::front::none , IsHlcStateManual>,
            //-----------------------------------------+--------------------+-------------------+-------------------------+--------------------+
            boost::msm::front::Row < Manual 			, EventHlcState		, Standby			, boost::msm::front::none , IsHlcStateReady>
            //-----------------------------------------+--------------------+-------------------+-------------------------+--------------------+
            > {};

    	private:

			// -------------------- Variables -------------------------------

    		NeMALA::Publisher* m_pPublisher;

    };

    private:

    NeMALA::Publisher* m_pPublisher;
    boost::msm::back::state_machine<c_front_end>* m_pBackend;
};

#endif /* _MISSION_CONTROL_STATE_MACHINE_H_ */

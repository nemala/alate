/*
 * MissionControl.cpp
 *
 *  Created on: Jul 16, 2017
 *      Author: dave
 */

#include <Config.h>
#include "Handlers/HlcStateHandler.h"
#include <NeMALA/Dispatcher.h>
#include <NeMALA/Publisher.h>
#include <iostream>
#include <boost/property_tree/json_parser.hpp>
#include "Handlers/OpHandler.h"

int main (int argc, char *argv[])
{
	boost::property_tree::ptree pt;
	bool bFailed(false);
	NeMALA::Dispatcher* pDispatcher(NULL);
	NeMALA::Publisher* pPublisher(NULL);
	OpHandler* pOpHandler(NULL);
	HlcStateHandler* pHlcStateHandler(NULL);
	MissionControlStateMachine* pMcSm(NULL);
	std::string strProxyName;

	std::cout << std::endl << argv[0] << std::endl;

	if ( 3 != argc)
	{
		std::cout << "Usage: " << std::endl;
		std::cout << argv[0] << " proxy_name config.json" << std::endl;
		std::cout << "See documentation." << std::endl;

		bFailed = true;
	}
	else
	{
		strProxyName = "proxies.";
		strProxyName += argv[1];
	}

	if (!bFailed)
	{
		// Check input
		try
		{
			boost::property_tree::read_json(argv[2],pt);
		}
		catch (std::exception& e)
		{
			std::cerr << "Error in MissionControl input: " << e.what() << std::endl;
			bFailed = true;
		}
	}

	// Set up Dispatcher
	if (!bFailed)
	{
		try
		{
			pDispatcher = new NeMALA::Dispatcher(NEMALA_ALATE_VERSION_MAJOR, NEMALA_ALATE_VERSION_MINOR, pt.get<unsigned int>("nodes.mission_control"),
					pt.get<std::string>(strProxyName + ".subscribers"));
		}
		catch (std::exception& e)
		{
			std::cerr << "Failed to initialize MissionControl: " << e.what() << std::endl;
			bFailed = true;
		}
		if (NULL == pDispatcher)
		{
			std::cerr << "Failed to Create MissionControl dispatcher." << std::endl;
			bFailed = true;
		}
	}

	// Set up Publisher
	if (!bFailed)
	{
		pDispatcher->PrintVersion();

		std::string strEndPoint(pt.get<std::string>(strProxyName + ".publishers"));
		unsigned int unTopicNumber(pt.get<unsigned int>("topics.mcm_state"));

		try
		{
			pPublisher = new NeMALA::Publisher(strEndPoint.c_str(),unTopicNumber);
		}
		catch (std::exception& e)
		{
			std::cerr << "MissionControl could not create Publisher(" << strEndPoint << " , " << unTopicNumber << "): "
					<< e.what() << std::endl;
			bFailed = true;
		}

		if (NULL == pPublisher)
		{
			std::cerr << "MissionControl failed to Create Publisher." << std::endl;
			bFailed = true;
		}
	}

	// Set up Handlers
	if (!bFailed)
	{
		pMcSm = new MissionControlStateMachine(pPublisher);
		pOpHandler = new OpHandler(pMcSm);
		pHlcStateHandler = new HlcStateHandler(pMcSm);

		if ((NULL == pHlcStateHandler)||(NULL == pOpHandler)||(NULL == pMcSm))
		{
			std::cerr << "MissionControl failed to Create handlers." << std::endl;
			bFailed = true;
		}

	}

	if (!bFailed)
	{
		// Add the publisher to the dispatcher
		pDispatcher->AddPublisher(pPublisher);
		// Add the handlers to the dispatcher
		pDispatcher->AddHandler(pt.get<unsigned int>("topics.operator_command"),pOpHandler);
		pDispatcher->AddHandler(pt.get<unsigned int>("topics.hlc_state"),pHlcStateHandler);

		try
		{
			pDispatcher->Dispatch();
		}
		catch (std::exception& e)
		{
			std::cerr << "Error in MissionControl: " << e.what() << std::endl;
		}

		delete pHlcStateHandler;
		delete pOpHandler;
		delete pMcSm;
		delete pPublisher;
		delete pDispatcher;

		std::cout << "MissionControl shutting down" << std::endl;
	}

    return 0;
}

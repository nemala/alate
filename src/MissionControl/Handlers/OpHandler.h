/*
 * OpHandler.h
 *
 *  Created on: Jul 16, 2017
 *      Author: dave
 */

#ifndef _MCM_OPERATOR_HANDLER_H_
#define _MCM_OPERATOR_HANDLER_H_

#include <NeMALA/Handler.h>
#include "../MissionControlStateMachine.h"

/*
 * Handle Operator messages.
 */
class OpHandler : public NeMALA::Handler
{
public:

	//-------------- Methods --------------------
	OpHandler(MissionControlStateMachine* pMcmSM):m_pMcmSM(pMcmSM){}
	~OpHandler(){}

	/*
	 * Handle a message.
	 */
	void Handle(NeMALA::Proptree pt);

private:
	MissionControlStateMachine* m_pMcmSM;
};

#endif // _MCM_OPERATOR_HANDLER_H_

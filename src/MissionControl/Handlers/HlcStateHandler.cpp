/*
 * HlcStateHandler.cpp
 *
 *  Created on: Jul 17, 2017
 *      Author: dave
 */

#include "HlcStateHandler.h"
#include <string>
#include <HlcStateMessage.h>
#include <iostream>

//--------------------------------------------------------------------------------

void HlcStateHandler::Handle(NeMALA::Proptree pt)
{
	// Build a message from the property tree received.
	HlcStateMessage msg(pt);
	// Extract values
	HlcStateMessage::HlcStateEnum eState = msg.GetValue();
	// Do something
	m_pMcmSM->HandleHlcStateEvent(eState);
}

//--------------------------------------------------------------------------------


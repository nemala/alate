#include <OpMessage.h>
#include <iostream>
#include "OpHandler.h"

//--------------------------------------------------------------------------------

void OpHandler::Handle(NeMALA::Proptree pt)
{
	// Build a message from the property tree received.
	OpMessage msg(pt);
	// Extract values
	OpMessage::OpCommandEnum eCommand = msg.GetValue();
	// Do something
	m_pMcmSM->HandleOpEvent(eCommand);
}

//--------------------------------------------------------------------------------


/*
 * HlcStateHandler.h
 *
 *  Created on: Jul 17, 2017
 *      Author: dave
 */

#ifndef _MCM_HLC_STATE_HANDLER_H_
#define _MCM_HLC_STATE_HANDLER_H_

#include <NeMALA/Handler.h>
#include "../MissionControlStateMachine.h"

/*
 * Handle HLC state messages.
 */
class HlcStateHandler : public NeMALA::Handler
{
public:

	//-------------- Methods --------------------
	HlcStateHandler(MissionControlStateMachine* pMcmSM):m_pMcmSM(pMcmSM){}
	~HlcStateHandler(){}

	/*
	 * Handle a message.
	 */
	void Handle(NeMALA::Proptree pt);

private:
	MissionControlStateMachine* m_pMcmSM;
};

#endif // _MCM_HLC_STATE_HANDLER_H_

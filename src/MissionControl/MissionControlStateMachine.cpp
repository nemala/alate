/*
 * MissionControlStateMachine.cpp
 *
 *  Created on: Jul 13, 2017
 *      Author: dave
 */

#include "MissionControlStateMachine.h"

//--------------------------------------------------------------------------------

MissionControlStateMachine::MissionControlStateMachine(NeMALA::Publisher* pPublisher):
m_pPublisher(pPublisher)
{
	m_pBackend = new boost::msm::back::state_machine<c_front_end>(m_pPublisher);
	m_pBackend->start();
}

//--------------------------------------------------------------------------------

void MissionControlStateMachine::HandleOpEvent(OpMessage::OpCommandEnum eCommand)
{
	m_pBackend->process_event(EventRc(eCommand));
}

//--------------------------------------------------------------------------------

void MissionControlStateMachine::HandleHlcStateEvent(HlcStateMessage::HlcStateEnum eState)
{
	m_pBackend->process_event(EventHlcState(eState));
}

//--------------------------------------------------------------------------------

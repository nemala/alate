/*
 * Factory.cpp
 *
 *  Created on: 21 Nov 2019
 *      Author: dave
 */

#include "Factory.h"
#include <string>

namespace G2Wp {

Factory::Factory(
		boost::property_tree::ptree *ptRoot,
		boost::property_tree::ptree *ptBehavior,
		NeMALA::Dispatcher* pDispatcher): Behaviors::Factory(ptRoot, ptBehavior, pDispatcher)
{
	std::string strEndPointVelCom(ptRoot->get<std::string>(ptBehavior->get<std::string>("publishers.velocity_command.endpoint")));
	unsigned int unTopicNumberVelCom(ptRoot->get<unsigned int>(ptBehavior->get<std::string>("publishers.velocity_command.topic")));

	// Construct members
	m_pPublisherVelCommand = new NeMALA::Publisher(strEndPointVelCom.c_str(),unTopicNumberVelCom);

	m_pBehaviorG2Wp = new BehaviorG2Wp(m_pPublisherVelCommand,
			ptBehavior->get<double>("params.gain_on_yaw_diff"),
			ptBehavior->get<double>("params.gain_on_distance"),
			ptBehavior->get<double>("params.dampening"));

	m_pHandlerDestination =  new HandlerDestination(m_pBehaviorG2Wp);
	m_pHandlerPosition =  new HandlerPosition(m_pBehaviorG2Wp);

	// Wire members to framework
	m_pBehavior = m_pBehaviorG2Wp;

	pDispatcher->AddHandler(ptRoot->get<unsigned int>(ptBehavior->get<std::string>("handlers.position")), m_pHandlerPosition);
	pDispatcher->AddHandler(ptRoot->get<unsigned int>(ptBehavior->get<std::string>("handlers.destination")), m_pHandlerDestination);

	pDispatcher->AddPublisher(m_pPublisherVelCommand);
}

// -------------------------------------------------------------------------------------------------------------------

Factory::~Factory()
{
	delete m_pHandlerPosition;
	delete m_pHandlerDestination;
	delete m_pBehaviorG2Wp;
	delete m_pPublisherVelCommand;
}

// -------------------------------------------------------------------------------------------------------------------

} /* namespace G2Wp */

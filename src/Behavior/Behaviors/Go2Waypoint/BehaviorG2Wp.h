/*
 * BehaviorG2Wp.h
 *
 *  Created on: 21 Nov 2021
 *      Author: dave
 */

#ifndef _BEHAVIOR_GO_TO_WAYPOINT_H_
#define _BEHAVIOR_GO_TO_WAYPOINT_H_

#include "../Behavior.h"
#include <NeMALA/Publisher.h>
#include <Vector6Message.h>

/*
 * A behavior that publishes a velocity command towards a desired position.
 */

class BehaviorG2Wp : public Behavior
{
public:
	//-------------- Methods --------------------

	BehaviorG2Wp(NeMALA::Publisher* pPublisherVelCommand,	double dGainYaw, double dGainR, double dDampening);
	~BehaviorG2Wp(){}

	// -------------- Behavior -------------------

	void Activate(){m_bActive = true;}
	void Deactivate(){m_bActive = false; m_bValidDestination= false;}

	// -------------- For the Handlers ----------------

	void SetPosition(double X, double Y, double Z, double Yaw);
	void SetDestination(double X, double Y, double Z, double Yaw);

private:

	//-------------- Variables --------------------
	NeMALA::Publisher* m_pPublisherVelCommand;
	bool m_bActive;
	Vector6Message::Coordinate m_Destination;
	bool m_bValidDestination;
	double m_dGainYaw, m_dGainR, m_dDampening;
};

#endif /* _BEHAVIOR_GO_TO_WAYPOINT_H_ */

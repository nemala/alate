/*
 * Factory.h
 *
 *  Created on: 21 Nov 2021
 *      Author: dave
 */

#ifndef SRC_BEHAVIORS_GO_TO_WAYPOINT_FACTORY_H_
#define SRC_BEHAVIORS_GO_TO_WAYPOINT_FACTORY_H_

#include "BehaviorG2Wp.h"
#include "../Factory.h"
#include "Handlers/HandlerDestination.h"
#include "Handlers/HandlerPosition.h"
#include <NeMALA/Publisher.h>
#include <NeMALA/Dispatcher.h>
#include <boost/property_tree/json_parser.hpp>
#include <boost/dll/alias.hpp>
#include <boost/smart_ptr/shared_ptr.hpp>

namespace G2Wp {

class Factory : public Behaviors::Factory
{
public:
	Factory(boost::property_tree::ptree *ptRoot,
			boost::property_tree::ptree *ptBehavior,
			NeMALA::Dispatcher* pDispatcher);

	~Factory();

	static boost::shared_ptr<Factory> create(	boost::property_tree::ptree* ptRoot,
												boost::property_tree::ptree* ptBehavior,
												NeMALA::Dispatcher* pDispatcher
											){ return boost::shared_ptr<Factory>(new Factory(ptRoot, ptBehavior, pDispatcher));}

private:
	BehaviorG2Wp* m_pBehaviorG2Wp;
	NeMALA::Publisher* m_pPublisherVelCommand;
	HandlerDestination* m_pHandlerDestination;
	HandlerPosition* m_pHandlerPosition;

};

BOOST_DLL_ALIAS(G2Wp::Factory::create, CreateBehavior)

} /* namespace G2Wp */

#endif /* SRC_BEHAVIORS_GO_TO_WAYPOINT_FACTORY_H_ */

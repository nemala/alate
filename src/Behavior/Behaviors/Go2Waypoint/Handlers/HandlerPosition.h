/*
 * HandlerPosition.h
 *
 *  Created on: 22 Nov 2021
 *      Author: dave
 */

#ifndef _BEHAVIORS_GO2WAYPOINT_HANDLERS_HANDLERPOSITION_H_
#define _BEHAVIORS_GO2WAYPOINT_HANDLERS_HANDLERPOSITION_H_

#include "HandlerCoordinate.h"
#include <Vector6Message.h>

namespace G2Wp
{

class HandlerPosition : public HandlerCoordinate
{
public:

	//-------------- Methods --------------------

	HandlerPosition(BehaviorG2Wp* pBehavior) : HandlerCoordinate(pBehavior){}

	~HandlerPosition(){}

	/*
	 * Handle a message.
	 */
	void Handle(NeMALA::Proptree pt)
	{Vector6Message msg(pt); m_pBehavior->SetPosition(msg.GetX(), msg.GetY(), msg.GetZ(), msg.GetYaw());}

};

} /* namespace G2Wp */

#endif /* _BEHAVIORS_GO2WAYPOINT_HANDLERS_HANDLERPOSITION_H_ */

/*
 * HandlerCoordinate.h
 *
 *  Created on: 21 Nov 2021
 *      Author: dave
 */

#ifndef _BEHAVIORS_GO2WAYPOINT_HANDLERS_HANDLERCOORDINATE_H_
#define _BEHAVIORS_GO2WAYPOINT_HANDLERS_HANDLERCOORDINATE_H_

#include <NeMALA/Handler.h>
#include "../BehaviorG2Wp.h"

namespace G2Wp {

class HandlerCoordinate : public NeMALA::Handler
{
public:

	//-------------- Methods --------------------
	HandlerCoordinate(BehaviorG2Wp* pBehavior):m_pBehavior(pBehavior){}
	~HandlerCoordinate(){}

	/*
	 * Handle a message.
	 */
	void Handle(NeMALA::Proptree pt) = 0;

protected:
	BehaviorG2Wp* m_pBehavior;
};

} /* namespace G2Wp */

#endif /* _BEHAVIORS_GO2WAYPOINT_HANDLERS_HANDLERCOORDINATE_H_ */

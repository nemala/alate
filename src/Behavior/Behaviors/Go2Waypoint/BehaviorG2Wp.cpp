/*
 * BehaviorG2Wp.cpp
 *
 *  Created on: 21 Nov 2021
 *      Author: dave
 */

#include "BehaviorG2Wp.h"
#include <VelocityMessage.h>
#include <GetDistance.h>
#include <math.h>

#include <boost/math/constants/constants.hpp>

const double pi = boost::math::constants::pi<double>();

#include <iostream>

//------------------------------------------------------------------------------------------------

BehaviorG2Wp::BehaviorG2Wp(NeMALA::Publisher* pPublisherVelCommand,	double dGainYaw, double dGainR, double dDampening):
			m_pPublisherVelCommand(pPublisherVelCommand), m_bActive(false),
			m_dGainYaw(dGainYaw), m_dGainR(dGainR), m_dDampening(dDampening), m_bValidDestination(false)
{}

// -------------- For the Handlers ----------------

void BehaviorG2Wp::SetPosition(double X, double Y, double Z, double Yaw)
{
	if (m_bActive)
	{
		if (m_bValidDestination)
		{
			std::cout << "BehaviorG2Wp::SetPosition Pos: (" << X << "," << Y << "," << Z << "," << Yaw << ")" << std::endl;

/*			std::cout << "BehaviorG2Wp::SetPosition Des: (" << m_Destination.dX << "," << m_Destination.dY << ","
					<< m_Destination.dZ << ", " << m_Destination.dYaw << ")" << std::endl;
*/

			double dDeltaX(m_Destination.dX - X), dDeltaY(m_Destination.dY - Y),
					dDeltaZ(m_Destination.dZ - Z), dDeltaYaw(m_Destination.dYaw - Yaw),
					dCos(cos(Yaw * pi / 180)), dSin(sin(Yaw * pi / 180));
			double dDistance = AlateUtils::GetDistance(dDeltaX,	dDeltaY, dDeltaZ);
			double dNormalizedGain(m_dGainR);
			double dLocalDx(dDeltaX * dCos - dDeltaY * dSin);
			double dLocalDy(dDeltaY * dCos + dDeltaX * dSin);
			double dForward(dLocalDx);
			double dRight(-dLocalDy);
			double dDown(-dDeltaZ);

			// Normalize the gain when not close to the destination.
			if (1 < dDistance)
			{
				dNormalizedGain = m_dGainR / dDistance;
			}
			else
			{
				dNormalizedGain = m_dGainR / m_dDampening;
			}


			//dDeltaYaw = AlateUtils::GetDistance(m_Destination.dYaw - curretPos.dYaw);

			VelocityMessage msgVelocity(dNormalizedGain * dForward, dNormalizedGain * dRight, dNormalizedGain * dDown, m_dGainYaw * dDeltaYaw,0,0);

			m_pPublisherVelCommand->Publish(msgVelocity);

/*			std::cout << "BehaviorG2Wp::SetPosition Delta: (" << dDeltaX << "," << dDeltaY << "," << dDeltaZ
					<< "," << dDeltaYaw << ")" << std::endl;

			std::cout << "BehaviorG2Wp::SetPosition Delta in local frame: (" << dLocalDx << "," << dLocalDy << "," << dDeltaZ
					<< "," << dDeltaYaw << ")" << std::endl;

*/			std::cout << "BehaviorG2Wp::SetPosition velocity Message: "
					<< "(" << msgVelocity.GetX() << "," << msgVelocity.GetY() << "," << msgVelocity.GetZ() << ","
					<< msgVelocity.GetYaw() << ")" << std::endl;

		}
	}
}

//------------------------------------------------------------------------------------------------

void BehaviorG2Wp::SetDestination(double X, double Y, double Z, double Yaw)
{
	if (m_bActive)
	{
		std::cout << "BehaviorG2Wp::SetDestination: (" << X << "," << Y << "," << Z << "," << Yaw << ")" << std::endl;

		m_Destination.dX = X;
		m_Destination.dY = Y;
		m_Destination.dZ = Z;
		m_Destination.dYaw = Yaw;
		m_bValidDestination = true;
	}
}

//------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------

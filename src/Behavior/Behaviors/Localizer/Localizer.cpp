/*
 * Localizer.cpp
 *
 *  Created on: 21 Nov 2021
 *      Author: dave
 */

#include "Localizer.h"
#include <Vector6Message.h>
#include <GeographicLib/GeoCoords.hpp>
#include <math.h>
#include <boost/math/constants/constants.hpp>

const double pi = boost::math::constants::pi<double>();

//------------------------------------------------------------------------------------------------

Localizer::Localizer(NeMALA::Publisher* pPublisher,
		double dOriginLat, double dOriginLon, double dOriginAlt, double dOriginYaw):
			m_pPublisher(pPublisher), m_bActive(false)
{
	GeographicLib::GeoCoords geoCoordsGps(dOriginLat, dOriginLon);
	m_Origin.dX = geoCoordsGps.Northing();
	m_Origin.dY = geoCoordsGps.Easting();
	m_Origin.dZ = dOriginAlt;
	m_Origin.dYaw = dOriginYaw;
}

// -------------- For the Handlers ----------------

/* Algorithm:
 * 1. Subtract the incoming coordinate from the origin to get a vector v.
 * 2. Rotate v in the opposite of the local frame's rotation.
 * 3. Publish.
 */
void Localizer::SetPosition(double dLat, double dLon, double dAlt, double dYaw)
{
	if (m_bActive)
	{
		Vector6Message msg(0,0,0,0,0,0);
		GeographicLib::GeoCoords geoCoordsGps(dLat, dLon);
		double dCos(cos(-m_Origin.dYaw * pi / 180));
		double dSin(sin(-m_Origin.dYaw * pi / 180));
		// NED to NWU
		double dX(geoCoordsGps.Northing() - m_Origin.dX);
		double dY(m_Origin.dY - geoCoordsGps.Easting());
		// NWU to XYZ
		msg.SetX(dX * dCos + dY * dSin);
		msg.SetY(- dX * dSin + dY * dCos);
		msg.SetZ(dAlt - m_Origin.dZ);
		msg.SetYaw(dYaw * 180 / pi - m_Origin.dYaw);

		m_pPublisher->Publish(msg);
	}
}

//------------------------------------------------------------------------------------------------

#include "HandlerLlc.h"

#include <LlcStatusMessage.h>

namespace Lclzr {

//--------------------------------------------------------------------------------

void HandlerLlc::Handle(NeMALA::Proptree pt)
{
	LlcStatusMessage msg(pt);
	LlcStatusMessage::LlcStateStruct llcStatus(msg.GetLlcStatus());

	m_pLocalizer->SetPosition(llcStatus.dLatitude, llcStatus.dLongitude,  llcStatus.dAltitude, llcStatus.dYaw);
}

//--------------------------------------------------------------------------------

} /* namespace Lclzr */

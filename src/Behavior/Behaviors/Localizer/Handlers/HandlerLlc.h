/*
 * HandlerLlc.h
 *
 *  Created on: 23 Nov 2021
 *      Author: dave
 */

#ifndef _LOCALIZER_HANDLER_LLC_H_
#define _LOCALIZER_HANDLER_LLC_H_

#include <NeMALA/Handler.h>
#include "../Localizer.h"

namespace Lclzr
{

/*
 * Handle LLC status messages.
 */
class HandlerLlc : public NeMALA::Handler
{
public:

	//-------------- Methods --------------------
	HandlerLlc(Localizer* pLocalizer):m_pLocalizer(pLocalizer){}
	~HandlerLlc(){}

	/*
	 * Handle a message.
	 */
	void Handle(NeMALA::Proptree pt);

private:
	Localizer* m_pLocalizer;
};

} /* namespace Lclzr */

#endif /* _LOCALIZER_HANDLER_LLC_H_ */

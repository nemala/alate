/*
 * Factory.cpp
 *
 *  Created on: 21 Nov 2019
 *      Author: dave
 */

#include "Factory.h"
#include <string>

namespace Lclzr {

Factory::Factory(
		boost::property_tree::ptree *ptRoot,
		boost::property_tree::ptree *ptBehavior,
		NeMALA::Dispatcher* pDispatcher): Behaviors::Factory(ptRoot, ptBehavior, pDispatcher)
{
	std::string strEndPointPublisher(ptRoot->get<std::string>(ptBehavior->get<std::string>("publishers.position.endpoint")));
	unsigned int unTopicNumberPosition(ptRoot->get<unsigned int>(ptBehavior->get<std::string>("publishers.position.topic")));

	// Construct members
	m_pPublisher = new NeMALA::Publisher(strEndPointPublisher.c_str(),unTopicNumberPosition);

	m_pLocalizer = new Localizer(m_pPublisher,
			ptBehavior->get<double>("params.origin.lon"),
			ptBehavior->get<double>("params.origin.lat"),
			ptBehavior->get<double>("params.origin.alt"),
			ptBehavior->get<double>("params.origin.yaw"));

	m_pHandlerLlc =  new HandlerLlc(m_pLocalizer);

	// Wire members to framework
	m_pBehavior = m_pLocalizer;

	pDispatcher->AddHandler(ptRoot->get<unsigned int>(ptBehavior->get<std::string>("handlers.llc")), m_pHandlerLlc);

	pDispatcher->AddPublisher(m_pPublisher);
}

// -------------------------------------------------------------------------------------------------------------------

Factory::~Factory()
{
	delete m_pHandlerLlc;
	delete m_pLocalizer;
	delete m_pPublisher;
}

// -------------------------------------------------------------------------------------------------------------------

} /* namespace Lclzr */

/*
 * Factory.h
 *
 *  Created on: 23 Nov 2021
 *      Author: dave
 */

#ifndef SRC_BEHAVIORS_LOCALIZER_FACTORY_H_
#define SRC_BEHAVIORS_LOCALIZER_FACTORY_H_

#include "Localizer.h"
#include "../Factory.h"
#include "Handlers/HandlerLlc.h"
#include <NeMALA/Publisher.h>
#include <NeMALA/Dispatcher.h>
#include <boost/property_tree/json_parser.hpp>
#include <boost/dll/alias.hpp>
#include <boost/smart_ptr/shared_ptr.hpp>

namespace Lclzr {

class Factory : public Behaviors::Factory
{
public:
	Factory(boost::property_tree::ptree *ptRoot,
			boost::property_tree::ptree *ptBehavior,
			NeMALA::Dispatcher* pDispatcher);

	~Factory();

	static boost::shared_ptr<Factory> create(	boost::property_tree::ptree* ptRoot,
												boost::property_tree::ptree* ptBehavior,
												NeMALA::Dispatcher* pDispatcher
											){ return boost::shared_ptr<Factory>(new Factory(ptRoot, ptBehavior, pDispatcher));}

private:
	Localizer* m_pLocalizer;
	NeMALA::Publisher* m_pPublisher;
	HandlerLlc* m_pHandlerLlc;
};

BOOST_DLL_ALIAS(Lclzr::Factory::create, CreateBehavior)

} /* namespace Lclzr */

#endif /* SRC_BEHAVIORS_LOCALIZER_FACTORY_H_ */

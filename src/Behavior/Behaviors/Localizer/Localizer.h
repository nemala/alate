/*
 * Localizer.h
 *
 *  Created on: 23 Nov 2021
 *      Author: dave
 */

#ifndef _BEHAVIOR_LOCALIZER_H_
#define _BEHAVIOR_LOCALIZER_H_

#include "../Behavior.h"
#include <NeMALA/Publisher.h>
#include <Vector6Message.h>

/*
 * A behavior that translates global GPS coordinates to local MKS coordinates.
 */

class Localizer : public Behavior
{
public:
	//-------------- Methods --------------------

	Localizer(NeMALA::Publisher* pPublisher,
			double dOriginLat, double dOriginLon, double dOriginAlt, double dOriginYaw);
	~Localizer(){}

	// -------------- Behavior -------------------

	void Activate(){m_bActive = true;}
	void Deactivate(){m_bActive = false;}

	// -------------- For the Handlers ----------------

	void SetPosition(double dLat, double dLon, double dAlt, double dYaw);

private:

	//-------------- Variables --------------------
	NeMALA::Publisher* m_pPublisher;
	bool m_bActive;
	Vector6Message::Coordinate m_Origin;
};

#endif /* _BEHAVIOR_LOCALIZER_H_ */

/*
 * InterfaceVideoRecorder.h
 *
 *  Created on: Feb 27, 2018
 *      Author: dave
 */

#ifndef _INTERFACE_VIDEO_RECORDER_H_
#define _INTERFACE_VIDEO_RECORDER_H_


//--------------------------------------------------------
// 						Class InterfaceVideoRecorder
//--------------------------------------------------------

/*
 * An interface to a video recorder:
 */

class InterfaceVideoRecorder
{
public:

	//-------------------------- Methods ------------------------------------

	InterfaceVideoRecorder(){}
	virtual ~InterfaceVideoRecorder(){}

	virtual void StartRecording() = 0;
	virtual void StopRecording() = 0;
	virtual void Shutdown() = 0;
};



#endif /* _INTERFACE_VIDEO_RECORDER_H_ */

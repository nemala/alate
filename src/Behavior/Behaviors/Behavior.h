/*
 * Behavior.h
 *
 *  Created on: 23 Jun 2019
 *      Author: dave
 */

#ifndef _BEHAVIOR_H_
#define _BEHAVIOR_H_

/*
 * An abstract class to activate / deactivate derived behavior classes.
 */
class Behavior
{
public:
	Behavior(){}
	virtual ~Behavior(){}

	virtual void Activate() = 0;
	virtual void Deactivate() = 0;
};

#endif /* _BEHAVIOR_H_ */

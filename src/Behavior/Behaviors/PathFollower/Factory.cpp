/*
 * Factory.cpp
 *
 *  Created on: 23 Nov 2021
 *      Author: dave
 */

#include "Factory.h"
#include <string>

namespace PathFlwr {

Factory::Factory(
		boost::property_tree::ptree *ptRoot,
		boost::property_tree::ptree *ptBehavior,
		NeMALA::Dispatcher* pDispatcher): Behaviors::Factory(ptRoot, ptBehavior, pDispatcher)
{
	std::string strEndPointPublisherDestination(ptRoot->get<std::string>(ptBehavior->get<std::string>("publishers.destination.endpoint")));
	std::string strEndPointPublisherAppStatus(ptRoot->get<std::string>(ptBehavior->get<std::string>("publishers.app_status.endpoint")));
	unsigned int unTopicNumberDestination(ptRoot->get<unsigned int>(ptBehavior->get<std::string>("publishers.destination.topic")));
	unsigned int unTopicNumberAppStatus(ptRoot->get<unsigned int>(ptBehavior->get<std::string>("publishers.app_status.topic")));

	// Construct members
	m_pPublisherDestination = new NeMALA::Publisher(strEndPointPublisherDestination.c_str(),unTopicNumberDestination);
	m_pPublisherAppStatus = new NeMALA::Publisher(strEndPointPublisherAppStatus.c_str(),unTopicNumberAppStatus);

	m_pPathFollower = new PathFollower(m_pPublisherDestination, m_pPublisherAppStatus,
			ptBehavior->get<int>("params.repeat"),
			ptBehavior->get<double>("params.close_enough"),
			ptBehavior->get<double>("params.aligned_enough"),
			&(ptBehavior->get_child("params.path")));

	m_pHandlerPosition =  new HandlerPosition(m_pPathFollower);

	// Wire members to framework
	m_pBehavior = m_pPathFollower;

	pDispatcher->AddHandler(ptRoot->get<unsigned int>(ptBehavior->get<std::string>("handlers.position")), m_pHandlerPosition);

	pDispatcher->AddPublisher(m_pPublisherDestination);
	pDispatcher->AddPublisher(m_pPublisherAppStatus);
}

// -------------------------------------------------------------------------------------------------------------------

Factory::~Factory()
{
	delete m_pHandlerPosition;
	delete m_pPathFollower;
	delete m_pPublisherDestination;
	delete m_pPublisherAppStatus;
}

// -------------------------------------------------------------------------------------------------------------------

} /* namespace PathFlwr */

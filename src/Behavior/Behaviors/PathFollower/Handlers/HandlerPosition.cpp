#include "HandlerPosition.h"
#include <Vector6Message.h>

namespace PathFlwr {

//--------------------------------------------------------------------------------

void HandlerPosition::Handle(NeMALA::Proptree pt)
{
	Vector6Message msg(pt);
	m_pPathFollower->SetPosition(msg.GetX(), msg.GetY(), msg.GetZ(), msg.GetYaw());
}

//--------------------------------------------------------------------------------

} /* namespace PathFlwr */

/*
 * HandlerPosition.h
 *
 *  Created on: 24 Nov 2021
 *      Author: dave
 */

#ifndef _PATH_FOLLOWER_HANDLER_POSITION_H_
#define _PATH_FOLLOWER_HANDLER_POSITION_H_

#include <NeMALA/Handler.h>
#include "../PathFollower.h"

namespace PathFlwr
{

/*
 * Handle Position messages.
 */
class HandlerPosition : public NeMALA::Handler
{
public:

	//-------------- Methods --------------------
	HandlerPosition(PathFollower* pPathFollower):m_pPathFollower(pPathFollower){}
	~HandlerPosition(){}

	/*
	 * Handle a message.
	 */
	void Handle(NeMALA::Proptree pt);

private:
	PathFollower* m_pPathFollower;
};

} /* namespace PathFlwr */

#endif /* _PATH_FOLLOWER_HANDLER_POSITION_H_ */

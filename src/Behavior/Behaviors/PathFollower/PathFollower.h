/*
 * PathFollower.h
 *
 *  Created on: 24 Nov 2021
 *      Author: dave
 */

#ifndef _BEHAVIOR_PATH_FOLLOWER_H_
#define _BEHAVIOR_PATH_FOLLOWER_H_

#include "../Behavior.h"
#include <NeMALA/Publisher.h>
#include <Vector6Message.h>
#include <boost/property_tree/json_parser.hpp>
#include <vector>

/*
 * A behavior that follows a path.
 */

class PathFollower : public Behavior
{
public:
	//-------------- Methods --------------------

	PathFollower(NeMALA::Publisher* pPublisherDestination, NeMALA::Publisher* pPublisherStatus,
			int nRepeat, double dCloseEnough, double dAlignedEnough, boost::property_tree::ptree* ptPath);
	~PathFollower(){}

	// -------------- Behavior -------------------

	void Activate(){m_bActive = true;}
	void Deactivate(){m_bActive = false;}

	// -------------- For the Handlers ----------------

	void SetPosition(double X, double Y, double Z, double dYaw);

private:

	//-------------- Variables --------------------
	NeMALA::Publisher* m_pPublisherDestination;
	NeMALA::Publisher* m_pPublisherStatus;
	bool m_bActive;
	std::vector<Vector6Message::Coordinate> m_vPath;
	Vector6Message::Coordinate m_Destination;
	unsigned int m_unWp;
	int m_nRepeat;
	double m_dCloseEnough;
	double m_dAlignedEnough;
};

#endif /* _BEHAVIOR_PATH_FOLLOWER_H_ */

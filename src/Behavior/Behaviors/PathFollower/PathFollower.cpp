/*
 * PathFollower.cpp
 *
 *  Created on: 21 Nov 2021
 *      Author: dave
 */

#include "PathFollower.h"
#include <AppMessage.h>
#include <Vector6Message.h>
#include <boost/foreach.hpp>
#include <GetDistance.h>
#include <iostream>

//------------------------------------------------------------------------------------------------

PathFollower::PathFollower(NeMALA::Publisher* pPublisherDestination, NeMALA::Publisher* pPublisherStatus,
		int nRepeat, double dCloseEnough, double dAlignedEnough, boost::property_tree::ptree* ptPath):
			m_pPublisherDestination(pPublisherDestination), m_pPublisherStatus(pPublisherStatus),
			m_bActive(false), m_unWp(0), m_nRepeat(nRepeat), m_dCloseEnough(dCloseEnough), m_dAlignedEnough(dAlignedEnough)
{
	Vector6Message::Coordinate waypoint;
	BOOST_FOREACH(boost::property_tree::ptree::value_type &vWaypoint, ptPath->get_child(""))
	{
		waypoint.dX = vWaypoint.second.get<double>("x");
		waypoint.dY = vWaypoint.second.get<double>("y");
		waypoint.dZ = vWaypoint.second.get<double>("z", 10);
		waypoint.dYaw = vWaypoint.second.get<double>("yaw", 0);

		m_vPath.push_back(waypoint);
	}
	m_Destination = m_vPath[0];
}

// -------------- For the Handlers ----------------

void PathFollower::SetPosition(double X, double Y, double Z, double dYaw)
{
//	std::cout << "PathFollower::SetPosition(" << X << "," << Y << "," << Z << "," << dYaw << ")" << std::endl;
	if (m_bActive)
	{
		Vector6Message::Coordinate curretPos;
		curretPos.dX = X;
		curretPos.dY = Y;
		curretPos.dZ = Z;
		curretPos.dYaw = dYaw;
		Vector6Message msg(m_Destination);
		double dDistance, dDeltaYaw;

/*
 * 		std::cout << "PathFollower::SetPosition Destination (" << m_Destination.dX << ","
 * 			<< m_Destination.dY << "," << m_Destination.dZ << "," << m_Destination.dYaw << ")" << std::endl;
 */

		// first things first, publish the destination (whether it needs updating or not).
		m_pPublisherDestination->Publish(msg);

		dDistance = AlateUtils::GetDistance(m_Destination.dX - curretPos.dX,
				m_Destination.dY - curretPos.dY, m_Destination.dZ - curretPos.dZ);
		dDeltaYaw = AlateUtils::GetDistance(m_Destination.dYaw - curretPos.dYaw);

		std::cout << "PathFollower::SetPosition (Distance, DeltaYaw) = (" << dDistance << "," << dDeltaYaw << ")" << std::endl;

		// Have we reached the current waypoint?
		if ((m_dCloseEnough > dDistance) and (m_dAlignedEnough > dDeltaYaw))
		{
			// time to start following the next waypoint, if possible
			// Are there waypoints remaining in the path?
			if (m_unWp < m_vPath.size())
			{
				// Next waypoint becomes the destination
				m_Destination = m_vPath[m_unWp++];

				std::cout << "PathFollower::SetPosition New Destination (" << m_Destination.dX << ","
								<< m_Destination.dY << "," << m_Destination.dZ << "," << m_Destination.dYaw << ")" << std::endl;			}
			else // path complete.
			{
				std::cout << "PathFollower::SetPosition Path Complete" << std::endl;

				AppMessage appMsg(AppMessage::AppStatusEnum::AppSuccessfulTermination);
				if (0 != m_nRepeat)
				{
					appMsg.SetValue(AppMessage::AppStatusEnum::AppSuccess);
					// repeat
					m_nRepeat--;
				}
				// restart path
				m_unWp = 0;
				m_pPublisherStatus->Publish(appMsg);
			}
		}
	}
}

//------------------------------------------------------------------------------------------------

/*
 * Factory.h
 *
 *  Created on: 24 Nov 2021
 *      Author: dave
 */

#ifndef SRC_BEHAVIORS_PATH_FOLLOWER_FACTORY_H_
#define SRC_BEHAVIORS_PATH_FOLLOWER_FACTORY_H_

#include "PathFollower.h"
#include "../Factory.h"
#include "Handlers/HandlerPosition.h"
#include <NeMALA/Publisher.h>
#include <NeMALA/Dispatcher.h>
#include <boost/property_tree/json_parser.hpp>
#include <boost/dll/alias.hpp>
#include <boost/smart_ptr/shared_ptr.hpp>

namespace PathFlwr {

class Factory : public Behaviors::Factory
{
public:
	Factory(boost::property_tree::ptree *ptRoot,
			boost::property_tree::ptree *ptBehavior,
			NeMALA::Dispatcher* pDispatcher);

	~Factory();

	static boost::shared_ptr<Factory> create(	boost::property_tree::ptree* ptRoot,
												boost::property_tree::ptree* ptBehavior,
												NeMALA::Dispatcher* pDispatcher
											){ return boost::shared_ptr<Factory>(new Factory(ptRoot, ptBehavior, pDispatcher));}

private:
	PathFollower* m_pPathFollower;
	NeMALA::Publisher* m_pPublisherDestination;
	NeMALA::Publisher* m_pPublisherAppStatus;
	HandlerPosition* m_pHandlerPosition;
};

BOOST_DLL_ALIAS(PathFlwr::Factory::create, CreateBehavior)

} /* namespace PathFlwr */

#endif /* SRC_BEHAVIORS_PATH_FOLLOWER_FACTORY_H_ */

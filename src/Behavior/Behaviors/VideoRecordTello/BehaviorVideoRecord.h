/*
 * BehaviorVideoRecord.h
 *
 *  Created on: 10 Sep 2019
 *      Author: dave
 */

#ifndef _BEHAVIORS_VIDEO_RECORD_TELLO_H_
#define _BEHAVIORS_VIDEO_RECORD_TELLO_H_

#include "../Behavior.h"
#include "VideoRecorderTello.h"
#include <boost/thread/thread.hpp>

namespace VideoRecordTello {

/*
 * A class that behaves like a video recorder
 */

class BehaviorVideoRecord: public Behavior
{
public:
	BehaviorVideoRecord(int nWriteFPS):m_videoRecorder(nWriteFPS), m_threadVideoRecorder(boost::ref(m_videoRecorder)){}
	~BehaviorVideoRecord(){ m_videoRecorder.Shutdown();	m_threadVideoRecorder.join();}

	// -------------- Behavior -------------------

	void Activate(){m_videoRecorder.StartRecording();}
	void Deactivate(){m_videoRecorder.StopRecording();}

private:
	VideoRecorderTello m_videoRecorder;
	boost::thread m_threadVideoRecorder;
};

} /* namespace VideoRecordTello */

#endif /* _BEHAVIORS_VIDEO_RECORD_TELLO_H_ */

/*
 * VideoRecorderTello.cpp
 *
 *  Created on: 10 Sep 2019
 *      Author: dave
 */

#include "VideoRecorderTello.h"
#include <boost/process/system.hpp>
#include <boost/process/search_path.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <iostream>
#include <thread>
#include <chrono>

// ---------------------------------------------------------------------------------

void VideoRecorderTello::StartRecording()
{
	if (!m_bIsRecording)
	{
		m_bIsRecording = true;
	}
}

// ---------------------------------------------------------------------------------

void VideoRecorderTello::StopRecording()
{
	if (m_bIsRecording)
	{
		m_bIsRecording = false;
	}
}

// ---------------------------------------------------------------------------------

void VideoRecorderTello::Shutdown()
{
	m_bRunning = false;
}

//--------------------------------------------------------------------------------

int VideoRecorderTello::operator()()
{
	// Try to open the video stream as long as the behavior is running.
	while (m_bRunning && (1 > cvCapture.get(cv::CAP_PROP_FPS)))
	{
		std::cout << "VideoRecorderTello attempting to open video stream." << std::endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(40));
		cvCapture.open("udp://@0.0.0.0:11111");
	}

	// If we made it up to here and the behavior is still running, then the video stream must have opened successfully.
	if (m_bRunning)
	{
		int nFPS = cvCapture.get(cv::CAP_PROP_FPS);
		m_pBuffer = new boost::circular_buffer<cv::Mat>(nFPS*2);
		cv::Size cvSizeInput(cvCapture.get(cv::CAP_PROP_FRAME_WIDTH),cvCapture.get(cv::CAP_PROP_FRAME_HEIGHT));

		std::cout << "VideoRecorderTello opened video stream with fps: " << nFPS <<
						"; size: " << cvSizeInput <<  "." << std::endl;

		TelloReadFromUDP Reader(&m_bIsRecording, &m_bRunning, m_nWriteFPS, &m_mutex, m_pBuffer, &cvCapture);
		TelloWriteToFile Writer(&m_bIsRecording, &m_bRunning, m_nWriteFPS, &m_mutex, m_pBuffer, cvSizeInput);

		boost::thread threadReader(Reader);
		boost::thread threadWriter(Writer);

		threadReader.join();
		threadWriter.join();
	}
	std::cout << "VideoRecorderRaspivid shutting down" << std::endl;
	return 0;
}

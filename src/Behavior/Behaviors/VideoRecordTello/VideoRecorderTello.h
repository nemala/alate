/*
 * VideoRecorderTello.h
 *
 *  Created on: 10 Sep 2019
 *      Author: dave
 */

#ifndef _VIDEO_RECORDER_TELLO_H_
#define _VIDEO_RECORDER_TELLO_H_

#include <boost/thread.hpp>
#include <boost/circular_buffer.hpp>
#include <opencv2/opencv.hpp>
#include "../Behavior.h"
#include "../VideoRecord/InterfaceVideoRecorder.h"
#include "TelloReadFromUDP.h"
#include "TelloWriteToFile.h"

//--------------------------------------------------------
// 						Class VideoRecorderTello
//--------------------------------------------------------

/*
 * A video recorder encapsulation of a Tello mini drone's camera:
 */

class VideoRecorderTello : public InterfaceVideoRecorder
{
public:

	//-------------------------- Methods ------------------------------------

	VideoRecorderTello(int nWriteFPS):
		m_bIsRecording(false), m_bRunning(true), m_nWriteFPS(nWriteFPS), m_pBuffer(NULL){}
	~VideoRecorderTello(){}

	int operator()();

	//-------------------------- InterfaceVideoRecorder Methods ------------------------------------

	void StartRecording();
	void StopRecording();
	void Shutdown();

private:
	bool m_bIsRecording;
	bool m_bRunning;
	int m_nWriteFPS;
	cv::VideoCapture cvCapture;
	boost::mutex m_mutex;
	boost::circular_buffer<cv::Mat>* m_pBuffer;
};



#endif /* _VIDEO_RECORDER_TELLO_H_ */
 

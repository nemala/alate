/*
 * TelloWriteToFile.cpp
 *
 *  Created on: 3 Nov 2020
 *      Author: dave
 */

#include "TelloWriteToFile.h"
#include <boost/process/system.hpp>
#include <boost/process/search_path.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <iostream>
#include <chrono>
#include <thread>

//--------------------------------------------------------------------------------

int TelloWriteToFile::operator()()
{
	auto loop_start = std::chrono::system_clock::now();
	auto loop_end = std::chrono::system_clock::now();
    auto start = std::chrono::system_clock::now();
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;

	boost::posix_time::ptime rawTime(boost::date_time::microsec_clock<boost::posix_time::ptime>::universal_time());
	std::string timeStampedName(boost::posix_time::to_simple_string(rawTime));
	timeStampedName = "./logs/" + timeStampedName + ".avi";

	cv::VideoWriter cvWriter(timeStampedName, cv::VideoWriter::fourcc('X','2','6','4'), m_nWriteFPS, m_cvSizeInput);

	while (*m_pbRunning)
	{
		loop_end = std::chrono::system_clock::now();
	    elapsed_seconds = loop_end - loop_start;
	    std::cout << "elapsed time for write loop: " << elapsed_seconds.count() << "s\n";
	    loop_start = std::chrono::system_clock::now();

	    int nChunkSize(0);

	    m_pMutex->lock();
	    nChunkSize = m_pBuffer->size();
		m_pMutex->unlock();

		std::cout << "TelloWriteToFile attempting to write " << nChunkSize << " frames." << std::endl;

		if(0 == nChunkSize)
		{
			start = std::chrono::system_clock::now();
			//avoid busy wait
			std::this_thread::sleep_for(std::chrono::milliseconds(500/m_nWriteFPS));
			end = std::chrono::system_clock::now();
			elapsed_seconds = end-start;
			std::cout << "TelloWriteToFile elapsed time for waiting for frame: " << elapsed_seconds.count() << "s\n";
		}
		else
		{
			for(int i(0); i < nChunkSize; i++)
			{
				start = std::chrono::system_clock::now();
				cvWriter.write(m_pBuffer->front());
				m_pMutex->lock();
				m_pBuffer->pop_front();
				m_pMutex->unlock();

				end = std::chrono::system_clock::now();
				elapsed_seconds = end-start;
				std::cout << "elapsed time for writing frame: " << elapsed_seconds.count() << "s\n";
			}
		}
	}
	return 0;
}

/*
 * TelloReadFromUDP.cpp
 *
 *  Created on: 3 Nov 2020
 *      Author: dave
 */

#include "TelloReadFromUDP.h"
#include <iostream>
#include <chrono>
#include <thread>

//--------------------------------------------------------------------------------

int TelloReadFromUDP::operator()()
{
	auto loop_start = std::chrono::system_clock::now();
	auto loop_end = std::chrono::system_clock::now();
	auto start = std::chrono::system_clock::now();
	auto end = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end-start;

	int nFPS = m_pcvCapture->get(cv::CAP_PROP_FPS);
	int nDropFrames((nFPS/m_nWriteFPS)-1);
	int nFrameCounter(0);

	while ((*m_pbRunning))
	{
		loop_end = std::chrono::system_clock::now();
		elapsed_seconds = loop_end - loop_start;
		std::cout << "elapsed time for read loop: " << elapsed_seconds.count() << "s\n";
		loop_start = std::chrono::system_clock::now();

		cv::Mat frame;

		start = std::chrono::system_clock::now();
		(*m_pcvCapture) >> frame;
		end = std::chrono::system_clock::now();
		elapsed_seconds = end-start;
		std::cout << "elapsed time for capturing frame: " << elapsed_seconds.count() << "s\n";

		start = std::chrono::system_clock::now();
		if (frame.empty())
		{
			//avoid busy wait
			std::this_thread::sleep_for(std::chrono::milliseconds(500/nFPS));
			end = std::chrono::system_clock::now();
			elapsed_seconds = end-start;
			std::cout << "TelloReadFromUDP elapsed time for waiting for frame: " << elapsed_seconds.count() << "s\n";
		}
		else
		{
			nFrameCounter++;
			std::cout << "Frame counter: " << nFrameCounter << "?>? Drop frames: " << nDropFrames << std::endl;
			if(nFrameCounter > nDropFrames)
			{
				nFrameCounter = 0;
				if (*m_pbIsRecording)
				{
					bool bSuccess(true);
					m_pMutex->lock();
					if(m_pBuffer->full())
					{
						std::cout << "TelloReadFromUDP buffer full, dropping frame." << std::endl;
						bSuccess = false;
					}
					else
					{
						m_pBuffer->push_back(frame);
					}
					m_pMutex->unlock();
					end = std::chrono::system_clock::now();
					elapsed_seconds = end-start;
					if (bSuccess)
					{
						std::cout << "elapsed time for buffering frame: " << elapsed_seconds.count() << "s\n";
					}
					else
					{
						std::cout << "elapsed time for dropping frame: " << elapsed_seconds.count() << "s\n";
					}
				}
				else
				{
					end = std::chrono::system_clock::now();
					elapsed_seconds = end-start;
					std::cout << "elapsed time for NOT buffering frame: " << elapsed_seconds.count() << "s\n";
				}
			}
			else
			{
				end = std::chrono::system_clock::now();
				elapsed_seconds = end-start;
				std::cout << "elapsed time for dropping frame: " << elapsed_seconds.count() << "s\n";
			}
		}
	}
	return 0;
}

/*
 * TelloReadFromUDP.h
 *
 *  Created on: 3 Nov 2020
 *      Author: dave
 */

#ifndef _TELLO_READ_FROM_UDP_H_
#define _TELLO_READ_FROM_UDP_H_

#include <boost/thread.hpp>
#include <boost/circular_buffer.hpp>
#include <opencv2/opencv.hpp>

//--------------------------------------------------------
// 						Class TelloReadFromUDP
//--------------------------------------------------------

/*
 * A callable class for reading Tello's camera stream from UDP
 */

class TelloReadFromUDP
{
public:

	//-------------------------- Methods ------------------------------------

	TelloReadFromUDP(bool* pbIsRecording, bool* pbRunning, int nWriteFPS,	boost::mutex* pMutex,
			boost::circular_buffer<cv::Mat>* pBuffer, cv::VideoCapture* pcvCapture):
				m_pbIsRecording(pbIsRecording), m_pbRunning(pbRunning),
				m_nWriteFPS(nWriteFPS), m_pMutex(pMutex), m_pBuffer(pBuffer), m_pcvCapture(pcvCapture){}
	~TelloReadFromUDP(){}

	int operator()();

private:
	bool* m_pbIsRecording;
	bool* m_pbRunning;
	int m_nWriteFPS;
	boost::mutex* m_pMutex;
	boost::circular_buffer<cv::Mat>* m_pBuffer;
	cv::VideoCapture* m_pcvCapture;
};



#endif /* _TELLO_READ_FROM_UDP_H_ */
 

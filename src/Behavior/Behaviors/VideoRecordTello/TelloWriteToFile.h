/*
 * TelloWriteToFile.h
 *
 *  Created on: 3 Nov 2020
 *      Author: dave
 */

#ifndef _TELLO_WRITE_TO_FILE_H_
#define _TELLO_WRITE_TO_FILE_H_

#include <boost/thread.hpp>
#include <boost/circular_buffer.hpp>
#include <opencv2/opencv.hpp>

//--------------------------------------------------------
// 						Class TelloWriteToFile
//--------------------------------------------------------

/*
 * A callable class for writing Tello's camera stream to a file
 */

class TelloWriteToFile
{
public:

	//-------------------------- Methods ------------------------------------

	TelloWriteToFile(bool* pbIsRecording, bool* pbRunning, int nWriteFPS,
			boost::mutex* pMutex, boost::circular_buffer<cv::Mat>* pBuffer, cv::Size cvSizeInput):
				m_pbIsRecording(pbIsRecording), m_pbRunning(pbRunning),
				m_nWriteFPS(nWriteFPS), m_pMutex(pMutex), m_pBuffer(pBuffer), m_cvSizeInput(cvSizeInput){}
	~TelloWriteToFile(){}

	int operator()();

private:
	bool* m_pbIsRecording;
	bool* m_pbRunning;
	int m_nWriteFPS;
	boost::mutex* m_pMutex;
	boost::circular_buffer<cv::Mat>* m_pBuffer;
	cv::Size m_cvSizeInput;
};



#endif /* _VIDEO_RECORDER_TELLO_H_ */
 

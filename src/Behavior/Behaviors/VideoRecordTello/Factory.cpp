/*
 * Factory.cpp
 *
 *  Created on: 10 Sep 2019
 *      Author: dave
 */

#include "Factory.h"
#include <string>

namespace VideoRecordTello {

VideoRecordTello::Factory::Factory(
		boost::property_tree::ptree *ptRoot,
		boost::property_tree::ptree *ptBehavior,
		NeMALA::Dispatcher* pDispatcher): Behaviors::Factory(ptRoot, ptBehavior, pDispatcher)
{
	m_pBehavior = new BehaviorVideoRecord(ptBehavior->get<int>("params.write_fps"));
}

// -------------------------------------------------------------------------------------------------------------------

Factory::~Factory()
{
	delete m_pBehavior;
}

// -------------------------------------------------------------------------------------------------------------------

} /* namespace VideoRecordTello */

/*
 * BehaviorVideoRecorderFfmpeg.cpp
 *
 *  Created on: Nov 5, 2020
 *      Author: dave
 */

#include "BehaviorVideoRecorderFfmpeg.h"
#include <boost/process/search_path.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/filesystem.hpp>
#include <iostream>

// ---------------------------------------------------------------------------------

BehaviorVideoRecorderFfmpeg::BehaviorVideoRecorderFfmpeg(std::string strInVcodec, std::string strOutVcodec,
		std::string strInputURL):bIsRecording(false), nInterval(0),
		m_strInVcodec(strInVcodec), m_strOutVcodec(strOutVcodec), m_strInputURL(strInputURL),
		m_work(m_io_service), m_pThreadFfmpegLauncher(NULL), m_pProcessFfmpeg(NULL)
{
	boost::posix_time::ptime rawTime(boost::date_time::microsec_clock<boost::posix_time::ptime>::universal_time());
	m_strTimeStampedName = "./logs/" + boost::posix_time::to_simple_string(rawTime);
	boost::filesystem::create_directory(m_strTimeStampedName);

	m_pThreadFfmpegLauncher = new boost::thread(boost::bind(&boost::asio::io_service::run,&m_io_service));
}

// ---------------------------------------------------------------------------------

BehaviorVideoRecorderFfmpeg::~BehaviorVideoRecorderFfmpeg()
{
	std::cout << "BehaviorVideoRecorderFfmpeg::~BehaviorVideoRecorderFfmpeg" << std::endl;

	StopInstance();
	m_io_service.stop();
	m_pThreadFfmpegLauncher->join();
	delete m_pThreadFfmpegLauncher;
}

// ---------------------------------------------------------------------------------

void BehaviorVideoRecorderFfmpeg::RunInstance()
{
	std::cout << "BehaviorVideoRecorderFfmpeg::RunInstance" << std::endl;

	if (!bIsRecording)
	{
		try
		{
			m_pProcessFfmpeg = new boost::process::child(boost::process::search_path("ffmpeg"),
					"-vcodec", m_strInVcodec, "-i", m_strInputURL, "-vcodec", m_strOutVcodec,
					m_strTimeStampedName + "/" + std::to_string(nInterval) + ".mkv",
					boost::process::std_in < m_opstream);
		}
		catch (std::exception& e)
		{
			std::cerr << "Error in BehaviorVideoRecordFfmpeg: " << e.what() << std::endl;
		}
		bIsRecording = true;
	}
}

// ---------------------------------------------------------------------------------

void BehaviorVideoRecorderFfmpeg::StopInstance()
{
	std::cout << "BehaviorVideoRecorderFfmpeg::StopInstance" << std::endl;

	if (bIsRecording)
	{
		m_opstream << 'q';
		m_pProcessFfmpeg->terminate();

		if (NULL != m_pProcessFfmpeg)
		{
			int nResult;
			m_pProcessFfmpeg->wait();
			nResult = m_pProcessFfmpeg->exit_code();
			delete m_pProcessFfmpeg;
			std::cout << "Video Recording interval " << nInterval++ << " exited with code " << nResult << std::endl;
			m_pProcessFfmpeg = NULL;
		}
		else
		{
			std::cout << "Video Recording Module exiting a non-existing process!!" << std::endl;
		}

		bIsRecording = false;
	}
}

// ---------------------------------------------------------------------------------

void BehaviorVideoRecorderFfmpeg::Activate()
{
	std::cout << "BehaviorVideoRecorderFfmpeg::Activate" << std::endl;

	m_io_service.post(boost::bind(&BehaviorVideoRecorderFfmpeg::RunInstance, this));
}

// ---------------------------------------------------------------------------------

void BehaviorVideoRecorderFfmpeg::Deactivate()
{
	std::cout << "BehaviorVideoRecorderFfmpeg::Deactivate" << std::endl;

	m_io_service.post(boost::bind(&BehaviorVideoRecorderFfmpeg::StopInstance, this));
}

//--------------------------------------------------------------------------------

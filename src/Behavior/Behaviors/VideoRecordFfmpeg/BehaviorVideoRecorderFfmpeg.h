/*
 * BehaviorVideoRecorderFfmpeg.h
 *
 *  Created on: 5 Nov 2020
 *      Author: dave
 */

#ifndef _BEHAVIORS_VIDEO_RECORDER_FFMPEG_H_
#define _BEHAVIORS_VIDEO_RECORDER_FFMPEG_H_

#include "../Behavior.h"
#include <boost/asio.hpp>
#include <boost/thread/thread.hpp>
#include <boost/process/io.hpp>
#include <boost/process/child.hpp>

/*
 * A class that behaves like a video recorder, powered by ffmpeg.
 */

class BehaviorVideoRecorderFfmpeg: public Behavior
{
public:
	BehaviorVideoRecorderFfmpeg(std::string strInVcodec, std::string strOutVcodec, std::string strInputURL);
	~BehaviorVideoRecorderFfmpeg();

	void RunInstance();
	void StopInstance();

	// -------------- Behavior -------------------

	void Activate();
	void Deactivate();

private:
	bool bIsRecording;
	int nInterval;

	std::string m_strInVcodec;
	std::string m_strOutVcodec;
	std::string m_strInputURL;

	boost::asio::io_service m_io_service;
	boost::asio::io_service::work m_work;
	boost::thread* m_pThreadFfmpegLauncher;
	boost::process::opstream m_opstream;
	boost::process::child* m_pProcessFfmpeg;

	std::string m_strTimeStampedName;
};

#endif /* _BEHAVIORS_VIDEO_RECORDER_FFMPEG_H_ */

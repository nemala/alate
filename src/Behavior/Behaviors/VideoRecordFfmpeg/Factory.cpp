/*
 * Factory.cpp
 *
 *  Created on: 9 Sep 2019
 *      Author: dave
 */

#include "Factory.h"
#include <string>

namespace VideoRecordFfmpeg {

VideoRecordFfmpeg::Factory::Factory(
		boost::property_tree::ptree *ptRoot,
		boost::property_tree::ptree *ptBehavior,
		NeMALA::Dispatcher* pDispatcher): Behaviors::Factory(ptRoot, ptBehavior, pDispatcher)
{
	m_pBehavior = new BehaviorVideoRecorderFfmpeg(ptBehavior->get<std::string>("params.input_codec"),
			ptBehavior->get<std::string>("params.output_codec"), ptBehavior->get<std::string>("params.device_url"));
}

// -------------------------------------------------------------------------------------------------------------------

Factory::~Factory()
{
	delete m_pBehavior;
}

// -------------------------------------------------------------------------------------------------------------------

} /* namespace VideoRecordFfmpeg */

/*
 * Factory.h
 *
 *  Created on: 9 Sep 2019
 *      Author: dave
 */

#ifndef _BEHAVIORS_VIDEO_RECORD_FFMPEG_FACTORY_H_
#define _BEHAVIORS_VIDEO_RECORD_FFMPEG_FACTORY_H_

#include "../Factory.h"
#include <NeMALA/Dispatcher.h>
#include <boost/property_tree/json_parser.hpp>
#include <boost/dll/alias.hpp>
#include <boost/smart_ptr/shared_ptr.hpp>
#include "BehaviorVideoRecorderFfmpeg.h"

namespace VideoRecordFfmpeg {

class Factory : public Behaviors::Factory
{
public:
	Factory(boost::property_tree::ptree *ptRoot,
			boost::property_tree::ptree *ptBehavior,
			NeMALA::Dispatcher* pDispatcher);

	~Factory();

	static boost::shared_ptr<Factory> create(	boost::property_tree::ptree* ptRoot,
												boost::property_tree::ptree* ptBehavior,
												NeMALA::Dispatcher* pDispatcher
											){ return boost::shared_ptr<Factory>(new Factory(ptRoot, ptBehavior, pDispatcher));}
};

BOOST_DLL_ALIAS(VideoRecordFfmpeg::Factory::create, CreateBehavior)

} /* namespace VideoRecordFfmpeg */

#endif /* _BEHAVIORS_VIDEO_RECORD_FFMPEG_FACTORY_H_ */

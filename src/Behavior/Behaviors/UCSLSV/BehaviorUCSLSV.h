/*
 * BehaviorUCSLSV.h
 *
 *  Created on: 23 Jun 2019
 *      Author: dave
 */

#ifndef _BEHAVIOR_UCSLSV_H_
#define _BEHAVIOR_UCSLSV_H_

#include "../Behavior.h"
#include <NeMALA/Publisher.h>

/*
 * A behavior that mimics a Unicycle Agent with Crude Sensing over a Limited Sector of Visibility swarming protocol.
 * see
 * Dovrat, David, and Alfred M. Bruckstein. "On Gathering and Control of Unicycle A(ge)nts with Crude Sensing
Capabilities. IEEE Intelligent Systems 6 (2017): 40-46. (http://ieeexplore.ieee.org/document/8267998/)
 */

class BehaviorUCSLSV : public Behavior
{
public:
	//-------------- Methods --------------------

	BehaviorUCSLSV(NeMALA::Publisher* pPublisher, double dForwardVelMeterPerSec, double dYawRateDegPerSecMax,
			double dYawRateDegPerSecMin);

	~BehaviorUCSLSV(){}

	// -------------- Behavior -------------------

	void Activate();
	void Deactivate();

	// -------------- For the Handlers ----------------

	void SetCurrentYaw(double dYawRad);
	void PeerRecognized(bool bRecognized);
	void SetControlDirection(double dDirectionDeg);

private:

	//-------------- Variables --------------------
	NeMALA::Publisher* m_pPublisher;
	double m_dForwardVelMeterPerSec;
	double m_dYawRateDegPerSecMax;
	double m_dYawRateDegPerSecMin;
	double m_dBroadcastDirectionDeg;
	double m_dAzimuthDeg;
	bool m_bActive;
};

#endif /* _BEHAVIOR_UCSLSV_H_ */

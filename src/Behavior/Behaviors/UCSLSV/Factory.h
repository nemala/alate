/*
 * Factory.h
 *
 *  Created on: 23 Jun 2019
 *      Author: dave
 */

#ifndef SRC_BEHAVIORS_UCSLSV_FACTORY_H_
#define SRC_BEHAVIORS_UCSLSV_FACTORY_H_

#include "BehaviorUCSLSV.h"
#include "Handlers/LlcStatusHandler.h"
#include "Handlers/UcslsvHandlerAzimuthCommand.h"
#include "Handlers/UcslsvHandlerPeer.h"
#include "Handlers/OperatorPayloadHandler.h"
#include "../Factory.h"
#include <NeMALA/Publisher.h>
#include <NeMALA/Dispatcher.h>
#include <boost/property_tree/json_parser.hpp>
#include <boost/dll/alias.hpp>
#include <boost/smart_ptr/shared_ptr.hpp>

namespace UCSLSV {

class Factory : public Behaviors::Factory
{
public:
	Factory(boost::property_tree::ptree *ptRoot,
			boost::property_tree::ptree *ptBehavior,
			NeMALA::Dispatcher* pDispatcher);

	~Factory();

	static boost::shared_ptr<Factory> create(	boost::property_tree::ptree* ptRoot,
												boost::property_tree::ptree* ptBehavior,
												NeMALA::Dispatcher* pDispatcher
											){ return boost::shared_ptr<Factory>(new Factory(ptRoot, ptBehavior, pDispatcher));}

private:
	BehaviorUCSLSV* m_pBehaviorUCSLSV;
	NeMALA::Publisher* m_pPublisher;
	UcslsvHandlerAzimuthCommand* m_pUcslsvHandlerAzimuthCommand;
	LlcStatusHandler* m_pLlcStatusHandler;
	OperatorPayloadHandler* m_pOperatorPayloadHandler;
};

BOOST_DLL_ALIAS(UCSLSV::Factory::create, CreateBehavior)

} /* namespace UCSLSV */

#endif /* SRC_BEHAVIORS_UCSLSV_FACTORY_H_ */

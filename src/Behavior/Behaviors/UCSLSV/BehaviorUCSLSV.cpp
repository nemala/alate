/*
 * BehaviorUCSLSV.cpp
 *
 *  Created on: 23 Jun 2019
 *      Author: dave
 */

#include "BehaviorUCSLSV.h"
#include <VelocityMessage.h>
#include <math.h>
#include <boost/math/constants/constants.hpp>

const double pi = boost::math::constants::pi<double>();

//------------------------------------------------------------------------------------------------

BehaviorUCSLSV::BehaviorUCSLSV(NeMALA::Publisher* pPublisher, double dForwardVelMeterPerSec, double dYawRateDegPerSecMax,
		double dYawRateDegPerSecMin): m_pPublisher(pPublisher), m_dForwardVelMeterPerSec(dForwardVelMeterPerSec),
		m_dYawRateDegPerSecMax(dYawRateDegPerSecMax), m_dYawRateDegPerSecMin(dYawRateDegPerSecMin),
		m_dBroadcastDirectionDeg(-1), m_dAzimuthDeg(0), m_bActive(false)
		{}

// -------------- Behavior -------------------

//------------------------------------------------------------------------------------------------

void BehaviorUCSLSV::Activate(){m_bActive = true;}

//------------------------------------------------------------------------------------------------

void BehaviorUCSLSV::Deactivate(){m_bActive = false;}

// -------------- For the Handlers ----------------

//------------------------------------------------------------------------------------------------

void BehaviorUCSLSV::SetCurrentYaw(double dYawRad)
{
	m_dAzimuthDeg = dYawRad*180/pi;
}

//------------------------------------------------------------------------------------------------

void BehaviorUCSLSV::PeerRecognized(bool bRecognized)
{
	if(m_bActive)
	{
		double dSpeed(m_dForwardVelMeterPerSec);
		VelocityMessage msgVelocity(dSpeed,0,0,m_dYawRateDegPerSecMax,0,0);

		if (0 <= m_dBroadcastDirectionDeg)
		{
			dSpeed = m_dForwardVelMeterPerSec*(1 + 0.5 * sin((m_dBroadcastDirectionDeg - m_dAzimuthDeg)*pi/180));
			msgVelocity.SetX(dSpeed);
		}

		if (bRecognized)
		{
			msgVelocity.SetYaw(m_dYawRateDegPerSecMin);
		}

		m_pPublisher->Publish(msgVelocity);
	}
}

//------------------------------------------------------------------------------------------------

void BehaviorUCSLSV::SetControlDirection(double dDirectionDeg)
{
	m_dBroadcastDirectionDeg = dDirectionDeg;
}

//----------------------------------------------------------------------------------------


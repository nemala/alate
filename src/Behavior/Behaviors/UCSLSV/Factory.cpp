/*
 * Factory.cpp
 *
 *  Created on: 23 Jun 2019
 *      Author: dave
 */

#include "Factory.h"
#include <string>

namespace UCSLSV {

UCSLSV::Factory::Factory(
		boost::property_tree::ptree *ptRoot,
		boost::property_tree::ptree *ptBehavior,
		NeMALA::Dispatcher* pDispatcher): Behaviors::Factory(ptRoot, ptBehavior, pDispatcher)
{
	std::string strEndPoint(ptRoot->get<std::string>(ptBehavior->get<std::string>("publishers.velocity_command.endpoint")));
	unsigned int unTopicNumber(ptRoot->get<unsigned int>(ptBehavior->get<std::string>("publishers.velocity_command.topic")));

	// Construct members
	m_pPublisher = new NeMALA::Publisher(strEndPoint.c_str(),unTopicNumber);

	m_pBehaviorUCSLSV = new BehaviorUCSLSV(m_pPublisher,
			ptBehavior->get<double>("params.forward_vel_msec"),
			ptBehavior->get<double>("params.yaw_rate_deg_per_sec.max"),
			ptBehavior->get<double>("params.yaw_rate_deg_per_sec.min"));

	m_pUcslsvHandlerAzimuthCommand =  new UcslsvHandlerAzimuthCommand(m_pBehaviorUCSLSV);
	m_pLlcStatusHandler =  new LlcStatusHandler(m_pBehaviorUCSLSV);
	m_pOperatorPayloadHandler =  new OperatorPayloadHandler(m_pBehaviorUCSLSV);

	// Wire members to framework
	m_pBehavior = m_pBehaviorUCSLSV;

	pDispatcher->AddHandler(ptRoot->get<unsigned int>(ptBehavior->get<std::string>("handlers.azimuth_command")), m_pUcslsvHandlerAzimuthCommand);
	pDispatcher->AddHandler(ptRoot->get<unsigned int>(ptBehavior->get<std::string>("handlers.status")), m_pLlcStatusHandler);
	pDispatcher->AddHandler(ptRoot->get<unsigned int>(ptBehavior->get<std::string>("handlers.peers")), m_pOperatorPayloadHandler);

	pDispatcher->AddPublisher(m_pPublisher);
}

// -------------------------------------------------------------------------------------------------------------------

Factory::~Factory()
{
	delete m_pUcslsvHandlerAzimuthCommand;
	delete m_pLlcStatusHandler;
	delete m_pOperatorPayloadHandler;
	delete m_pBehaviorUCSLSV;
	delete m_pPublisher;
}

// -------------------------------------------------------------------------------------------------------------------

} /* namespace UCSLSV */

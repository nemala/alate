/*
 * LlcStatusHandler.h
 *
 *  Created on: May 2, 2018
 *      Author: dave
 */

#ifndef MISSION_BEHAVIORSWARM_HANDLERS_LLCSTATUSHANDLER_H_
#define MISSION_BEHAVIORSWARM_HANDLERS_LLCSTATUSHANDLER_H_

#include <NeMALA/Handler.h>
#include "../BehaviorUCSLSV.h"

namespace UCSLSV {

/*
 * Handle LLC status messages.
 */
class LlcStatusHandler : public NeMALA::Handler
{
public:

	//-------------- Methods --------------------
	LlcStatusHandler(BehaviorUCSLSV* pUCSLSV):m_pUCSLSV(pUCSLSV){}
	~LlcStatusHandler(){}

	/*
	 * Handle a message.
	 */
	void Handle(NeMALA::Proptree pt);

private:
	BehaviorUCSLSV* m_pUCSLSV;
};

} /* namespace UCSLSV */

#endif /* MISSION_BEHAVIORSWARM_HANDLERS_LLCSTATUSHANDLER_H_ */

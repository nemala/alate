/*
 * OperatorPayloadHandler.h
 *
 *  Created on: 8 Jul 2019
 *      Author: dave
 */

#ifndef _ALATE_BEHAVIORS_UCSLSV_HANDLER_OPERATOR_PAYLOAD_H_
#define _ALATE_BEHAVIORS_UCSLSV_HANDLER_OPERATOR_PAYLOAD_H_

#include <NeMALA/Handler.h>
#include "../BehaviorUCSLSV.h"

namespace UCSLSV {

/*
 * Handle Operator Message
 */
class OperatorPayloadHandler : public NeMALA::Handler
{
public:

	//-------------- Methods --------------------
	OperatorPayloadHandler(BehaviorUCSLSV* pUCSLSV):m_pUCSLSV(pUCSLSV){}
	~OperatorPayloadHandler(){}

	/*
	 * Handle a message.
	 */
	void Handle(NeMALA::Proptree pt);

private:
	BehaviorUCSLSV* m_pUCSLSV;
};

} /* namespace UCSLSV */

#endif /* _ALATE_BEHAVIORS_UCSLSV_HANDLER_OPERATOR_PAYLOAD_H_ */

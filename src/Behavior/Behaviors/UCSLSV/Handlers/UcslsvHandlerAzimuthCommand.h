/*
 * UcslsvHandlerAzimuthCommand.h
 *
 *  Created on: May 2, 2018
 *      Author: dave
 */

#ifndef _BEHAVIORS_HANDLERS_UCSLSV_AZIMUTH_COMMAND_H_
#define _BEHAVIORS_HANDLERS_UCSLSV_AZIMUTH_COMMAND_H_

#include <NeMALA/Handler.h>
#include "../BehaviorUCSLSV.h"

/*
 * Handle direction command messages.
 */
class UcslsvHandlerAzimuthCommand : public NeMALA::Handler
{
public:
	//-------------- Methods --------------------
	UcslsvHandlerAzimuthCommand(BehaviorUCSLSV* pUCSLSV):m_pUCSLSV(pUCSLSV){}
	~UcslsvHandlerAzimuthCommand(){}

	/*
	 * Handle a message.
	 */
	void Handle(NeMALA::Proptree pt);

private:

	//-------------- Variables --------------------
	BehaviorUCSLSV* m_pUCSLSV;
};

#endif /* _BEHAVIORS_HANDLERS_UCSLSV_AZIMUTH_COMMAND_H_ */

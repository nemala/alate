#include "../../UCSLSV/Handlers/LlcStatusHandler.h"

#include <LlcStatusMessage.h>

namespace UCSLSV {

//--------------------------------------------------------------------------------

void LlcStatusHandler::Handle(NeMALA::Proptree pt)
{
	LlcStatusMessage msg(pt);
	m_pUCSLSV->SetCurrentYaw(msg.GetLlcStatus().dYaw);
}

//--------------------------------------------------------------------------------

} /* namespace UCSLSV */

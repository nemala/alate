/*
 * OperatorPayloadHandler.cpp
 *
 *  Created on: 8 Jul 2019
 *      Author: dave
 */

#include "OperatorPayloadHandler.h"
#include <StringMessage.h>
#include <boost/property_tree/json_parser.hpp>
#include <iostream>

namespace UCSLSV {

//--------------------------------------------------------------------------------

void OperatorPayloadHandler::Handle(NeMALA::Proptree pt)
{
	boost::property_tree::ptree ptPayload;
	// Build a message from the property tree received.
	StringMessage msg(pt);
	// Extract a property tree from the json content
	std::istringstream is;
	is.str(msg.GetContent());
	boost::property_tree::read_json(is,ptPayload);
	// Tell the behavior about it.
	m_pUCSLSV->PeerRecognized(ptPayload.get<bool>("detected"));
}

//--------------------------------------------------------------------------------

} /* namespace UCSLSV */

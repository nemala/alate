/*
 * UcslsvHandlerPeer.cpp
 *
 *  Created on: Aug 6, 2017
 *      Author: dave
 */

#include <PeerMessage.h>
#include "UcslsvHandlerPeer.h"

//--------------------------------------------------------------------------------

void UcslsvHandlerPeer::Handle(NeMALA::Proptree pt)
{
	// Build a message from the property tree received.
	PeerMessage msg(pt);
	// Tell the behavior about it.
	m_pUCSLSV->PeerRecognized(PeerMessage::PeerRecognized == msg.GetValue());
}

//--------------------------------------------------------------------------------


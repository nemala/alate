/*
 * PeerHandler.h
 *
 *  Created on: Aug 6, 2017
 *      Author: dave
 */

#ifndef _BEHAVIOR_UCSLSV_PEER_HANDLER_H_
#define _BEHAVIOR_UCSLSV_PEER_HANDLER_H_

#include <NeMALA/Handler.h>
#include "../BehaviorUCSLSV.h"

/*
 * Handle Peer messages.
 */
class UcslsvHandlerPeer : public NeMALA::Handler
{
public:

	//-------------- Methods --------------------
	UcslsvHandlerPeer(BehaviorUCSLSV* pUCSLSV):m_pUCSLSV(pUCSLSV){}
	~UcslsvHandlerPeer(){}

	/*
	 * Handle a message.
	 */
	void Handle(NeMALA::Proptree pt);

private:

	//-------------- Variables --------------------
	BehaviorUCSLSV* m_pUCSLSV;
};

#endif // _BEHAVIOR_UCSLSV_PEER_HANDLER_H_

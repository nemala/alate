/*
 * UcslsvHandlerAzimuthCommand.cpp
 *
 *  Created on: May 2, 2018
 *      Author: dave
 */

#include "UcslsvHandlerAzimuthCommand.h"

#include <FloatMessage.h>

//--------------------------------------------------------------------------------

void UcslsvHandlerAzimuthCommand::Handle(NeMALA::Proptree pt)
{
	// Build a message from the property tree received.
	FloatMessage msg(pt);
	// Tell the behavior about it.
	m_pUCSLSV->SetControlDirection(msg.GetValue());
}

//--------------------------------------------------------------------------------


/*
 * VideoRecorderRaspivid.cpp
 *
 *  Created on: Feb 27, 2018
 *      Author: dave
 */

#include "VideoRecorderRaspivid.h"
#include <boost/process/system.hpp>
#include <boost/process/search_path.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <iostream>

// ---------------------------------------------------------------------------------

VideoRecorderRaspivid::VideoRecorderRaspivid() : bIsRecording(false)
{}

// ---------------------------------------------------------------------------------

void VideoRecorderRaspivid::StartRecording()
{
	if (!bIsRecording)
	{
		m_opstream << std::endl;
		bIsRecording = true;
	}
}

// ---------------------------------------------------------------------------------

void VideoRecorderRaspivid::StopRecording()
{
	if (bIsRecording)
	{
		m_opstream << std::endl;
		bIsRecording = false;
	}
}

// ---------------------------------------------------------------------------------

void VideoRecorderRaspivid::Shutdown()
{
	m_opstream << 'x' << std::endl;

	std::cout << "VideoRecorderRaspivid shutting down" << std::endl;
}

//--------------------------------------------------------------------------------

int VideoRecorderRaspivid::operator()()
{
	int nResult(0);

	boost::posix_time::ptime rawTime(boost::date_time::microsec_clock<boost::posix_time::ptime>::universal_time());
	std::string timeStampedName(boost::posix_time::to_simple_string(rawTime));
	timeStampedName = "./logs/" + timeStampedName + ".h264";
	boost::process::child* pVideoProcess(NULL);

	try
	{
		pVideoProcess = new boost::process::child(boost::process::search_path("raspivid"),"-t", "0","-o", timeStampedName,  "-i", "pause", "-k",
			boost::process::std_in < m_opstream);
	}
	catch (std::exception& e)
	{
		std::cerr << "Error in VideoRecorderRaspivid: " << e.what() << std::endl;
	}

	if (pVideoProcess)
	{
		pVideoProcess->wait();
		nResult = pVideoProcess->exit_code();
		delete pVideoProcess;
		std::cout << "Video Recording Module exited with code " << nResult << std::endl;
	}
	return nResult;
}

/*
 * Factory.cpp
 *
 *  Created on: 9 Sep 2019
 *      Author: dave
 */

#include "Factory.h"
#include <string>

namespace VideoRecordRaspivid {

VideoRecordRaspivid::Factory::Factory(
		boost::property_tree::ptree *ptRoot,
		boost::property_tree::ptree *ptBehavior,
		NeMALA::Dispatcher* pDispatcher): Behaviors::Factory(ptRoot, ptBehavior, pDispatcher)
{
	m_pBehavior = new BehaviorVideoRecord();
}

// -------------------------------------------------------------------------------------------------------------------

Factory::~Factory()
{
	delete m_pBehavior;
}

// -------------------------------------------------------------------------------------------------------------------

} /* namespace VideoRecordRaspivid */

/*
 * BehaviorVideoRecord.h
 *
 *  Created on: 23 Jun 2019
 *      Author: dave
 */

#ifndef _BEHAVIORS_VIDEO_RECORD_H_
#define _BEHAVIORS_VIDEO_RECORD_H_

#include "../Behavior.h"
#include "VideoRecorderRaspivid.h"
#include <boost/thread/thread.hpp>

/*
 * A class that behaves like a video recorder
 */

class BehaviorVideoRecord: public Behavior
{
public:
	BehaviorVideoRecord():m_threadVideoRecorder(boost::ref(m_videoRecorderRaspivid)){}
	~BehaviorVideoRecord(){ m_videoRecorderRaspivid.Shutdown();	m_threadVideoRecorder.join();}

	// -------------- Behavior -------------------

	void Activate(){m_videoRecorderRaspivid.StartRecording();}
	void Deactivate(){m_videoRecorderRaspivid.StopRecording();}

private:
	VideoRecorderRaspivid m_videoRecorderRaspivid;
	boost::thread m_threadVideoRecorder;
};

#endif /* _BEHAVIORS_VIDEO_RECORD_H_ */

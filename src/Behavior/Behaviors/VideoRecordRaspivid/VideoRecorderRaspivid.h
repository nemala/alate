/*
 * VideoRecorderRaspivid.h
 *
 *  Created on: Feb 27, 2018
 *      Author: dave
 */

#ifndef _VIDEO_RECORDER_RASPIVID_H_
#define _VIDEO_RECORDER_RASPIVID_H_

#include "../VideoRecord/InterfaceVideoRecorder.h"
#include "../Behavior.h"
#include <boost/process/io.hpp>

//--------------------------------------------------------
// 						Class VideoRecorderRaspivid
//--------------------------------------------------------

/*
 * A video recorder encapsulation of raspivid:
 * https://www.raspberrypi.org/documentation/raspbian/applications/camera.md
 */

class VideoRecorderRaspivid : public InterfaceVideoRecorder
{
public:

	//-------------------------- Methods ------------------------------------

	VideoRecorderRaspivid();
	~VideoRecorderRaspivid(){}

	int operator()();

	//-------------------------- InterfaceVideoRecorder Methods ------------------------------------

	void StartRecording();
	void StopRecording();
	void Shutdown();

private:
	bool bIsRecording;
	boost::process::opstream m_opstream;
};



#endif /* _VIDEO_RECORDER_RASPIVID_H_ */
 

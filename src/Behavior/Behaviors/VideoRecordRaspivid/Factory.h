/*
 * Factory.h
 *
 *  Created on: 9 Sep 2019
 *      Author: dave
 */

#ifndef _BEHAVIORS_VIDEO_RECORD_RASPIVID_FACTORY_H_
#define _BEHAVIORS_VIDEO_RECORD_RASPIVID_FACTORY_H_

#include "BehaviorVideoRecord.h"
#include "../Factory.h"
#include <NeMALA/Dispatcher.h>
#include <boost/property_tree/json_parser.hpp>
#include <boost/dll/alias.hpp>
#include <boost/smart_ptr/shared_ptr.hpp>

namespace VideoRecordRaspivid {

class Factory : public Behaviors::Factory
{
public:
	Factory(boost::property_tree::ptree *ptRoot,
			boost::property_tree::ptree *ptBehavior,
			NeMALA::Dispatcher* pDispatcher);

	~Factory();

	static boost::shared_ptr<Factory> create(	boost::property_tree::ptree* ptRoot,
												boost::property_tree::ptree* ptBehavior,
												NeMALA::Dispatcher* pDispatcher
											){ return boost::shared_ptr<Factory>(new Factory(ptRoot, ptBehavior, pDispatcher));}
};

BOOST_DLL_ALIAS(VideoRecordRaspivid::Factory::create, CreateBehavior)

} /* namespace VideoRecordRaspivid */

#endif /* _BEHAVIORS_VIDEO_RECORD_RASPIVID_FACTORY_H_ */

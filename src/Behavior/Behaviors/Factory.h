/*
 * Factory.h
 *
 *  Created on: 4 Sep 2019
 *      Author: dave
 */

#ifndef _BEHAVIORS_FACTORY_H_
#define _BEHAVIORS_FACTORY_H_

#include "Behavior.h"
#include <NeMALA/Dispatcher.h>
#include <boost/config.hpp>

namespace Behaviors {

class BOOST_SYMBOL_VISIBLE Factory
{
public:

	Factory(boost::property_tree::ptree *ptRoot,
			boost::property_tree::ptree *ptBehavior,
			NeMALA::Dispatcher* pDispatcher): m_pBehavior(NULL){}

	virtual ~Factory() {}

	Behavior* GetBehavior(){return m_pBehavior;}

protected:
	Behavior* m_pBehavior;
};

} /* namespace Behaviors */

#endif /* _BEHAVIORS_FACTORY_H_ */

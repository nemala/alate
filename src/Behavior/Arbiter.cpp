/*
 * Arbiter.cpp
 *
 *  Created on: 23 Jun 2019
 *      Author: dave
 */

#include "Arbiter.h"
#include <stdexcept>

// -----------------------------------------------------------------------------------------------------

void Arbiter::AddBehaviorActivation(McMessage::McStateEnum eState, Behavior* pBehavior, bool bActivate)
{
	if (NULL != pBehavior)
	{
		if(bActivate)
		{
			m_mapBehaviorActivationSet[eState].insert(pBehavior);
		}
		else
		{
			m_mapBehaviorDectivationSet[eState].insert(pBehavior);
		}
	}
	else
	{
		throw std::invalid_argument("Arbiter::AddBehaviorActivation: added a null behavior");
	}
}

// -----------------------------------------------------------------------------------------------------

void Arbiter::HandleMcState(McMessage::McStateEnum eState)
{
	for (Behavior* pBehavior : m_mapBehaviorActivationSet[eState])
	{
		pBehavior->Activate();
	}
	for (Behavior* pBehavior : m_mapBehaviorDectivationSet[eState])
	{
		pBehavior->Deactivate();
	}
}

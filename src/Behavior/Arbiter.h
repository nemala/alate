/*
 * Arbiter.h
 *
 *  Created on: 23 Jun 2019
 *      Author: dave
 */

#ifndef ALATE_ARBITER_H_
#define ALATE_ARBITER_H_

#include <Behavior.h>
#include <McMessage.h>
#include <set>
#include <map>

/*
 * A class that decides which behavior will become active and when.
 */
class Arbiter
{
public:

	typedef std::set<Behavior*> BehaviorSet;

	Arbiter(){}
	~Arbiter(){}

	void AddBehaviorActivation(McMessage::McStateEnum, Behavior* pBehavior, bool bActivate);

	void HandleMcState(McMessage::McStateEnum eState);

private:
	std::map<McMessage::McStateEnum, BehaviorSet> m_mapBehaviorActivationSet;
	std::map<McMessage::McStateEnum, BehaviorSet> m_mapBehaviorDectivationSet;
};

#endif /* ALATE_ARBITER_H_ */


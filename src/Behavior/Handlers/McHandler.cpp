/*
 * McHandler.cpp
 *
 *  Created on: Aug 6, 2017
 *      Author: dave
 */

#include "McHandler.h"
#include <McMessage.h>

//--------------------------------------------------------------------------------

void McHandler::Handle(NeMALA::Proptree pt)
{
	// Build a message from the property tree received.
	McMessage msg(pt);
	// Extract values
	McMessage::McStateEnum eState = msg.GetValue();
	// Do something
	m_pArbiter->HandleMcState(eState);
}

//--------------------------------------------------------------------------------


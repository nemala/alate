/*
 * McHandler.h
 *
 *  Created on: Jun 24, 2019
 *      Author: dave
 */

#ifndef _ARBITER_MC_HANDLER_H_
#define _ARBITER_MC_HANDLER_H_

#include "../Arbiter.h"
#include <NeMALA/Handler.h>

/*
 * Handle MCM State messages.
 */
class McHandler : public NeMALA::Handler
{
public:

	//-------------- Methods --------------------

	McHandler(Arbiter* pArbiter):m_pArbiter(pArbiter){}
	~McHandler(){}

	/*
	 * Handle a message.
	 */
	void Handle(NeMALA::Proptree pt);

private:
	Arbiter* m_pArbiter;
};

#endif // _ARBITER_MC_HANDLER_H_

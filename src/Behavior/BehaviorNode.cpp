/*
 * BehaviorNode.cpp
 *
 *  Created on: Jun 24, 2019
 *      Author: dave
 */

#include "Arbiter.h"
#include "Handlers/McHandler.h"
#include "Behaviors/Factory.h"
#include <Config.h>
#include <NeMALA/Dispatcher.h>
#include <boost/property_tree/json_parser.hpp>
#include <boost/dll/import.hpp>
#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/foreach.hpp>
#include <boost/function.hpp>
#include <boost/filesystem.hpp>
#include <string>
#include <iostream>
#include <list>

int main (int argc, char** argv)
{
	boost::property_tree::ptree pt;
	bool bFailed(false);
	NeMALA::Dispatcher* pDispatcher(NULL);
	McHandler* pMcHandler(NULL);
	std::string strNode;
	std::string strProxyName("proxies.");
	Arbiter arbiter;

    typedef boost::shared_ptr<Behaviors::Factory> (CreateBehavior_t)(	boost::property_tree::ptree *ptRoot,
    																	boost::property_tree::ptree *ptBehavior,
																		NeMALA::Dispatcher* pDispatcher);
    std::list<boost::function<CreateBehavior_t> > listBehaviorFactoryFactories;
    std::list<boost::shared_ptr<Behaviors::Factory> > listBehaviorFactories;

	std::cout << std::endl << argv[0] << std::endl;

	if ( 3 != argc)
	{
		std::cout << "Usage: " << std::endl;
		std::cout << argv[0] << " node_name config.json" << std::endl;
		std::cout << "See documentation." << std::endl;

		bFailed = true;
	}
	else
	{
		strNode = "behaviors.";
		strNode += argv[1];
	}

	if (!bFailed)
	{
		// Check input
		try
		{
			boost::property_tree::read_json(argv[2],pt);
		}
		catch (std::exception& e)
		{
			std::cerr << "Error in Arbitrator input: " << e.what() << std::endl;
			bFailed = true;
		}
	}

	// Set up Dispatcher
	if (!bFailed)
	{
		std::string strNodeId("nodes.");
		strNodeId += pt.get<std::string>(strNode + ".node");
		strProxyName +=	pt.get<std::string>(strNode + ".proxies.main");

		try
		{
			pDispatcher = new NeMALA::Dispatcher(NEMALA_ALATE_VERSION_MAJOR, NEMALA_ALATE_VERSION_MINOR, pt.get<unsigned int>(strNodeId),
					pt.get<std::string>(strProxyName + ".subscribers"));
		}
		catch (std::exception& e)
		{
			std::cerr << "Failed to initialize Arbiter: " << e.what() << std::endl;
			bFailed = true;
		}
		if (NULL == pDispatcher)
		{
			std::cerr << "Failed to Create Arbiter dispatcher." << std::endl;
			bFailed = true;
		}
	}

	// Set up Behaviors
	if (!bFailed)
	{
		pDispatcher->PrintVersion();
		std::map<std::string, McMessage::McStateEnum> mapMcState; // A string to enumeration dictionary.
		boost::filesystem::path lib_path("lib"); // where behavior plugins can be found.

		mapMcState["init"]		= McMessage::McStateInit;
		mapMcState["standby"]	= McMessage::McStateStandby;
		mapMcState["taking_off"]= McMessage::McStateTakingoff;
		mapMcState["mission"]	= McMessage::McStatePerformingMission;
		mapMcState["landing"]	= McMessage::McStateLanding;
		mapMcState["manual"]	= McMessage::McStateManual;
		mapMcState["rtl"]		= McMessage::McStateReturnToLaunch;
		mapMcState["error"]		= McMessage::McStateError;

		// For each behavior within the node,
		BOOST_FOREACH(boost::property_tree::ptree::value_type &vBehavior, pt.get_child(strNode + ".behaviors"))
		{
			Behavior* pBehavior;
		    boost::function<CreateBehavior_t> createBehavior;

		    // load a plugin library,
		    createBehavior = boost::dll::import_alias<CreateBehavior_t>(lib_path / vBehavior.second.get<std::string>("plugin_lib_name"),
		    		"CreateBehavior", boost::dll::load_mode::append_decorations);

		    // and generate a factory for the plugin behavior.
		    boost::shared_ptr<Behaviors::Factory> pBehaviorFactory = createBehavior(&pt, &(vBehavior.second), pDispatcher);

		    // Don't unload the plugin libraries when leaving scope.
		    listBehaviorFactoryFactories.push_back(createBehavior);
		    // Don't lose the behaviors, handlers and publishers when leaving scope.
		    listBehaviorFactories.push_back(pBehaviorFactory);

		    // Get the plugin behavior,
		    pBehavior = pBehaviorFactory->GetBehavior();

		    // and add its activation states to the node's arbiter.
	    	BOOST_FOREACH(boost::property_tree::ptree::value_type &vActivation, vBehavior.second.get_child("activation_by_mcm_state"))
	    	{
	    		arbiter.AddBehaviorActivation(mapMcState[vActivation.first], pBehavior, (0 == vActivation.second.data().compare("true")));
	    	}
		}

		// In case there are additional proxies the node should listen to.
		BOOST_FOREACH(boost::property_tree::ptree::value_type &vProxy, pt.get_child(strNode + ".proxies.additional."))
		{
			std::string strProxy("proxies.");
			pDispatcher->Subscribe(pt.get<std::string>(strProxy + vProxy.second.data() + ".subscribers").c_str());
		}
	}

	// Set up Handlers
	if (!bFailed)
	{
		try
		{
			pMcHandler = new McHandler(&arbiter);
		}
		catch(std::exception& e)
		{
			std::cerr << "Arbiter could not create Handlers: " << e.what() << std::endl;
			bFailed = true;
		}
		if (NULL == pMcHandler)
		{
			std::cerr << "Arbiter failed to Create handlers." << std::endl;
			bFailed = true;
		}

	}

	if (!bFailed)
	{
		// Add Handlers
		pDispatcher->AddHandler(pt.get<unsigned int>("topics.mcm_state"),pMcHandler);
		try
		{
			pDispatcher->Dispatch();
		}
		catch (std::exception& e)
		{
			std::cerr << "Error in Arbiter: " << e.what() << std::endl;
		}
		delete pMcHandler;
		listBehaviorFactories.clear();
	    listBehaviorFactoryFactories.clear();
		delete pDispatcher;
		std::cout << "Arbiter shutting down" << std::endl;
	}
	return 0;
}

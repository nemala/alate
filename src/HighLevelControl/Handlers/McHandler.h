/*
 * McHandler.h
 *
 *  Created on: Jul 16, 2017
 *      Author: dave
 */

#ifndef _HLC_MC_HANDLER_H_
#define _HLC_MC_HANDLER_H_

#include "../HlcStateMachine.h"
#include <NeMALA/Handler.h>

/*
 * Handle MCM State messages.
 */
class McHandler : public NeMALA::Handler
{
public:

	//-------------- Methods --------------------
	McHandler(HlcStateMachine* pHlcSM):m_pHlcSM(pHlcSM){}
	~McHandler(){}

	/*
	 * Handle a message.
	 */
	void Handle(NeMALA::Proptree pt);

private:
	HlcStateMachine* m_pHlcSM;
};

#endif // _HLC_MC_HANDLER_H_

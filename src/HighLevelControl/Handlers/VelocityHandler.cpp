/*
 * VelocityHandler.cpp
 *
 *  Created on: Aug 7, 2017
 *      Author: dave
 */


#include "VelocityHandler.h"
#include <VelocityMessage.h>

//--------------------------------------------------------------------------------

void VelocityHandler::Handle(NeMALA::Proptree pt)
{
	VelocityMessage msgVelocity(pt);
	m_pAutopilot->SetVelocityBodyFrame(msgVelocity.GetX(),msgVelocity.GetY(), msgVelocity.GetZ());
	m_pAutopilot->SetYawRate(msgVelocity.GetYaw());
}

//--------------------------------------------------------------------------------


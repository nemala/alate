/*
 * VelocityHandler.h
 *
 *  Created on: Aug 7, 2017
 *      Author: dave
 */

#ifndef _HLC_VELOCITY_HANDLER_H_
#define _HLC_VELOCITY_HANDLER_H_

#include <NeMALA/Handler.h>
#include "../InterfaceAutopilot.h"

/*
 * Handle Peer messages.
 */
class VelocityHandler : public NeMALA::Handler
{
public:

	//-------------- Methods --------------------
	VelocityHandler(InterfaceAutopilot* pAutopilot):m_pAutopilot(pAutopilot){}
	~VelocityHandler(){}

	/*
	 * Handle a message.
	 */
	void Handle(NeMALA::Proptree pt);

private:
	InterfaceAutopilot* m_pAutopilot;
};

#endif // _HLC_VELOCITY_HANDLER_H_

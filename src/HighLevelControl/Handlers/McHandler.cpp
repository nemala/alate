#include "McHandler.h"
#include <string>
#include <McMessage.h>
#include <iostream>

//--------------------------------------------------------------------------------

void McHandler::Handle(NeMALA::Proptree pt)
{
	// Build a message from the property tree received.
	McMessage msg(pt);
	// Extract values
	McMessage::McStateEnum eState = msg.GetValue();
	// Do something
	m_pHlcSM->PostMcStateEvent(eState);
}

//--------------------------------------------------------------------------------


/*
 * InterfaceTimer.h
 *
 *  Created on: Jul 17, 2017
 *      Author: dave
 */

#ifndef _INTERFACE_TIMER_H_
#define _INTERFACE_TIMER_H_


//--------------------------------------------------------
// 						Class InterfaceTimer
//--------------------------------------------------------

/*
 * An interface to a timer:
 */

class InterfaceTimer
{
public:

	//-------------------------- Methods ------------------------------------

	InterfaceTimer(){}
	virtual ~InterfaceTimer(){}

	virtual void Set(unsigned int unMilliSeconds) = 0;
	virtual void Abort() = 0;
};



#endif /* _INTERFACE_TIMER_H_ */

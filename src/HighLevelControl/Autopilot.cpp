/*
 * Autopilot.cpp
 *
 *  Created on: Jul 23, 2017
 *      Author: dave
 */

#include "Autopilot.h"
#include <boost/process/search_path.hpp>
#include <boost/process/io.hpp>
#include <iostream>
#include <sstream>
#include <LlcStatusMessage.h>
#include <StringMessage.h>
#include <GetTimeString.h>
#include <stdexcept>
//--------------------------------------------------------------------------------

Autopilot::Autopilot(NeMALA::Publisher* pLlcStatusPublisher, NeMALA::Publisher* pLlcErrorPublisher,
		boost::property_tree::ptree &ptArgs):
	m_pLlcStatusPublisher(pLlcStatusPublisher), m_pLlcErrorPublisher(pLlcErrorPublisher),
	m_pHlcStateMachine(NULL), m_strToken("nemala"), m_work(m_io_service)
{
	std::ostringstream os;
	boost::property_tree::write_json(os, ptArgs, false);
	m_strArgs = os.str();

	m_pathPythonExecutable = ptArgs.get<std::string>("full_path_to_python");
	m_dTargetAltitude = ptArgs.get<double>("altitude");
	m_dLowBatVoltage = ptArgs.get<double>("low_bat_voltage");
	m_strExecutable = "run_autopilot.py";

	m_pThreadWriteToLlc = new boost::thread(boost::bind(&boost::asio::io_service::run,&m_io_service));
	m_pThreadReadFromLlcErr  = new boost::thread(boost::bind(&boost::asio::io_service::run,&m_io_service));
	m_io_service.post(boost::bind(&Autopilot::HandleErrorMessages, this));
}

//--------------------------------------------------------------------------------

void Autopilot::StopHandling()
{
	m_io_service.stop();
	if(m_pThreadWriteToLlc)
	{
		m_pThreadWriteToLlc->join();
		std::cout << "Autopilot: output thread joined." << std::endl;
		delete m_pThreadWriteToLlc;
		m_pThreadWriteToLlc = NULL;
	}
	if(m_pThreadReadFromLlcErr)
	{
		m_pThreadReadFromLlcErr->join();
		std::cout << "Autopilot: input thread joined." << std::endl;
		delete m_pThreadReadFromLlcErr;
		m_pThreadReadFromLlcErr = NULL;
	}
}

//--------------------------------------------------------------------------------

void Autopilot::HandleErrorMessages()
{
	std::string strLine;
	while(std::getline(m_ipstream, strLine))
	{
		if (std::string::npos != strLine.find("Link timeout, no heartbeat in last 5 seconds"))
		{
			Shutdown();
		}
		if (std::string::npos != strLine.find(" flushing stderr"))
		{
			break;
		}
		else
		{
			std::cout << AlateUtils::GetTimeString() << "Autopilot:: " << strLine << std::endl;
			StringMessage msg(strLine);
			m_pLlcErrorPublisher->Publish(msg);
		}
	}
}

//--------------------------------------------------------------------------------

void Autopilot::Shutdown()
{
	m_io_service.post(boost::bind(&Autopilot::PostStateCommand, this, InterfaceAutopilot::AutoPilotActionShutDown));
}

//--------------------------------------------------------------------------------

int Autopilot::operator()()
{
	boost::process::ipstream _ipstream;
	std::string strLine;
	InterfaceAutopilot::AutopilotActionEnum eAction;
	std::size_t pos = std::string::npos;
	int nResult(0);
	LlcStatusMessage::LlcStateStruct stLlcState;
	boost::process::child* pProcessPython(NULL);

	if (0 == m_pathPythonExecutable.string().length())
	{
		std::cout << "Autopilot: autopilot.full_path_to_python was not set in the configuration file. Searching for the path." << std::endl;
		m_pathPythonExecutable = boost::process::search_path("python3");
		if (0 == m_pathPythonExecutable.string().length())
		{
			throw std::length_error("Path to python NULL");
		}
		else
		{
			std::cout << "Autopilot: python found in " << m_pathPythonExecutable.string() << std::endl;
		}
	}

	try
	{
		pProcessPython = new boost::process::child(m_pathPythonExecutable,"-i",
				m_strExecutable,
				m_strArgs,
				boost::process::std_out > _ipstream,
				boost::process::std_err > m_ipstream,
				boost::process::std_in < m_opstream);
	}
	catch (std::exception& e)
	{
		std::cerr << AlateUtils::GetTimeString() << "Autopilot: failed running python, " << e.what() << std::endl;
	}

	if (NULL != pProcessPython)
	{
		std::cout << AlateUtils::GetTimeString() << "Autopilot: running." << std::endl;

		while (pProcessPython->running())
		{
			if (std::getline(_ipstream, strLine) && !strLine.empty())
			{
				boost::property_tree::ptree pt;
				AutopilotState stState;

				std::cout << AlateUtils::GetTimeString() << strLine << std::endl;
				pos = strLine.find(m_strToken);
				if(std::string::npos != pos)
				{
					pos += m_strToken.length();
					std::istringstream is(strLine.substr(pos));
					boost::property_tree::read_json(is,pt);
					bool bPost = false;

					stLlcState.dAltitude = stState.dAltitude = pt.get<double>("altitude");
					stLlcState.bArmed = stState.bArmed = pt.get<bool>("armed");
					stLlcState.dBatteryVoltage = stState.dBatteryVoltage = pt.get<double>("battery_voltage");
					stLlcState.nGpsFix = pt.get<double>("gps_fix");
					stLlcState.dGpsHdop = pt.get<double>("gps_hdop");
					stLlcState.strMode = pt.get<std::string>("mode");
					stLlcState.dLatitude = pt.get<double>("lat");
					stLlcState.dLongitude = pt.get<double>("lon");
					stLlcState.dYaw = pt.get<double>("yaw");
					stLlcState.strState = pt.get<std::string>("state");

					if(0 == stLlcState.strMode.compare("LAND"))
					{
						stState.eAction = InterfaceAutopilot::AutoPilotActionLand;
						bPost = true;
					}
					else if(0 == stLlcState.strMode.compare("GUIDED"))
					{
						if (3 < stLlcState.nGpsFix)
						{
							// TODO: if airborne (by altitude?) stop posting ready to takeoff.
							stState.eAction = InterfaceAutopilot::AutoPilotActionReadyToTakeoff;
							bPost = true;
						}
						if (m_dLowBatVoltage > stState.dBatteryVoltage)
						{
							stState.eAction = InterfaceAutopilot::AutoPilotActionLowBattery;
							bPost = true;
						}
					}
					else if(0 == stLlcState.strMode.compare("RTL"))
					{
						if (false == stState.bArmed)
						{
							stState.eAction = InterfaceAutopilot::AutoPilotActionReadyToTakeoff;
							bPost = true;
						}
					}
					else
					{
						stState.eAction = InterfaceAutopilot::AutoPilotActionManualOverride;
						bPost = true;
					}
					if (bPost)
					{
						m_pHlcStateMachine->PostAutoPilotEvent(stState);
					}
					LlcStatusMessage llcMsg(stLlcState);
					m_pLlcStatusPublisher->Publish(llcMsg);
				}
				if(	(std::string::npos != strLine.find("exiting peacefully.")) ||
					(std::string::npos != strLine.find("couldn't initialize")))
				{
					stState.eAction = InterfaceAutopilot::AutoPilotActionShutDown;
					m_pHlcStateMachine->PostAutoPilotEvent(stState);
					StopHandling();
					pProcessPython->terminate();
				}
			}
		}
		pProcessPython->wait();
		nResult = pProcessPython->exit_code();
		delete pProcessPython;
	}

	std::cout << "Autopilot: python process exited with code " << nResult << std::endl;
	return nResult;
}

//--------------------------------------------------------------------------------

void Autopilot::GetReadyForTakeoff()
{
	m_io_service.post(boost::bind(&Autopilot::PostStateCommand, this, InterfaceAutopilot::AutoPilotActionReadyToTakeoff));
}

//--------------------------------------------------------------------------------

void Autopilot::Arm()
{
	m_io_service.post(boost::bind(&Autopilot::PostStateCommand, this, InterfaceAutopilot::AutoPilotActionArm));
}

//--------------------------------------------------------------------------------

void Autopilot::Takeoff()
{
	m_io_service.post(boost::bind(&Autopilot::PostTakeoff, this));
}

//--------------------------------------------------------------------------------

void Autopilot::Land()
{
	m_io_service.post(boost::bind(&Autopilot::PostStateCommand, this, InterfaceAutopilot::AutoPilotActionLand));
}

//--------------------------------------------------------------------------------

void Autopilot::ReturnToLaunch()
{
	m_io_service.post(boost::bind(&Autopilot::PostStateCommand, this, InterfaceAutopilot::AutoPilotActionRTL));
}

//--------------------------------------------------------------------------------

void Autopilot::SetVelocityBodyFrame(double dForward, double dRight, double dDown)
{
	m_io_service.post(boost::bind(&Autopilot::PostVelocityBodyFrame, this, dForward, dRight, dDown));
}

//--------------------------------------------------------------------------------

void Autopilot::SetYawRate(double dDegPerSec)
{
	m_io_service.post(boost::bind(&Autopilot::PostYawRate, this, dDegPerSec));
}

//--------------------------------------------------------------------------------

void Autopilot::PostStateCommand(InterfaceAutopilot::AutopilotActionEnum eAction)
{
	m_opstream << m_strToken << eAction << std::endl;
}

//--------------------------------------------------------------------------------

void Autopilot::PostTakeoff()
{
	m_opstream << m_strToken << InterfaceAutopilot::AutoPilotActionTakeoff << " " << m_dTargetAltitude << std::endl;
}

//--------------------------------------------------------------------------------

void Autopilot::PostVelocityBodyFrame(double dForward, double dRight, double dDown)
{
	m_opstream << m_strToken << InterfaceAutopilot::AutoPilotActionVelocityBodyFrame << " " << dForward << " " << dRight << " " << dDown << std::endl;
}

//--------------------------------------------------------------------------------

void Autopilot::PostYawRate(double dDegPerSec)
{
	m_opstream << m_strToken << InterfaceAutopilot::AutoPilotActionYawRate << " " << dDegPerSec << std::endl;
}

//--------------------------------------------------------------------------------

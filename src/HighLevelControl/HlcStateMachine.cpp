/*
 * HlcStateMachine.cpp
 *
 *  Created on: Jul 13, 2017
 *      Author: dave
 */

#include "HlcStateMachine.h"

//--------------------------------------------------------------------------------

HlcStateMachine::HlcStateMachine(NeMALA::Publisher* pPublisher, InterfaceTimer* piTimer, InterfaceAutopilot* pAutoPilot):
	m_work(m_io_service), m_pMachineExecution(NULL)
{
	m_pBackend = new boost::msm::back::state_machine<c_front_end>(pPublisher,piTimer,pAutoPilot);
}

//--------------------------------------------------------------------------------

HlcStateMachine::~HlcStateMachine()
{
	m_io_service.stop();
	m_pMachineExecution->join();
	m_pBackend->stop();
	if(m_pMachineExecution)
	{
		delete m_pMachineExecution;
	}
	delete m_pBackend;
}

//--------------------------------------------------------------------------------

void HlcStateMachine::Start()
{
	m_pBackend->start();
	m_pMachineExecution = new boost::thread(boost::bind(&boost::asio::io_service::run,&m_io_service));
}

//--------------------------------------------------------------------------------

void HlcStateMachine::PostMcStateEvent(McMessage::McStateEnum eState)
{
	m_io_service.post(boost::bind(&HlcStateMachine::HandleMcState, this, eState));
}

//--------------------------------------------------------------------------------

void HlcStateMachine::PostAutoPilotEvent(InterfaceAutopilot::AutopilotState stState)
{
	m_io_service.post(boost::bind(&HlcStateMachine::HandleAutoPilotEvent, this, stState));
}

//--------------------------------------------------------------------------------

void HlcStateMachine::PostTimeout()
{
//	std::cout << "HlcStateMachine::PostTimeout" << std::endl;
	m_io_service.post(boost::bind(&HlcStateMachine::HandleTimeout, this));
}

//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------

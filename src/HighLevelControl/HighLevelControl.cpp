/*
 * HighLevelControl.cpp
 *
 *  Created on: Jul 16, 2017
 *      Author: dave
 */

#include "HlcStateMachine.h"
#include "TimerHlc.h"
#include "Handlers/McHandler.h"
#include "Handlers/VelocityHandler.h"
#include "Autopilot.h"
#include <iostream>
#include <Config.h>
#include <NeMALA/Dispatcher.h>
#include <NeMALA/Publisher.h>
#include <boost/property_tree/json_parser.hpp>
#include <boost/thread/thread.hpp>

int main (int argc, char *argv[])
{
	boost::property_tree::ptree pt;
	bool bFailed(false);
	NeMALA::Dispatcher* pDispatcher(NULL);
	NeMALA::Publisher* pPublisherState(NULL);
	NeMALA::Publisher* pLlcStatusPublisher;
	NeMALA::Publisher* pLlcErrorPublisher;
	McHandler* pMcHandler(NULL);
	VelocityHandler* pVelocityHandler(NULL);
	HlcStateMachine* pHlcSm(NULL);
	Autopilot* pAutoPilot(NULL);
	std::string strProxyName;
	TimerHlc timer(1);

	std::cout << std::endl << argv[0] << std::endl;

	if ( 3 != argc)
	{
		std::cout << "Usage: " << std::endl;
		std::cout << argv[0] << " proxy_name config.json" << std::endl;
		std::cout << "See documentation." << std::endl;

		bFailed = true;
	}
	else
	{
		strProxyName = "proxies.";
		strProxyName += argv[1];
	}

	if (!bFailed)
	{
		// Check input
		try
		{
			boost::property_tree::read_json(argv[2],pt);
		}
		catch (std::exception& e)
		{
			std::cerr << "Error in HLC input: " << e.what() << std::endl;
			bFailed = true;
		}
	}

	// Set up Dispatcher
	if (!bFailed)
	{
		try
		{
			pDispatcher = new NeMALA::Dispatcher(NEMALA_ALATE_VERSION_MAJOR,NEMALA_ALATE_VERSION_MINOR,pt.get<unsigned int>("nodes.high_level_control"),
					pt.get<std::string>(strProxyName + ".subscribers"));
		}
		catch (std::exception& e)
		{
			std::cerr << "Failed to initialize HLC: " << e.what() << std::endl;
			bFailed = true;
		}
		if (NULL == pDispatcher)
		{
			std::cerr << "Failed to Create HLC dispatcher." << std::endl;
			bFailed = true;
		}
	}

	// Set up Publishers
	if (!bFailed)
	{
		pDispatcher->PrintVersion();

		std::string strEndPoint(pt.get<std::string>(strProxyName + ".publishers"));
		unsigned int unTopicNumberState(pt.get<unsigned int>("topics.hlc_state"));
		unsigned int unTopicNumberStatus(pt.get<unsigned int>("topics.hlc_telemetry"));
		unsigned int unTopicNumberErrMsg(pt.get<unsigned int>("topics.hlc_error"));

		try
		{
			pPublisherState = new NeMALA::Publisher(strEndPoint.c_str(),unTopicNumberState);
			pLlcStatusPublisher = new NeMALA::Publisher(strEndPoint.c_str(),unTopicNumberStatus);
			pLlcErrorPublisher = new NeMALA::Publisher(strEndPoint.c_str(),unTopicNumberErrMsg);
		}
		catch (std::exception& e)
		{
			std::cerr << "HLC could not create Publishers(" << strEndPoint << " , "
					<< unTopicNumberState << " , " << unTopicNumberStatus
					 << " , " << unTopicNumberErrMsg << "): " << e.what() << std::endl;
			bFailed = true;
		}

		if ((NULL == pPublisherState)||(NULL == pLlcStatusPublisher)||(NULL == pLlcErrorPublisher))
		{
			std::cerr << "HLC failed to Create Publishers." << std::endl;
			bFailed = true;
		}
	}

	// Set up the autopilot
	if (!bFailed)
	{
		try
		{
			pAutoPilot = new Autopilot(pLlcStatusPublisher, pLlcErrorPublisher, pt.get_child("autopilot"));
		}
		catch (std::exception& e)
		{
			std::cerr << "HLC could not Create Autopilot. " << e.what() << std::endl;
			bFailed = true;
		}
		if (NULL == pAutoPilot)
		{
			std::cerr << "HLC Failed to Create Autopilot " << pt.get<std::string>("autopilot.type") << std::endl;
			bFailed = true;
		}
	}

	// Set up Handlers
	if (!bFailed)
	{
		pHlcSm = new HlcStateMachine(pPublisherState,&timer,pAutoPilot);
		timer.SetStateMachine(pHlcSm);
		pAutoPilot->SetHlcStateMachine(pHlcSm);
		pMcHandler = new McHandler(pHlcSm);
		pVelocityHandler = new VelocityHandler(pAutoPilot);

		if ((NULL == pMcHandler)||(NULL == pVelocityHandler) ||(NULL == pHlcSm))
		{
			std::cerr << "HLC Failed to Create Handlers." << std::endl;
			bFailed = true;
		}

	}

	if (!bFailed)
	{
		// Add the publishers to the dispatcher
		pDispatcher->AddPublisher(pPublisherState);
		pDispatcher->AddPublisher(pLlcStatusPublisher);
		pDispatcher->AddPublisher(pLlcErrorPublisher);
		// Fire up the state machine
		pHlcSm->Start();
		// Add the handlers to the dispatcher
		pDispatcher->AddHandler(pt.get<unsigned int>("topics.mcm_state"),pMcHandler);
		pDispatcher->AddHandler(pt.get<unsigned int>("topics.velocity_command"),pVelocityHandler);
		// Start the autopilot
		boost::thread threadAutoPilot(boost::ref(*pAutoPilot));

		try
		{
			pDispatcher->Dispatch();
		}
		catch (std::exception& e)
		{
			std::cerr << "Error in HLC: " << e.what() << std::endl;
		}

		pAutoPilot->Shutdown();
		threadAutoPilot.join(); // wait for thread to close down

		delete pMcHandler;
		delete pVelocityHandler;
		delete pHlcSm;
		delete pPublisherState;
		delete pLlcStatusPublisher;
		delete pLlcErrorPublisher;
		delete pAutoPilot;
		delete pDispatcher;

		std::cout << "HLC shutting down" << std::endl;
	}

    return 0;
}

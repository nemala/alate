/*
 * TimerHlc.h
 *
 *  Created on: Jul 17, 2017
 *      Author: dave
 */

#ifndef _TIMER_HLC_H_
#define _TIMER_HLC_H_

#include "HlcStateMachine.h"
#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include "InterfaceTimer.h"
#include <boost/thread/thread.hpp>

class TimerHlc: public InterfaceTimer
{
public:

	TimerHlc(unsigned int unThreadcount);
	~TimerHlc();

	void SetStateMachine(HlcStateMachine* pStateMachine){m_pStateMachine = pStateMachine;}

	// InterfaceTimer
	void Set(unsigned int unMilliSeconds);
	void Abort();

private:
	void HandleTimeout(const boost::system::error_code &error);

private:
	boost::asio::io_service m_io;
	boost::asio::io_service::work m_work;
	HlcStateMachine* m_pStateMachine;
	boost::asio::deadline_timer m_deadline_timer;
	boost::thread_group m_thread_group;
};

#endif /* _TIMER_HLC_H_ */

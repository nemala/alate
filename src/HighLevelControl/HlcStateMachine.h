/*
 * HlcStateMachine.h
 *
 *  Created on: Jul 16, 2017
 *      Author: dave
 */

#ifndef _HLC_STATE_MACHINE_H_
#define _HLC_STATE_MACHINE_H_

// Hack for big transition tables
#define BOOST_MPL_CFG_NO_PREPROCESSED_HEADERS
#define BOOST_MPL_LIMIT_VECTOR_SIZE 30
#define BOOST_MPL_LIMIT_MAP_SIZE 30

#include <boost/msm/back/state_machine.hpp>
#include <boost/msm/front/state_machine_def.hpp>
#include <boost/msm/front/functor_row.hpp>
#include <boost/asio.hpp>
#include <boost/thread/thread.hpp>
#include <NeMALA/Publisher.h>
#include <McMessage.h>
#include <HlcStateMessage.h>
#include "InterfaceTimer.h"
#include "InterfaceAutopilot.h"
#include <GetTimeString.h>

#include <iostream>

/*
 * A class defining a state machine for the High Level Control Module (HLC)
 */

class HlcStateMachine
{
public:

	// ----------------------------------- Procedures -------------------------------------

	HlcStateMachine(NeMALA::Publisher* pPublisher, InterfaceTimer* piTimer, InterfaceAutopilot* pAutoPilot);
	~HlcStateMachine();

	void Start();

	// Post Events
	void PostMcStateEvent(McMessage::McStateEnum eState);
	void PostAutoPilotEvent(InterfaceAutopilot::AutopilotState stState);
	void PostTimeout();

private:
	// Handlers - private so that only the post functions would be used.
	void HandleMcState(McMessage::McStateEnum eState){m_pBackend->process_event(EventMcState(eState));}
	void HandleAutoPilotEvent(InterfaceAutopilot::AutopilotState stState){m_pBackend->process_event(EventAutoPilot(stState));}
	void HandleTimeout(){m_pBackend->process_event(EventTimeout());}

	// ------------------------ Constants and Type Declarations ---------------------------

	/*
	 * All states in the HLC state machine publish the current state upon entry
	 */
	class HlcState : public boost::msm::front::state<>
    {
    public:
		// -------------------- Procedures -------------------------------

		HlcState():m_eState(HlcStateMessage::HlcStateNone){}
		virtual ~HlcState(){}

		void SetState(HlcStateMessage::HlcStateEnum eState){m_eState = eState;}

		template <class Event,class FSM>
		void on_entry(Event const&,FSM& fsm)
		{
			HlcStateMessage msg(m_eState);
			std::cout << AlateUtils::GetTimeString() << "HLC entering state: " << msg.GetName() << std::endl;
			fsm.Publish(msg);
		}

		template <class Event,class FSM>
		void on_exit(Event const&,FSM& fsm)
		{
			HlcStateMessage msg(m_eState);
			std::cout << AlateUtils::GetTimeString() << "HLC exiting state: " << msg.GetName() << std::endl;
		}

    private:

		// -------------------- Variables -------------------------------

		HlcStateMessage::HlcStateEnum m_eState;
    };


	////////////////////////////////////////////////////////////////////
	// ------------------------ Events -------------------------------//
	////////////////////////////////////////////////////////////////////

	/*
	 * Timeout Event
	 */
	struct EventTimeout{};

	/*
	 * Events generated from the Mission Control Module (MCM)
	 */

	class EventMcState
	{
		public:
			EventMcState(McMessage::McStateEnum eState):m_eState(eState){}
    		~EventMcState(){}

    		McMessage::McStateEnum GetEvent() const {return m_eState;}

		private:
    		McMessage::McStateEnum m_eState;
	};

	/*
	 * Events generated from the Autopilot
	 */
	class EventAutoPilot
	{
		public:
			EventAutoPilot(InterfaceAutopilot::AutopilotState stState):m_stState(stState){}
    		~EventAutoPilot(){}

    		InterfaceAutopilot::AutopilotActionEnum	GetState() const {return m_stState.eAction;}
    		bool									GetArmed() const {return m_stState.bArmed;}
    		double 									GetAltitude() const {return m_stState.dAltitude;}

		private:
    		InterfaceAutopilot::AutopilotState m_stState;
	};

	/*
	 * A front-end definition for the FSM structure
	 */
	class c_front_end : public boost::msm::front::state_machine_def<c_front_end,HlcState>
	{
	public:

		// ---------------------------- Procedures -------------------------------
		c_front_end(NeMALA::Publisher* pPublisher,InterfaceTimer* piTimer, InterfaceAutopilot* pAutoPilot):
			m_pPublisher(pPublisher),m_piTimer(piTimer),m_pAutoPilot(pAutoPilot){}
		~c_front_end(){}

		void Publish(NeMALA::BaseMessage &msg)
		{
			m_pPublisher->Publish(msg);
		}

		InterfaceAutopilot* GetAutoPilot(){return m_pAutoPilot;}

		void SetTimer(unsigned int unMilliseconds){m_piTimer->Set(unMilliseconds);}
		void AbortTimer(){m_piTimer->Abort();}

		template <class Event,class FSM>
		void on_entry(Event const& ,FSM&)
		{
			std::cout << AlateUtils::GetTimeString() << "Entering High Level Control" << std::endl;
		}

		template <class Event,class FSM>
		void on_exit(Event const&,FSM& )
		{
			std::cout << AlateUtils::GetTimeString() << "Leaving High Level Control" << std::endl;
		}

		// ------------------- Constants and Type Declarations -------------------

		////////////////////////////////////////////////////////////////////
		// ------------------------ States -------------------------------//
		////////////////////////////////////////////////////////////////////

		// ------------------------------------- NotError --------------------------------------------

		class NotError : public HlcState
		{
		public:
			NotError(){SetState(HlcStateMessage::HlcStateNoError);}
			~NotError(){}
		};

		// ------------------------------------- Init --------------------------------------------

		class Init : public HlcState
		{
		public:
			Init(){SetState(HlcStateMessage::HlcStateInit);}
			~Init(){}

			template <class Event,class FSM>
			void on_entry(Event const& ev,FSM& fsm)
			{
				HlcState::on_entry(ev,fsm);
				fsm.SetTimer(3000);
				fsm.GetAutoPilot()->GetReadyForTakeoff();
			}

		};

		// ------------------------------------- WaitingForMc --------------------------------------------

		class WaitingForMc : public HlcState
		{
		public:
			WaitingForMc(){SetState(HlcStateMessage::HlcStateWaitingForMc);}
			~WaitingForMc(){}

			template <class Event,class FSM>
			void on_entry(Event const& ev,FSM& fsm)
			{
				HlcState::on_entry(ev,fsm);
				fsm.SetTimer(3000);
			}
		};

		// ------------------------------------- WaitingForLlc --------------------------------------------

		class WaitingForLlc : public HlcState
		{
		public:
			WaitingForLlc(){SetState(HlcStateMessage::HlcStateWaitingForLlc);}
			~WaitingForLlc(){}

			template <class Event,class FSM>
			void on_entry(Event const& ev,FSM& fsm)
			{
				HlcState::on_entry(ev,fsm);
				fsm.SetTimer(20000);
				fsm.GetAutoPilot()->GetReadyForTakeoff();
			}
		};

		// ------------------------------------- Ready --------------------------------------------

		class Ready : public HlcState
		{
		public:
			Ready(){SetState(HlcStateMessage::HlcStateReady);}
			~Ready(){}

            // Internal Transition Table for catching Autopilot events while in Ready state
            struct internal_transition_table : boost::mpl::vector<
			//								Start		Event			Next		Action				Guard
			//--------------------------+---------+-----------------+-----------+---------------------+----------------------+
			boost::msm::front::Internal<			EventAutoPilot>
            //----------------------------------------------------------------------------------------------------------------
            > {};
		};

		// ------------------------------------- Takeoff --------------------------------------------

		class Takeoff : public HlcState
		{
		public:
			Takeoff(){SetState(HlcStateMessage::HlcStateTakeoff);}
			~Takeoff(){}

			template <class Event,class FSM>
			void on_entry(Event const& ev,FSM& fsm)
			{
				HlcState::on_entry(ev,fsm);
				fsm.SetTimer(3000);
				fsm.GetAutoPilot()->Arm();
			}

		};

		// ------------------------------------- GainingAltitude --------------------------------------------

		class GainingAltitude : public HlcState
		{
		public:
			GainingAltitude(){SetState(HlcStateMessage::HlcStateGainingAltitude);}
			~GainingAltitude(){}

			template <class Event,class FSM>
			void on_entry(Event const& ev,FSM& fsm)
			{
				HlcState::on_entry(ev,fsm);
				// A second for each meter, + 5 seconds to think about it.
				fsm.SetTimer(fsm.GetAutoPilot()->GetTakeoffAltitude() * 1000 + 5000);
				fsm.GetAutoPilot()->Takeoff();
			}
		};

		// ------------------------------------- Airborn --------------------------------------------

		class Airborne : public HlcState
		{
		public:
			Airborne(){SetState(HlcStateMessage::HlcStateAirborne);}
			~Airborne(){}
		};

		// ------------------------------------- Landing --------------------------------------------

		class Landing : public HlcState
		{
		public:
			Landing(){SetState(HlcStateMessage::HlcStateLanding);}
			~Landing(){}

			template <class Event,class FSM>
			void on_entry(Event const& ev,FSM& fsm)
			{
				HlcState::on_entry(ev,fsm);
				fsm.GetAutoPilot()->Land();
			}
		};

		// ------------------------------------- Manual --------------------------------------------

		class Manual : public HlcState
		{
		public:
			Manual(){SetState(HlcStateMessage::HlcStateManual);}
			~Manual(){}
		};

		// ------------------------------------- ReturnToLaunch --------------------------------------------

		class ReturnToLaunch : public HlcState
		{
		public:
			ReturnToLaunch(){SetState(HlcStateMessage::HlcStateRTL);}
			~ReturnToLaunch(){}

			template <class Event,class FSM>
			void on_entry(Event const& ev,FSM& fsm)
			{
				HlcState::on_entry(ev,fsm);
				fsm.GetAutoPilot()->ReturnToLaunch();
			}
		};

		// ----------------------------------------------------------------------------------------------
		// ------------------------------------- ErrorStates --------------------------------------------
		// ----------------------------------------------------------------------------------------------


		// ------------------------------------- LowBattery --------------------------------------------

		class LowBattery : public HlcState
		{
		public:
			LowBattery(){SetState(HlcStateMessage::HlcStateLowBattery);}
			~LowBattery(){}
		};

		// ------------------------------------- LlcDown --------------------------------------------

		class LlcDown : public boost::msm::front::terminate_state<>
		{
		public:
			LlcDown(){}
			~LlcDown(){}

			template <class Event,class FSM>
			void on_entry(Event const&,FSM& fsm)
			{
				HlcStateMessage msg(HlcStateMessage::HlcStateLlcDown);
				std::cout << AlateUtils::GetTimeString() << "HLC entering state: " << msg.GetName() << std::endl;
				fsm.Publish(msg);
			}
		};

		// ----------------------------------------------------------------------------------------------

		// boost::msm requires an initial_state to be defined
		typedef boost::mpl::vector<Init,NotError> initial_state;

		// ----------------------------------------------------------------------------------------------

		////////////////////////////////////////////////////////////////////
		// --------------------------- Actions ---------------------------//
		////////////////////////////////////////////////////////////////////

        struct StopTimer
        {
            template <class EVT,class FSM,class SourceState,class TargetState>
            void operator()(EVT const& ,FSM& fsm,SourceState& ,TargetState& )
            {
            	fsm.AbortTimer();
            }
        };

        struct GetReadyForTakeoff
          {
              template <class EVT,class FSM,class SourceState,class TargetState>
              void operator()(EVT const& ,FSM& fsm,SourceState& ,TargetState& )
              {
            	  fsm.GetAutoPilot()->GetReadyForTakeoff();
              }
          };

		////////////////////////////////////////////////////////////////////
		// ------------------------ Guard Conditions ---------------------//
		////////////////////////////////////////////////////////////////////

		// ---------------------------------------------------------------------------------
		// ------------------------------------- MC ----------------------------------------
		// ---------------------------------------------------------------------------------

        struct IsMcStateInit
        {
			template <class EVT,class FSM,class SourceState,class TargetState>
			bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
			{
				return (McMessage::McStateInit == evt.GetEvent());
			}
        };

		// ---------------------------------------------------------------------------------

        struct IsMcStateTakingOff
        {
			template <class EVT,class FSM,class SourceState,class TargetState>
			bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
			{
				return (McMessage::McStateTakingoff == evt.GetEvent());
			}
        };

		// ---------------------------------------------------------------------------------

        struct IsMcStateLanding
        {
			template <class EVT,class FSM,class SourceState,class TargetState>
			bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
			{
				return (McMessage::McStateLanding == evt.GetEvent());
			}
        };

		// ---------------------------------------------------------------------------------

        struct IsMcStateRTL
        {
			template <class EVT,class FSM,class SourceState,class TargetState>
			bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
			{
				return (McMessage::McStateReturnToLaunch == evt.GetEvent());
			}
        };

		// ---------------------------------------------------------------------------------

        struct IsMcStateManual
        {
			template <class EVT,class FSM,class SourceState,class TargetState>
			bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
			{
				return (McMessage::McStateManual == evt.GetEvent());
			}
        };

		// ---------------------------------------------------------------------------------
		// ---------------------------------- AutoPilot ------------------------------------
		// ---------------------------------------------------------------------------------


        struct IsAutoPilotDown
        {
			template <class EVT,class FSM,class SourceState,class TargetState>
			bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
			{
				return (InterfaceAutopilot::AutoPilotActionShutDown == evt.GetState());
			}
        };

        // ---------------------------------------------------------------------------------

        struct IsAutoPilotLowBattery
        {
			template <class EVT,class FSM,class SourceState,class TargetState>
			bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
			{
				return (InterfaceAutopilot::AutoPilotActionLowBattery == evt.GetState());
			}
        };

        // ---------------------------------------------------------------------------------

        struct IsAutoPilotReadyToTakeoff
        {
			template <class EVT,class FSM,class SourceState,class TargetState>
			bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
			{
				return (InterfaceAutopilot::AutoPilotActionReadyToTakeoff == evt.GetState());
			}
        };

		// ---------------------------------------------------------------------------------

        struct IsAutoPilotArmed
        {
			template <class EVT,class FSM,class SourceState,class TargetState>
			bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
			{
				return evt.GetArmed();
			}
        };

		// ---------------------------------------------------------------------------------

        struct IsAutoPilotTakeoff
        {
			template <class EVT,class FSM,class SourceState,class TargetState>
			bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
			{
				return (0.95 * fsm.GetAutoPilot()->GetTakeoffAltitude() < evt.GetAltitude());
			}
        };

		// ---------------------------------------------------------------------------------

        struct IsAutoPilotLanded
        {
			template <class EVT,class FSM,class SourceState,class TargetState>
			bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
			{
				return (false == evt.GetArmed());
			}
        };

		// ---------------------------------------------------------------------------------

        struct IsAutoPilotManual
        {
			template <class EVT,class FSM,class SourceState,class TargetState>
			bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
			{
				return (InterfaceAutopilot::AutoPilotActionManualOverride == evt.GetState());
			}
        };

		////////////////////////////////////////////////////////////////////
		// ------------------------ Transition Table ---------------------//
		////////////////////////////////////////////////////////////////////

		// Replaces the default no-transition response.
		template <class FSM,class Event>
		void no_transition(Event const& e, FSM&,int state)
		{
			std::cout << AlateUtils::GetTimeString() << "HLC State Machine:: no transition from state " << state + 1
					<< " on event " << typeid(e).name() << std::endl;
		}

		// Transition Table
		struct transition_table : boost::mpl::vector<
		//					    Start    		 		Event     			 	 	  		Next  			    	Action				 		Guard
		//-----------------------------------------+-------------------------------+---------------------------+---------------------------+-------------------------+
		boost::msm::front::Row < NotError			, EventAutoPilot				, LlcDown				 	, boost::msm::front::none	, IsAutoPilotDown>,
		boost::msm::front::Row < NotError			, EventAutoPilot				, LowBattery	 			, boost::msm::front::none	, IsAutoPilotLowBattery>,
		//-----------------------------------------+-------------------------------+---------------------------+---------------------------+-------------------------+
		boost::msm::front::Row < LowBattery			, EventAutoPilot				, LlcDown		 			, boost::msm::front::none	, IsAutoPilotDown>,
		boost::msm::front::Row < LowBattery			, EventAutoPilot				, LowBattery	 			, boost::msm::front::none	, IsAutoPilotLowBattery>,
		//-----------------------------------------+-------------------------------+---------------------------+---------------------------+-------------------------+
		boost::msm::front::Row < Init 	 			, EventAutoPilot				, WaitingForMc	 			, StopTimer					, IsAutoPilotReadyToTakeoff>,
		boost::msm::front::Row < Init	 			, EventTimeout					, Init>,
		boost::msm::front::Row < Init 				, EventMcState					, WaitingForLlc	 			, StopTimer					, IsMcStateInit>,
		//-----------------------------------------+-------------------------------+---------------------------+---------------------------+-------------------------+
		boost::msm::front::Row < WaitingForMc		, EventMcState					, Ready	 					, StopTimer					, IsMcStateInit>,
		boost::msm::front::Row < WaitingForMc	 	, EventTimeout					, WaitingForMc>,
		//-----------------------------------------+-------------------------------+---------------------------+---------------------------+-------------------------+
		boost::msm::front::Row < WaitingForLlc		, EventAutoPilot				, Ready	 					, StopTimer					, IsAutoPilotReadyToTakeoff>,
		boost::msm::front::Row < WaitingForLlc	 	, EventTimeout					, WaitingForLlc>,
		//-----------------------------------------+-------------------------------+---------------------------+---------------------------+-------------------------+
		boost::msm::front::Row < Ready	 			, EventMcState					, Takeoff					, boost::msm::front::none	, IsMcStateTakingOff>,
		//boost::msm::front::Row < Ready	 			, EventAutoPilot				, boost::msm::front::none>,
		//-----------------------------------------+-------------------------------+---------------------------+---------------------------+-------------------------+
		boost::msm::front::Row < Takeoff 			, EventAutoPilot				, GainingAltitude			, StopTimer					, IsAutoPilotArmed>,
		boost::msm::front::Row < Takeoff 			, EventTimeout					, Ready>,
		//-----------------------------------------+-------------------------------+---------------------------+---------------------------+-------------------------+
		boost::msm::front::Row < GainingAltitude 	, EventAutoPilot				, Airborne					, StopTimer					, IsAutoPilotTakeoff>,
		boost::msm::front::Row < GainingAltitude	, EventAutoPilot				, Manual					, StopTimer					, IsAutoPilotManual>,
		boost::msm::front::Row < GainingAltitude 	, EventMcState					, Landing					, StopTimer					, IsMcStateLanding>,
		boost::msm::front::Row < GainingAltitude	, EventTimeout					, Landing>,
		//-----------------------------------------+-------------------------------+---------------------------+---------------------------+-------------------------+
		boost::msm::front::Row < Airborne			, EventMcState					, Landing					, boost::msm::front::none	, IsMcStateLanding>,
		boost::msm::front::Row < Airborne			, EventMcState					, ReturnToLaunch			, boost::msm::front::none	, IsMcStateRTL>,
		boost::msm::front::Row < Airborne			, EventAutoPilot				, Manual					, boost::msm::front::none	, IsAutoPilotManual>,
		//-----------------------------------------+-------------------------------+---------------------------+---------------------------+-------------------------+
		boost::msm::front::Row < Landing		 	, EventAutoPilot				, Ready						, GetReadyForTakeoff		, IsAutoPilotLanded>,
		boost::msm::front::Row < Landing		 	, EventAutoPilot				, Manual					, boost::msm::front::none	, IsAutoPilotManual>,
		boost::msm::front::Row < Landing 			, EventMcState					, boost::msm::front::none	, boost::msm::front::none	, IsMcStateLanding>,
		//-----------------------------------------+-------------------------------+---------------------------+---------------------------+-------------------------+
		boost::msm::front::Row < Manual			 	, EventAutoPilot				, Ready						, GetReadyForTakeoff		, IsAutoPilotLanded>,
		boost::msm::front::Row < Manual 			, EventMcState					, boost::msm::front::none	, boost::msm::front::none	, IsMcStateManual>,
		//-----------------------------------------+-------------------------------+---------------------------+---------------------------+-------------------------+
		boost::msm::front::Row < ReturnToLaunch	 	, EventAutoPilot				, Ready						, GetReadyForTakeoff		, IsAutoPilotLanded>
		//-----------------------------------------+-------------------------------+---------------------------+---------------------------+-------------------------+
		> {};

		private:

		// -------------------- Variables -------------------------------

		NeMALA::Publisher* m_pPublisher;
		InterfaceTimer* m_piTimer;
		InterfaceAutopilot* m_pAutoPilot;
	};

	private:

	boost::msm::back::state_machine<c_front_end>* m_pBackend;
	boost::asio::io_service m_io_service;
	boost::asio::io_service::work m_work;
	boost::thread* m_pMachineExecution;
};

#endif /* _HLC_STATE_MACHINE_H_ */

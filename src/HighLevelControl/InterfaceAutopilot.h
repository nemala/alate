/*
 * InterfaceAutopilot.h
 *
 *  Created on: Jul 18, 2017
 *      Author: dave
 */

#ifndef _INTERFACE_AUTOPILOT_H_
#define _INTERFACE_AUTOPILOT_H_


//--------------------------------------------------------
// 						Class InterfaceAutopilot
//--------------------------------------------------------

/*
 * An interface to an autopilot:
 */

class InterfaceAutopilot
{
public:

	// ------------------------ Constants and Type Declarations ---------------------------

	typedef enum
	{
		AutoPilotActionShutDown=0,
		AutoPilotActionReadyToTakeoff,
		AutoPilotActionArm,
		AutoPilotActionTakeoff,
		AutoPilotActionLand,
		AutoPilotActionVelocityBodyFrame,
		AutoPilotActionYawRate,
		AutoPilotActionRTL,
		AutoPilotActionManualOverride,
		AutoPilotActionLowBattery
	}AutopilotActionEnum;

	typedef struct
	{
		double dAltitude;
		bool bArmed;
		AutopilotActionEnum eAction;
		double dBatteryVoltage;
	}AutopilotState;

	//-------------------------- Methods ------------------------------------

	InterfaceAutopilot(){}
	virtual ~InterfaceAutopilot(){}

	virtual void GetReadyForTakeoff() = 0;
	virtual void Arm() = 0;
	virtual void Takeoff() = 0;
	virtual void Land() = 0;
	virtual void ReturnToLaunch() = 0;
	virtual void SetVelocityBodyFrame(double dForward, double dRight, double dDown) = 0;
	virtual void SetYawRate(double dDegPerSec) = 0;
	virtual double GetTakeoffAltitude() = 0;

};




#endif /* _INTERFACE_AUTOPILOT_H_ */

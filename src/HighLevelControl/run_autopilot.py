#!/usr/bin/env python

###################################################################################
# File created by David Dovrat, Aug 1, 2019.
# For NeMALA Alate, under that project's license.
# The code in this file is provided "as is" and comes with no warranty whatsoever.
###################################################################################

import sys
import json
from AutoPilots import AutopilotFactory, AutopilotBridge

if __name__ == "__main__":
    success = True
    if (2 != len(sys.argv)):
        print("Usage Example: SITL - " + sys.argv[0] + ' {"type":"dronekit","master":"tcp:127.0.0.1:14552","baud":"921600","altitude":"10","low_bat_voltage":"11.1"}')
    else:
        autopilot = None
        factory = AutopilotFactory.AutopilotFactory()
        try:
            autopilot = factory.create(json.loads(sys.argv[1]))
        except (RuntimeError, KeyError):
            success = False
        if not success:
            print(sys.argv[0] + " couldn't initialize")
        else:
            bridge = AutopilotBridge.AutopilotBridge(autopilot)
            bridge.run()
            print(sys.argv[0] + " exiting peacefully.")
            sys.stderr.write(sys.argv[0] + " flushing stderr\n")

/*
 * TimerHlc.cpp
 *
 *  Created on: Jul 17, 2017
 *      Author: dave
 */

#include "TimerHlc.h"

#include <iostream>
#include <boost/bind.hpp>

// ---------------------------------------------------------------------------------

TimerHlc::TimerHlc(unsigned int unThreadcount):m_pStateMachine(NULL),m_deadline_timer(m_io),m_work(m_io)
{
	for (unsigned int i(0); i < unThreadcount; i++)
	{
		m_thread_group.create_thread(boost::bind(&boost::asio::io_service::run,&m_io));
	}
}

// ---------------------------------------------------------------------------------

TimerHlc::~TimerHlc()
{
	m_io.stop();
	m_thread_group.join_all();
}

// ---------------------------------------------------------------------------------

void TimerHlc::Set(unsigned int unMilliSeconds)
{
	m_deadline_timer.expires_from_now(boost::posix_time::milliseconds(unMilliSeconds));
	m_deadline_timer.async_wait(boost::bind(&TimerHlc::HandleTimeout, this, boost::asio::placeholders::error));
}

// ---------------------------------------------------------------------------------

void TimerHlc::Abort()
{
	m_deadline_timer.cancel();
	/*
	int nCancelled(0);
	nCancelled = m_deadline_timer.cancel();
	std::cout << "TimerHlc::Abort cancelled " << nCancelled << " operations" << std::endl;
	*/
}

// ---------------------------------------------------------------------------------

void TimerHlc::HandleTimeout(const boost::system::error_code &error)
{
	//std::cout << "TimerHlc::HandleTimeout: code: " << error.message() << std::endl;
	if(boost::asio::error::operation_aborted != error)
	{
		//std::cout << "TimerHlc::HandleTimeout: HandleTimeout" << std::endl;
		m_pStateMachine->PostTimeout();
	}/*
	else
	{
		std::cout << "TimerHlc::HandleTimeout: Aborted!" << std::endl;
	}*/
}

/*
 * Autopilot.h
 *
 *  Created on: Jul 16, 2019
 *      Author: dave
 */

#ifndef _AUTOPILOT_H_
#define _AUTOPILOT_H_

#include "HlcStateMachine.h"
#include "InterfaceAutopilot.h"
#include <boost/process/system.hpp>
#include <boost/thread/thread.hpp>
#include <boost/asio.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <string>
#include <NeMALA/Publisher.h>
#include <StringMessage.h>

//--------------------------------------------------------
// 						Class Autopilot
//--------------------------------------------------------

/*
 * A base class to derive concrete autopilots from:
 */

class Autopilot : public InterfaceAutopilot
{
	//-------------------------- Methods ------------------------------------

public:

	Autopilot(NeMALA::Publisher* pLlcStatusPublisher, NeMALA::Publisher* pLlcErrorPublisher,
			boost::property_tree::ptree &ptArgs);
	virtual ~Autopilot(){StopHandling();}

	void SetHlcStateMachine(HlcStateMachine* pStateMachine){m_pHlcStateMachine = pStateMachine;}

	void Shutdown();

	int operator()();

	// ----------------------------InterfaceAutopilot ---------------

	void GetReadyForTakeoff();
	void Arm();
	void Takeoff();
	void Land();
	void ReturnToLaunch();
	void SetVelocityBodyFrame(double dForward, double dRight, double dDown);
	void SetYawRate(double dDegPerSec);
	double GetTakeoffAltitude(){return m_dTargetAltitude;}

private:

	// Post commands for the worker thread
	void PostStateCommand(InterfaceAutopilot::AutopilotActionEnum eState);
	void PostTakeoff();
	void PostVelocityBodyFrame(double dForward, double dRight, double dDown);
	void PostYawRate(double dDegPerSec);

	// Shuts down the worker thread and cleans up. Irreversible.
	void StopHandling();

	void HandleErrorMessages();

	//-------------------------- Variables ------------------------------------

private:

	NeMALA::Publisher* m_pLlcErrorPublisher;
	NeMALA::Publisher* m_pLlcStatusPublisher;
	double m_dTargetAltitude;
	double m_dLowBatVoltage;
	HlcStateMachine* m_pHlcStateMachine;
	std::string m_strToken;
	std::string m_strExecutable;
	std::string m_strArgs;

	boost::filesystem::path m_pathPythonExecutable;
	boost::asio::io_service m_io_service;
	boost::asio::io_service::work m_work;
	boost::process::opstream m_opstream;
	boost::process::ipstream m_ipstream;
	boost::thread* m_pThreadWriteToLlc;
	boost::thread* m_pThreadReadFromLlcErr;
};

#endif /* _AUTOPILOT_H_ */

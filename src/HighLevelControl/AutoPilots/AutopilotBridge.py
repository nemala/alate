#!/usr/bin/env python

###################################################################################
# File created by David Dovrat, Aug 1, 2019.
# For NeMALA Alate, under that project's license.
# The code in this file is provided "as is" and comes with no warranty whatsoever.
###################################################################################

import sys
import threading
import time
import re

###################################################################################
# ------------------------- AutopilotBridge ----------------------------------
###################################################################################


class AutopilotBridge(object):
    """
    The AutopilotBridge's job is to mediate between the autopilot and the HLC
    """

    def __init__(self, autopilot):
        self._str_token = "nemala"
        self._compiled_re = re.compile(self._str_token)
        self.autopilot = autopilot
        self.status_publisher = threading.Thread(target=self.vehicle_status)

    def _shutdown(self):
        self.status_publisher.join()

    def run(self):
        """
        feed input lines from the HLC to interpreter until shutdown.
        """
        self.status_publisher.start()
        while self.autopilot.is_active():
            line = sys.stdin.readline()
            try:
                self.interpret_command(line)
            except Exception:
                print(sys.argv[0] + " something went wrong with the interpreter... ")
                self.autopilot.shutdown()
        self._shutdown()

    def vehicle_status(self):
        """
        feed the autopilot feedback back to the HLC
        """
        while self.autopilot.is_active():
            print(self._str_token + self.autopilot.print_status(), flush=True)
            time.sleep(1)

    def interpret_command(self, str_command):
        """
        translate HLC strings to AutoPilot commands
        :param str_command: self._str_token + opcode + args
        """
        enum_command = self._compiled_re.search(str_command)
        if enum_command:
            str_command = str_command[len(enum_command.group(0)):]
            tup_args = str_command.split()
            num_command = int(tup_args[0])
            if (0 == num_command):
                # shutdown
                self.autopilot.shutdown()
            elif (1 == num_command):
                # Prepare for takeoff
                self.autopilot.ready2takeoff()
            elif (2 == num_command):
                # Arm
                self.autopilot.arm()
            elif (3 == num_command):
                # Takeoff
                self.autopilot.takeoff(float(tup_args[1]))
            elif (4 == num_command):
                # Land
                self.autopilot.land()
            elif (5 == num_command):
                # Body Frame Velocity Command
                self.autopilot.set_velocity_body_frame(float(tup_args[1]), float(tup_args[2]), float(tup_args[3]))
            elif (6 == num_command):
                # Yaw Rate
                self.autopilot.set_yaw_rate(float(tup_args[1]))
            elif (7 == num_command):
                # RTL
                self.autopilot.rtl()
            else:
                print("AutopilotBridge:: unknown command")
        else:
            print(str_command)

# Test script
if __name__ == "__main__":
    print("TODO: test script for the AutoPilotBridge")

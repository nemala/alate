#!/usr/bin/env python

###################################################################################
# File created by David Dovrat, Jul 29, 2019.
# For NeMALA Alate, under that project's license.
# The code in this file is provided "as is" and comes with no warranty whatsoever.
###################################################################################

import sys
import threading
import time

if __name__ == "__main__":
    # When running the test script
    from AbstractAutopilot import AbstractAutopilot 
else:
    # When importing as a module
    from .AbstractAutopilot import AbstractAutopilot  

import socket


###################################################################################
# --------------------------- AutopilotTello ------------------------------------
###################################################################################

class AutopilotTello(AbstractAutopilot):
    """
    The AutopilotTello class is an abstraction of a DJI tello autopilot
    """
    def __init__(self, network_interface, enable_camera = False):
        super(AutopilotTello, self).__init__()
        # we have more than one thread reading and writing to the same socket.
        self._lock = threading.Lock()

        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._socket.setsockopt(socket.SOL_SOCKET, 2, network_interface.encode('utf-8'))
        self._socket.setblocking(0)
        self._socket.bind(('', 8889))
        self._tello_address = ('192.168.10.1', 8889)

        self._socket_status = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._socket_status.setsockopt(socket.SOL_SOCKET, 2, network_interface.encode('utf-8'))
        self._socket_status.setblocking(0)
        self._socket_status.bind(('', 8890))

        self.rc = {
            'roll': '0',
            'pitch': '0',
            'yaw': '0',
            'thrust': '0'
        }
        
        self._command_counter = 0
        self.enable_camera_command = 'streamoff'
        if enable_camera:
            self.enable_camera_command = 'streamon'

        self.rc_command_sender = threading.Thread(target=self._send_rc_command)
        self.rc_command_sender.start()

        try:
            self._send_command('command')
        except:
            print(sys.exc_info()[0])

        try:
            self._send_command(self.enable_camera_command)
        except:
            print(sys.exc_info()[0])

    def _send_command(self, command, debug=False):
        self._lock.acquire()  # ---------------------------- Lock -------------------------------------
        try:
            self._socket.sendto(command.encode('utf-8'), self._tello_address)
        except IOError:
            pass
        self._lock.release()  # ------------------------------- Release ------------------------------------
        time.sleep(0.2)
        if debug:
            try:
                response = self._socket.recv(1024)
            except IOError:
                return
            print('tello response to ' + command + ': ' + response)

    def _send_rc_command(self):
        while self.is_active():
            if self._status['armed']:
                self._send_command(
                    'rc ' + self.rc['roll'] + ' ' + self.rc['pitch'] + ' ' + self.rc['thrust'] + ' ' + self.rc['yaw'])
            else:
                if (self._command_counter > 10):
                    self._command_counter = 0
                    self._send_command(self.enable_camera_command)
                else:
                    self._command_counter = self._command_counter + 1
                    self._send_command('command')
            time.sleep(0.5)

    ###################################################################################
    # ----------------------- AbstractAutopilot Interface Methods --------------------#
    ###################################################################################

    def shutdown(self):
        super(AutopilotTello, self).shutdown()
        self._lock.acquire()  # ---------------------------- Lock -------------------------------------
        self.rc_command_sender.join()
        self._socket.close()
        self._lock.release()  # ------------------------------- Release ------------------------------------
        self._socket_status.close()

    def ready2takeoff(self):
        super(AutopilotTello, self).ready2takeoff()

    def arm(self):
        super(AutopilotTello, self).arm()

    def takeoff(self, dAltitude=1):
        self._send_command('takeoff')
        super(AutopilotTello, self).takeoff()

    def land(self):
        self._send_command('land')
        super(AutopilotTello, self).land()

    def rtl(self):
        self._send_command('land')
        super(AutopilotTello, self).rtl()

    def set_velocity_body_frame(self, forward, right, down):
        self.rc['pitch'] = str(forward*100)
        self.rc['roll'] = str(right*100)
        self.rc['thrust'] = str(-down*100)

    def set_yaw_rate(self, rate):
        self.rc['yaw'] = str(rate)

    def print_status(self):
        received_all = False
        result = ''
        while not received_all:
            try:
                str_status = self._socket_status.recv(1024)
                result = str_status
            except IOError:
                received_all = True
        if '' != result:
            str_status = result.decode('UTF-8').replace(':', ' ')
            str_status = str_status.replace(';', ' ')
            state = str_status.split()
            state = dict(zip(state[::2], state[1::2]))
            self._status['altitude'] = state['h']
            self._status['battery_voltage'] = state['bat']
            self._status['yaw'] = state['yaw']
        return super(AutopilotTello, self).print_status()


###################################################################################

# Test Script
if __name__ == "__main__":
    autopilot = AutopilotTello('TELLO-C4E0FA')
    print(autopilot.print_status())
    time.sleep(5)
    print(autopilot.print_status())
    autopilot.ready2takeoff()
    print(autopilot.print_status())
    autopilot.arm()
    print(autopilot.print_status())
    autopilot.takeoff()
    print(autopilot.print_status())
    time.sleep(5)
    print(autopilot.print_status())
    autopilot.set_velocity_body_frame(0.5, 0.5, -0.5)
    time.sleep(2)
    print(autopilot.print_status())
    autopilot.set_velocity_body_frame(-0.2, -0.2, 0.0)
    time.sleep(2)
    print(autopilot.print_status())
    autopilot.set_velocity_body_frame(0, 0, 0.0)
    time.sleep(2)
    print(autopilot.print_status())
    autopilot.land()
    time.sleep(5)
    print(autopilot.print_status())
    autopilot.shutdown()
    time.sleep(5)


#!/usr/bin/env python

###################################################################################
# File created by David Dovrat, Jul 23, 2017.
# For an AerialSwarm project, under that project's license of choice
# The code in this file is provided "as is" and comes with no warranty whatsoever
###################################################################################

import sys
import threading
import time
from dronekit import connect, VehicleMode, mavutil

if __name__ == "__main__":
    # When running the test script
    from AbstractAutopilot import AbstractAutopilot 
else:
    # When importing as a module
    from .AbstractAutopilot import AbstractAutopilot 

###################################################################################
# --------------------------- AutopilotDronekit ------------------------------------
###################################################################################

class AutopilotDronekit(AbstractAutopilot):
    """
        The AutopilotDronekit class is an abstraction of a dronekit autopilot
    """

    def __init__(self, target_address, baud_rate):
        """
        Establish connection to the autopilot
        """
        super(AutopilotDronekit, self).__init__()
        # A lock for guarding the vehicle since it handles its own connection
        self._lock = threading.Lock()
        connected = False
        time_start = time.time()
        self._lock.acquire()  # ------------------------------------ Lock -------------------------------------
        while not connected:
            try:
                self._Vehicle = connect(target_address, baud=baud_rate, wait_ready=True)
                connected = True
            except:
                print(sys.exc_info()[0])
                if (time.time() - time_start) < 60:
                    continue
                else:
                    break
        self._lock.release()  # ---------------------------------- Release ------------------------------------
        if not connected:
            raise RuntimeError("AutopilotDronekit.py failed to establish Vehicle Communication.")

    ###################################################################################
    # ----------------------- AbstractAutopilot Interface Methods --------------------#
    ###################################################################################

    def shutdown(self):
        self._lock.acquire()  # ------------------------------------ Lock -------------------------------------
        self._Vehicle.close()
        self._lock.release()  # ---------------------------------- Release ------------------------------------
        super(AutopilotDronekit, self).shutdown()

    def ready2takeoff(self):
        self._lock.acquire()  # ------------------------------------ Lock -------------------------------------
        self._Vehicle.mode = VehicleMode("GUIDED")
        self._lock.release()  # ---------------------------------- Release ------------------------------------

    def arm(self):
        self._lock.acquire()  # ------------------------------------ Lock -------------------------------------
        self._Vehicle.armed = True
        self._lock.release()  # ---------------------------------- Release ------------------------------------

    def takeoff(self, dAltitude):
        self._lock.acquire()  # ------------------------------------ Lock -------------------------------------
        self._Vehicle.simple_takeoff(dAltitude)
        self._lock.release()  # ---------------------------------- Release ------------------------------------

    def land(self):
        self._lock.acquire()  # ------------------------------------ Lock -------------------------------------
        self._Vehicle.mode = VehicleMode("LAND")
        self._lock.release()  # ---------------------------------- Release ------------------------------------

    def rtl(self):
        self._lock.acquire()  # ------------------------------------ Lock -------------------------------------
        self._Vehicle.mode = VehicleMode("RTL")
        self._lock.release()  # ---------------------------------- Release ------------------------------------

    def set_velocity_body_frame(self, forward, right, down):
        # print(dForward)
        msg = self._Vehicle.message_factory.set_position_target_local_ned_encode(
            0,  # time_boot_ms (not used)
            0, 0,  # target system, target component
            mavutil.mavlink.MAV_FRAME_BODY_OFFSET_NED,  # frame
            0b0000111111000111,  # type_mask (only speeds enabled)
            0, 0, 0,  # x, y, z positions (not used)
            forward, right, down,  # x, y, z velocity in m/s
            0, 0, 0,  # x, y, z acceleration (not supported yet, ignored in GCS_Mavlink)
            0, 0)  # yaw, yaw_rate (not supported yet, ignored in GCS_Mavlink)
        self._lock.acquire()  # ------------------------------------ Lock -------------------------------------
        self._Vehicle.send_mavlink(msg)
        self._lock.release()  # ---------------------------------- Release ------------------------------------

    def set_yaw_rate(self, deg_per_sec):
        # print(dDegPerSec)
        rotation_direction = 1
        if (0 > deg_per_sec):
            deg_per_sec = -deg_per_sec
            rotation_direction = -rotation_direction
        msg = self._Vehicle.message_factory.command_long_encode(
            0, 0,  # target system, target component
            mavutil.mavlink.MAV_CMD_CONDITION_YAW,  # command
            0,  # confirmation
            deg_per_sec,  # param 1, yaw in degrees
            0,  # param 2, yaw speed deg/s
            rotation_direction,  # param 3, direction -1 ccw, 1 cw
            1,  # param 4, relative offset 1, absolute angle 0
            0, 0, 0)  # param 5 ~ 7 not used
        self._lock.acquire()  # ------------------------------------ Lock -------------------------------------
        self._Vehicle.send_mavlink(msg)
        self._lock.release()  # ---------------------------------- Release ------------------------------------

    def print_status(self):
        self._lock.acquire()  # ------------------------------------ Lock -------------------------------------
        self._status['mode'] = self._Vehicle.mode.name
        self._status['armed'] = self._Vehicle.armed
        battery_voltage = self._Vehicle.battery.voltage
        if (None == battery_voltage):
            battery_voltage = 0
        self._status['battery_voltage'] = battery_voltage
        gps_hdop = self._Vehicle.gps_0.eph
        gps_fix = self._Vehicle.gps_0.fix_type
        self._status['gps_fix'] = gps_fix
        if (None == gps_hdop):
            gps_hdop = 9898
        self._status['gps_hdop'] = gps_hdop
        self._status['armable'] = self._Vehicle.is_armable
        self._status['altitude'] = self._Vehicle.location.global_relative_frame.alt
        self._status['lat'] = self._Vehicle.location.global_relative_frame.lat
        self._status['lon'] = self._Vehicle.location.global_relative_frame.lon
        self._status['yaw'] = self._Vehicle.attitude.yaw
        self._status['state'] = self._Vehicle.system_status.state
        self._lock.release()  # ---------------------------------- Release ------------------------------------
        return super(AutopilotDronekit, self).print_status()

###################################################################################

# Test Script
if __name__ == "__main__":
    autopilot = AutopilotDronekit("tcp:127.0.0.1:14552", "921600")
    print(autopilot.print_status())
    time.sleep(1)
    print(autopilot.print_status())
    autopilot.ready2takeoff()
    time.sleep(2)
    print(autopilot.print_status())
    autopilot.arm()
    time.sleep(2)
    print(autopilot.print_status())
    autopilot.takeoff(30)
    time.sleep(5)
    print(autopilot.print_status())
    autopilot.set_velocity_body_frame(5, 5, -5)
    time.sleep(2)
    print(autopilot.print_status())
    autopilot.set_velocity_body_frame(-2, -2, 0)
    time.sleep(2)
    print(autopilot.print_status())
    autopilot.set_velocity_body_frame(0, 0, 0)
    time.sleep(2)
    print(autopilot.print_status())
    autopilot.land()
    time.sleep(5)
    print(autopilot.print_status())
    autopilot.shutdown()
    time.sleep(5)

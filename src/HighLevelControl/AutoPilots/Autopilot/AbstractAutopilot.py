#!/usr/bin/env python

###################################################################################
# File created by David Dovrat, Aug 4, 2019.
# For NeMALA Alate, under that project's license.
# The code in this file is provided "as is" and comes with no warranty whatsoever.
###################################################################################

from abc import ABCMeta, abstractmethod
import json

###################################################################################
# ----------------------------- AbstractAutopilot ---------------------------------
###################################################################################


class AbstractAutopilot(object):
    """
    The AbstractAutopilot is an abstract class enforcing implementation of the autopilot interface.

    I used the dronekit api as a first implementation. The dronekit API has modes and states that provide insights
    into what goes wrong when trying to takeoff, as well as allowing the user application (Alate) to take small steps
    towards becoming airborne. Arming the motors for instance is a big deal when you're flying heavy drones that can
    chop off your finger if you make a mistake.

    These modes, states, and other values I think are important are maintained in this class' _status property.
    It lies within the user's responsibility to update the status.

    The second implementation I used was tello API. As a small and easy-to-use drone it doesn't expose its internal
    states and modes (if it has them at all), and with no GPS, all the gps safety oriented motor-arming preconditions are
    downright unnecessary, but I kept them in order to avoid an indoor/outdoor split of this interface.

    The result is the interface you see here. My hope is that it is useful enough for inclusion of many autopilots I
    haven't yet considered.

    All internal mechanisms required for smooth operation with the 'Alate' framework are implemented here, I leave it to
    future users to decide whether to call super() or not depending on the actual autopilot they are using.

    See AutopilotDronekit.py and AutopilotTello.py for examples of the two very different approaches contained under
    this interface.
    """
    __metaclass__ = ABCMeta

    def __init__(self):
        self._active = True
        self._status = {
            'mode': 'NONE',
            'armed': False,
            'altitude': 0,
            'battery_voltage': 0,
            'gps_fix': 0,
            'gps_hdop': 0,
            'armable': 0,
            'state': 'NONE',
            'lat': 0,
            'lon': 0,
            'yaw': 0
        }

    def is_active(self):
        """
        An autopilot initializes active, and remains active until being shutdown.
        This mechanism is entirely contained within the relationship between Autopilot and AutopilotBridge, and should
        not concern derived classes.
        Just be careful about overriding.
        :return: boolean value of 'active'
        """
        return self._active

    # -------------------------------- Abstract Methods ------------------------------

    @abstractmethod
    def shutdown(self):
        """
        Use this method to clean-up.
        """
        self._active = False

    @abstractmethod
    def ready2takeoff(self):
        """
        Make preparations for taking off.
        This base implementation is for autopilots that don't really have the below status fields (like the tello API).
        For those that do (dronekit), this method is best overridden and left in the qualified hands of the
        API encapsulation.
        See AutopilotDronekit.py and AutopilotTello.py for examples.
        """
        self._status['mode'] = "GUIDED"
        self._status['gps_fix'] = 10
        self._status['gps_hdop'] = 100
        self._status['armable'] = True
        self._status['state'] = "STANDBY"

    @abstractmethod
    def arm(self):
        """
        Arm motors.
        In models that don't have this option (like the Tello), just call this method in the derived class using super().
        """
        self._status['armed'] = True

    @abstractmethod
    def takeoff(self, dAltitude=1):
        self._status['state'] = "ACTIVE"

    @abstractmethod
    def land(self):
        self._status['mode'] = "LAND"
        self._status['armed'] = False

    @abstractmethod
    def rtl(self):
        self._status['mode'] = "RTL"
        self._status['armed'] = False

    @abstractmethod
    def set_velocity_body_frame(self, forward, right, down):
        pass

    @abstractmethod
    def set_yaw_rate(self, rate):
        pass

    @abstractmethod
    def print_status(self):
        """
        Used by the Autopilot bridge to communicate the autopilot's status to the Alate framework.
        Derived classes should do their best to populate the self._status property.
        :return: the self._status property in json format.
        """
        return json.dumps(self._status, sort_keys=False)


# Test script
if __name__ == "__main__":
    print("TEST ME")


#!/usr/bin/env python

###################################################################################
# File created by David Dovrat, Aug 2, 2019.
# For NeMALA Alate, under that project's license.
# The code in this file is provided "as is" and comes with no warranty whatsoever.
###################################################################################

###################################################################################
# ------------------------- Dependencies (EXPLICIT) -------------------------------
###################################################################################


if __name__ == "__main__":
    # When running the test script
    from Autopilot import AutopilotTello
    from Autopilot import AutopilotDronekit
else:
    # When importing as a module
    from .Autopilot import AutopilotTello
    from .Autopilot import AutopilotDronekit  


###################################################################################
# --------------------------- AutopilotFactory ------------------------------------
###################################################################################


class AutopilotFactory(object):
    """
    A factory for autopilots!
    import your own implementation of the Autopilot abstract class,
    and add an elif case to the autopilot method.
    """
    def __init__(self):
        pass

    def create(self, dict_config):
        """
        create an autopilot according to configuration.
        :param dict_config: a configuration dictionary
        :return: an autopilot object of the requested 'type'
        """
        if ('tello' == dict_config['type']):
            return AutopilotTello.AutopilotTello(dict_config["network interface"], ('true' == dict_config["enable_camera"]))
        elif ('dronekit' == dict_config['type']):
            return AutopilotDronekit.AutopilotDronekit(dict_config["master"], dict_config["baud"])
        else:
            raise KeyError(dict_config)


# Test script
if __name__ == "__main__":
    factory = AutopilotFactory()
    autopilot = factory.create({'type': 'tello', 'network interface': 'TELLO-C4E0FA', 'enable_camera': 'false'})
    print(autopilot.print_status())
    autopilot.shutdown()
    autopilot = factory.create({'type': 'dronekit', "master": "tcp:127.0.0.1:14552", "baud": "921600"})
    print(autopilot.print_status())
    autopilot.shutdown()

/*
 * AppMessage.h
 *
 *  Created on: Nov 24, 2021
 *      Author: dave
 */

#ifndef _MESSAGE_CLASSES_APPLICATION_MESSAGE_H_
#define _MESSAGE_CLASSES_APPLICATION_MESSAGE_H_

#include "OpMessage.h"
#include <NeMALA/MessagePropertyTree.h>

//--------------------------------------------------------
// 						Class AppMessage
//--------------------------------------------------------

/*
 * A message class encapsulating the operator command and application op-codes and status reports:
 */
class AppMessage : public NeMALA::MessagePropertyTree
{
public:

	//-------------- Constants --------------------
	typedef enum
	{
		AppSuccess = OpMessage::OpCommandEnum::OpCommandNone,
		AppStart = OpMessage::OpCommandEnum::OpCommandTakeoff,
		AppStop = OpMessage::OpCommandEnum::OpCommandLand,
		AppSuccessfulTermination = OpMessage::OpCommandEnum::OpCommandGoHome,
		AppFail = OpMessage::OpCommandEnum::OpCommandAux
	}AppStatusEnum;


	//-------------------------- Methods ------------------------------------
	/*
	 * Init class variables.
	 * Publishers should use this function.
	 */
	AppMessage(AppStatusEnum eCommand):m_eAppCode(eCommand){}

	/*
	 * Build a message from a property tree.
	 * Subscribers should use this function.
	 * @param pt: The property tree received from a publisher which translated to message.
	 */
	AppMessage(NeMALA::Proptree pt):
		m_eAppCode((AppStatusEnum)pt.get<double>("Command")){}

	~AppMessage(){}

	/*
	 * Synchronize the private members of this class
	 */
	void BuildMessage()
	{
		AddElementNumber("Command", m_eAppCode);
	}

	/*
	 * Getters in order to return the member variables of this class.
	 * The subscribers use this function.
	 */
	AppMessage GetValue() {return m_eAppCode;}

	void SetValue(AppStatusEnum eCommand) {m_eAppCode = eCommand;}

private:
	 //-------------- Variables --------------------
	AppStatusEnum m_eAppCode;
};

#endif /* _MESSAGE_CLASSES_APPLICATION_MESSAGE_H_ */

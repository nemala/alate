/*
 * HlcStateMessage.h
 *
 *  Created on: Jul 16, 2017
 *      Author: dave
 */

#ifndef _MESSAGE_CLASSES_HLC_STATE_MESSAGE_H_
#define _MESSAGE_CLASSES_HLC_STATE_MESSAGE_H_

#include <NeMALA/MessagePropertyTree.h>

//--------------------------------------------------------
// 					Class HlcStateMessage
//--------------------------------------------------------

/*
 * A message class depicting the High Level Control Module (HLC) states:
 */
class HlcStateMessage : public NeMALA::MessagePropertyTree
{
public:

	//-------------- Constants --------------------
	typedef enum
	{
		HlcStateNone=0,
		HlcStateInit,
		HlcStateWaitingForMc,
		HlcStateWaitingForLlc,
		HlcStateReady,
		HlcStateTakeoff,
		HlcStateGainingAltitude,
		HlcStateAirborne,
		HlcStateLanding,
		HlcStateManual,
		HlcStateRTL,
		HlcStateNoError,
		HlcStateLowBattery,
		HlcStateLlcDown
	}HlcStateEnum;


	//-------------------------- Methods ------------------------------------
	/*
	 * Init class variables.
	 * Publishers should use this function.
	 */
	HlcStateMessage(HlcStateEnum eState):m_eState(eState){}

	/*
	 * Build a message from a property tree.
	 * Subscribers should use this function.
	 * @param pt: The property tree received from a publisher which translated to message.
	 */
	HlcStateMessage(NeMALA::Proptree pt):
		m_eState((HlcStateEnum)pt.get<double>("State")){}

	~HlcStateMessage(){}

	/*
	 * Synchronize the private members of this class
	 */
	void BuildMessage()
	{
		AddElementNumber("State", m_eState);
	}

	/*
	 * Getters in order to return the member variables of this class.
	 * The subscribers use this function.
	 */
	HlcStateEnum GetValue() const {return m_eState;}


	std::string GetName()
	{
		std::string strResult;
		switch(m_eState)
		{
			case HlcStateNone:
				strResult = "None";
				break;
			case HlcStateInit:
				strResult = "Init";
				break;
			case HlcStateWaitingForMc:
				strResult = "WaitingForMc";
				break;
			case HlcStateWaitingForLlc:
				strResult = "WaitingForLlc";
				break;
			case HlcStateReady:
				strResult = "Ready";
				break;
			case HlcStateTakeoff:
				strResult = "Takeoff";
				break;
			case HlcStateGainingAltitude:
				strResult = "GainingAltitude";
				break;
			case HlcStateAirborne:
				strResult = "Airborne";
				break;
			case HlcStateLanding:
				strResult = "Landing";
				break;
			case HlcStateManual:
				strResult = "Manual";
				break;
			case HlcStateRTL:
				strResult = "Return to Launch";
				break;
			case HlcStateNoError:
				strResult = "No Error";
				break;
			case HlcStateLowBattery:
				strResult = "Low Battery";
				break;
			case HlcStateLlcDown:
				strResult = "LLC Down";
				break;
			default:
				strResult = "Unknown State!";
				break;
		}
		return strResult;
	}

private:
	 //-------------- Variables --------------------
	HlcStateEnum m_eState;
};

#endif /* _MESSAGE_CLASSES_HLC_STATE_MESSAGE_H_ */

/*
 * VelocityMessage.h
 *
 *  Created on: Aug 6, 2017
 *      Author: dave
 */

#ifndef _MESSAGE_CLASSES_VELOCITY_MESSAGE_H_
#define _MESSAGE_CLASSES_VELOCITY_MESSAGE_H_

#include "Vector6Message.h"

//--------------------------------------------------------
// 					Class VelocityMessage
//--------------------------------------------------------

/*
 * A message class depicting 6 DOF velocity:
 */
class VelocityMessage : public Vector6Message
{
public:

	//-------------------------- Methods ------------------------------------
	/*
	 * Init class variables.
	 * Publishers should use this function.
	 */
	VelocityMessage(double dX, double dY, double dZ, double dYaw, double dPitch, double dRoll):
		Vector6Message(dX, dY, dZ, dYaw, dPitch, dRoll){}

	/*
	 * Build a message from a property tree.
	 * Subscribers should use this function.
	 * @param pt: The property tree received from a publisher which translated to message.
	 */
	VelocityMessage(NeMALA::Proptree pt): Vector6Message(pt){}

	~VelocityMessage(){}
};

#endif /* _MESSAGE_CLASSES_VELOCITY_MESSAGE_H_ */

/*
 * StringMessage.h
 *
 *  Created on: Mar 8, 2017
 *      Author: dave
 */

#ifndef _MESSAGE_CLASSES_STRING_MESSAGE_H_
#define _MESSAGE_CLASSES_STRING_MESSAGE_H_

#include <NeMALA/MessagePropertyTree.h>

//--------------------------------------------------------
// 						Class StringMessage
//--------------------------------------------------------

/*
 * A message class containing a string:
 */
class StringMessage : public NeMALA::MessagePropertyTree
{
public:

	//-------------------------- Methods ------------------------------------
	/*
	 * Init class variables.
	 * Publishers should use this function.
	 */
	StringMessage(std::string str):m_str(str){}

	/*
	 * Build a message from a property tree.
	 * Subscribers should use this function.
	 * @param pt: The property tree received from a publisher which translated to message.
	 */
	StringMessage(NeMALA::Proptree pt):
		m_str(pt.get<std::string>("content")){}

	~StringMessage(){}

	/*
	 * Synchronize the private members of this class
	 */
	void BuildMessage()
	{
		AddElementText("content", m_str);
	}

	/*
	 * Getters in order to return the member variables of this class.
	 * The subscribers use this function.
	 */
	std::string GetContent() const {return m_str;}


private:
	 //-------------- Variables --------------------
	std::string m_str;
};

#endif /* _MESSAGE_CLASSES_STRING_MESSAGE_H_ */

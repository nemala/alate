/*
 * OpMessage.h
 *
 *  Created on: Jul 5, 2017
 *      Author: dave
 */

#ifndef _MESSAGE_CLASSES_OPERATOR_MESSAGE_H_
#define _MESSAGE_CLASSES_OPERATOR_MESSAGE_H_

#include <NeMALA/MessagePropertyTree.h>

//--------------------------------------------------------
// 						Class OpMessage
//--------------------------------------------------------

/*
 * A message class encapsulating an operator command:
 */
class OpMessage : public NeMALA::MessagePropertyTree
{
public:

	//-------------- Constants --------------------
	typedef enum
	{
		OpCommandNone=0,
		OpCommandTakeoff,
		OpCommandLand,
		OpCommandGoHome,
		OpCommandSetDirection,
		OpCommandAux
	}OpCommandEnum;


	//-------------------------- Methods ------------------------------------
	/*
	 * Init class variables.
	 * Publishers should use this function.
	 */
	OpMessage(OpCommandEnum eCommand):m_eCommand(eCommand){}

	/*
	 * Build a message from a property tree.
	 * Subscribers should use this function.
	 * @param pt: The property tree received from a publisher which translated to message.
	 */
	OpMessage(NeMALA::Proptree pt):
		m_eCommand((OpCommandEnum)pt.get<double>("Command")){}

	~OpMessage(){}

	/*
	 * Synchronize the private members of this class
	 */
	void BuildMessage()
	{
		AddElementNumber("Command", m_eCommand);
	}

	/*
	 * Getters in order to return the member variables of this class.
	 * The subscribers use this function.
	 */
	OpCommandEnum GetValue() {return m_eCommand;}

private:
	 //-------------- Variables --------------------
	OpCommandEnum m_eCommand;
};

#endif /* _MESSAGE_CLASSES_OPERATOR_MESSAGE_H_ */

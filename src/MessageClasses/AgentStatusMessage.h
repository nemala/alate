/*
 * AgentStatusMessage.h
 *
 *  Created on: Mar 21, 2018
 *      Author: dave
 */

#ifndef _MESSAGE_CLASSES_AGENT_STATUS_MESSAGE_H_
#define _MESSAGE_CLASSES_AGENT_STATUS_MESSAGE_H_

#include <NeMALA/MessagePropertyTree.h>
#include <boost/lexical_cast.hpp>
#include "LlcStatusMessage.h"
#include "HlcStateMessage.h"
#include "McMessage.h"
#include "StringMessage.h"


//--------------------------------------------------------
// 					Class AgentStatusMessage
//--------------------------------------------------------

/*
 * A message class summing the agent's status
 */
class AgentStatusMessage : public NeMALA::MessagePropertyTree
{
public:

	//-------------------------- Methods ------------------------------------

	// Empty constructor
	AgentStatusMessage(){}

	/*
	 * Init class variables.
	 * Publishers should use this function.
	 */
	AgentStatusMessage(LlcStatusMessage::LlcStateStruct &llcStateStruct, std::string &strLlcMessage,
			std::string &strHlcState, std::string &strMcState, std::string &strFeedback):
			m_stLlcState(llcStateStruct), m_strLlcMessage(strLlcMessage), m_strHlcState(strHlcState),
			m_strMcState(strMcState), m_strFeedback(strFeedback){}

	/*
	 * Build a message from a property tree.
	 * Subscribers should use this function.
	 * @param pt: The property tree received from a publisher which translated to message.
	 */
	AgentStatusMessage(NeMALA::Proptree pt):
		m_strLlcMessage(pt.get<std::string>("LlcMessage")),
		m_strHlcState(pt.get<std::string>("HlcState")),
		m_strMcState(pt.get<std::string>("McState")),
		m_strFeedback(pt.get<std::string>("Feedback"))
	{
		LlcStatusMessage llcMsg(pt);
		m_stLlcState = llcMsg.GetLlcStatus();
	}

	~AgentStatusMessage(){}


	void SetHeader(std::string time, unsigned int topic) {NeMALA::MessagePropertyTree::ResetMessage(); NeMALA::BaseMessage::SetHeader(time,topic);}

	/*
	 * Synchronize the private members of this class
	 */
	void BuildMessage()
	{
		AddElement("Lat", boost::lexical_cast<std::string>(m_stLlcState.dLatitude));
		AddElement("Lon", boost::lexical_cast<std::string>(m_stLlcState.dLongitude));
		AddElement("Alt", boost::lexical_cast<std::string>(m_stLlcState.dAltitude));
		AddElement("Yaw", boost::lexical_cast<std::string>(m_stLlcState.dYaw));
		AddElement("Armed", boost::lexical_cast<std::string>(m_stLlcState.bArmed ? 1 : 0));
		AddElement("Volt", boost::lexical_cast<std::string>(m_stLlcState.dBatteryVoltage));
		AddElement("GpsFix", boost::lexical_cast<std::string>(m_stLlcState.nGpsFix));
		AddElement("GpsHdop", boost::lexical_cast<std::string>(m_stLlcState.dGpsHdop));
		AddElement("Mode", m_stLlcState.strMode);
		AddElement("LlcMessage", m_strLlcMessage);
		AddElement("LlcState", m_stLlcState.strState);
		AddElement("HlcState", m_strHlcState);
		AddElement("McState", m_strMcState);
		AddElement("Feedback", m_strFeedback);
	}

	/*
	 * Getters in order to return the member variables of this class.
	 * The subscribers use this function.
	 */
	LlcStatusMessage::LlcStateStruct GetLlcStatus(){return m_stLlcState;}
	std::string GetHlcState(){return m_strHlcState;}
	std::string GetMcState(){return m_strMcState;}
	std::string GetLlcMessage(){return m_strLlcMessage;}
	std::string GetFeedback(){return m_strFeedback;}

	/*
	 * Setters in order to set the member variables of this class.
	 * The publishers use this function.
	 */
	void SetLlcStatus(LlcStatusMessage::LlcStateStruct &stLlcStatus){m_stLlcState = stLlcStatus;}
	void SetHlcState(std::string strHlcState){m_strHlcState = strHlcState;}
	void SetMcState(std::string strMcState){m_strMcState = strMcState;}
	void SetLlcMessage(std::string strLlcMessage){m_strLlcMessage = strLlcMessage;}
	void SetFeedback(std::string strFeedback){m_strFeedback = strFeedback;}

private:
	 //-------------- Variables --------------------
	LlcStatusMessage::LlcStateStruct m_stLlcState;
	std::string m_strLlcMessage;
	std::string m_strHlcState;
	std::string m_strMcState;
	std::string m_strFeedback;
};

#endif /* _MESSAGE_CLASSES_AGENT_STATUS_MESSAGE_H_ */

/*
 * McMessage.h
 *
 *  Created on: Jul 13, 2017
 *      Author: dave
 */

#ifndef _MESSAGE_CLASSES_MC_MESSAGE_H_
#define _MESSAGE_CLASSES_MC_MESSAGE_H_

#include <NeMALA/MessagePropertyTree.h>

//--------------------------------------------------------
// 						Class McMessage
//--------------------------------------------------------

/*
 * A message class depicting the Mission Control Module (MCM) states:
 */
class McMessage : public NeMALA::MessagePropertyTree
{
public:

	//-------------- Constants --------------------
	typedef enum
	{
		McStateNone=0,
		McStateInit,
		McStateStandby,
		McStateTakingoff,
		McStatePerformingMission,
		McStateLanding,
		McStateManual,
		McStateReturnToLaunch,
		McStateNoError,
		McStateError
	}McStateEnum;


	//-------------------------- Methods ------------------------------------
	/*
	 * Init class variables.
	 * Publishers should use this function.
	 */
	McMessage(McStateEnum eState):m_eState(eState){}

	/*
	 * Build a message from a property tree.
	 * Subscribers should use this function.
	 * @param pt: The property tree received from a publisher which translated to message.
	 */
	McMessage(NeMALA::Proptree pt):
		m_eState((McStateEnum)pt.get<double>("State")){}

	~McMessage(){}

	/*
	 * Synchronize the private members of this class
	 */
	void BuildMessage()
	{
		AddElementNumber("State", m_eState);
	}

	/*
	 * Getters in order to return the member variables of this class.
	 * The subscribers use this function.
	 */
	McStateEnum GetValue() const {return m_eState;}

	std::string GetName()
	{
		std::string strResult;
		switch(m_eState)
		{
			case McStateNone:
				strResult = "None";
				break;
			case McStateInit:
				strResult = "Init";
				break;
			case McStateStandby:
				strResult = "Standby";
				break;
			case McStateTakingoff:
				strResult = "Takingoff";
				break;
			case McStatePerformingMission:
				strResult = "Mission";
				break;
			case McStateLanding:
				strResult = "Landing";
				break;
			case McStateManual:
				strResult = "Manual";
				break;
			case McStateReturnToLaunch:
				strResult = "Return to Launch";
				break;
			case McStateNoError:
				strResult = "No Error";
				break;
			case McStateError:
				strResult = "Error!";
				break;
			default:
				strResult = "Unknown State!";
				break;
		}
		return strResult;
	}

private:
	 //-------------- Variables --------------------
	McStateEnum m_eState;
};

#endif /* _MESSAGE_CLASSES_MC_MESSAGE_H_ */

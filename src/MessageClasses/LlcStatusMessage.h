/*
 * LlcStatusMessage.h
 *
 *  Created on: Mar 21, 2018
 *      Author: dave
 */

#ifndef _MESSAGE_CLASSES_LLC_STATUS_MESSAGE_H_
#define _MESSAGE_CLASSES_LLC_STATUS_MESSAGE_H_

#include <NeMALA/MessagePropertyTree.h>

//--------------------------------------------------------
// 					Class LlcStatusMessage
//--------------------------------------------------------

/*
 * A message class summing the agent's LLC status
 */
class LlcStatusMessage : public NeMALA::MessagePropertyTree
{
public:

	//-------------- Type Definitions --------------------

	typedef struct
	{
		double dLatitude;
		double dLongitude;
		double dAltitude;
		double dYaw;
		bool bArmed;
		double dBatteryVoltage;
		int nGpsFix;
		double dGpsHdop;
		std::string strMode;
		std::string strState;
	}LlcStateStruct;

	//-------------------------- Methods ------------------------------------
	/*
	 * Init class variables.
	 * Publishers should use this function.
	 */
	LlcStatusMessage(LlcStateStruct &llcStateStruct) : m_llcStateStruct(llcStateStruct){}

	/*
	 * Build a message from a property tree.
	 * Subscribers should use this function.
	 * @param pt: The property tree received from a publisher which translated to message.
	 */
	LlcStatusMessage(NeMALA::Proptree pt)
	{
		m_llcStateStruct.dLatitude = pt.get<double>("Lat");
		m_llcStateStruct.dLongitude = pt.get<double>("Lon");
		m_llcStateStruct.dAltitude = pt.get<double>("Alt");
		m_llcStateStruct.dYaw = pt.get<double>("Yaw");
		m_llcStateStruct.bArmed = (1 == pt.get<int>("Armed"));
		m_llcStateStruct.dBatteryVoltage = pt.get<double>("Volt");
		m_llcStateStruct.nGpsFix = pt.get<int>("GpsFix");
		m_llcStateStruct.dGpsHdop = pt.get<double>("GpsHdop");
		m_llcStateStruct.strMode = pt.get<std::string>("Mode");
		m_llcStateStruct.strState = pt.get<std::string>("State");
	}

	~LlcStatusMessage(){}

	/*
	 * Synchronize the private members of this class
	 */
	void BuildMessage()
	{
		AddElementNumber("Lat", m_llcStateStruct.dLatitude);
		AddElementNumber("Lon", m_llcStateStruct.dLongitude);
		AddElementNumber("Alt", m_llcStateStruct.dAltitude);
		AddElementNumber("Yaw", m_llcStateStruct.dYaw);
		AddElementNumber("Armed", m_llcStateStruct.bArmed ? 1 : 0);
		AddElementNumber("Volt", m_llcStateStruct.dBatteryVoltage);
		AddElementNumber("GpsFix", m_llcStateStruct.nGpsFix);
		AddElementNumber("GpsHdop", m_llcStateStruct.dGpsHdop);
		AddElementText("Mode", m_llcStateStruct.strMode);
		AddElementText("State", m_llcStateStruct.strState);
	}

	/*
	 * Getters in order to return the member variables of this class.
	 * The subscribers use this function.
	 */
	LlcStateStruct GetLlcStatus(){return m_llcStateStruct;}

	/*
	 * Setters in order to set the member variables of this class.
	 * The publishers use this function.
	 */
	void SetLlcStatus(LlcStateStruct llcStateStruct){m_llcStateStruct = llcStateStruct;}

private:
	 //-------------- Variables --------------------
	LlcStateStruct m_llcStateStruct;
};

#endif /* _MESSAGE_CLASSES_LLC_STATUS_MESSAGE_H_ */

/*
 * FloatMessage.h
 *
 *  Created on: May 2, 2017
 *      Author: dave
 */

#ifndef _MESSAGE_CLASSES_FLOAT_MESSAGE_H_
#define _MESSAGE_CLASSES_FLOAT_MESSAGE_H_

#include <NeMALA/MessagePropertyTree.h>

//--------------------------------------------------------
// 						Class FloatMessage
//--------------------------------------------------------

/*
 * A message class encapsulating a floating point representation of a real number:
 */
class FloatMessage : public NeMALA::MessagePropertyTree
{
public:

	//-------------------------- Methods ------------------------------------
	/*
	 * Init class variables.
	 * Publishers should use this function.
	 */
	FloatMessage(double dValue):m_dValue(dValue){}

	/*
	 * Build a message from a property tree.
	 * Subscribers should use this function.
	 * @param pt: The property tree received from a publisher.
	 */
	FloatMessage(NeMALA::Proptree pt):
		m_dValue(pt.get<double>("Value")){}

	~FloatMessage(){}

	/*
	 * Synchronize the private members of this class
	 */
	void BuildMessage()
	{
		AddElementNumber("Value", m_dValue);
	}

	/*
	 * Getters in order to return the member variables of this class.
	 * The subscribers use this function.
	 */
	double GetValue() {return m_dValue;}

	/*
	 * Setters.
	 */
	void SetValue(double dValue) {m_dValue = dValue;}

private:
	 //-------------- Variables --------------------
	double m_dValue;
};

#endif /* _MESSAGE_CLASSES_FLOAT_MESSAGE_H_ */

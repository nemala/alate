/*
 * PeerMessage.h
 *
 *  Created on: Jul 12, 2017
 *      Author: dave
 */

#ifndef _MESSAGE_CLASSES_PEER_MESSAGE_H_
#define _MESSAGE_CLASSES_PEER_MESSAGE_H_

#include <iostream>
#include <NeMALA/MessagePropertyTree.h>


//--------------------------------------------------------
// 						Class PeerMessage
//--------------------------------------------------------

/*
 * A message class encapsulating whether a peer was recognized:
 */
class PeerMessage : public NeMALA::MessagePropertyTree
{
public:

	//-------------- Constants --------------------
	typedef enum
	{
		PeerNotRecognized=0,
		PeerRecognized
	}PeerEnum;


	//-------------------------- Methods ------------------------------------
	/*
	 * Init class variables.
	 * Publishers should use this function.
	 */
	PeerMessage(PeerEnum eRecognized):m_eRecognized(eRecognized){}

	/*
	 * Build a message from a property tree.
	 * Subscribers should use this function.
	 * @param pt: The property tree received from a publisher which translated to message.
	 */
	PeerMessage(NeMALA::Proptree pt):
		m_eRecognized((PeerEnum)pt.get<double>("Recognized")){}

	~PeerMessage(){}

	/*
	 * Synchronize the private members of this class
	 */
	void BuildMessage()
	{
		AddElementNumber("Recognized", m_eRecognized);
	}

	/*
	 * Getters in order to return the member variables of this class.
	 * The subscribers use this function.
	 */
	PeerEnum GetValue() {return m_eRecognized;}

private:
	 //-------------- Variables --------------------
	PeerEnum m_eRecognized;
};

#endif /* _MESSAGE_CLASSES_PEER_MESSAGE_H_ */

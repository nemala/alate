# Path Follower

![Path Follower Application Deployment Diagram](../images/deployment_path_follower1.svg)

## Synopsis
Publishes a sequence of Destination waypoints. 
When reaching a waypoint, proceed to the next, and emit a "success" signal.
When reaching the last waypoint, emit a "terminated successfully" signal.

## Interactions and Dependencies

The Path Follower behavior, when first written, was designed to feed list of [local](./localizer.md) coordinates to the [Go to Point](./go2point.md) behavior, 
and to update the destination when the destination is reached. 
 
![Path Follower Application Internal Components Deployment Diagram](../images/deployment_path_follower.svg)

## Plugin Library Name

*NeMALAteBehaviorPathFollower*

## Subscribes to Topics

1. "position"

## Publishes to Topics

1. "destination"
1. "application_status"

## Parameters

1. "path" - (x, y, z, yaw) list of waypoints.
1. "close_enough" - the distance (meters) to a waypoint that qualifies as reaching the waypoint.
1. "aligned_enough" - the acceptable difference in yaw that qualifies as reaching the waypoint.
1. "repeat" - how many times to repeat the path. 0 for once (complete the path once, repeat zero times).
Set a negative value to repeat indefinitely  

## License
[IIT](../../LICENSE)

## Extra Dependencies

The [Localizer](localizer.md) component depends on  [Geographiclib](https://sourceforge.net/projects/geographiclib/).

# Localizer

## Synopsis
Translate WGS84 (Lat, Lon, Alt, Azimuth) to (X, Y, Z, theta) in a predefined reference frame. 

## Plugin Library Name

*NeMALAteBehaviorLocalizer*

## Subscribes to Topics

1. "telemetry"

## Publishes to Topics

1. "position"

## Parameters

1. "origin" - (lon, lat, alt, yaw) of the origin of the reference frame.
Yaw is the orientation of the X axis, and the Z axis points up.

## License
[IIT](../../LICENSE)

## Extra Dependencies
 [Geographiclib](https://sourceforge.net/projects/geographiclib/)

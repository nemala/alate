# Unicycle Agents with Crude Sensing over a Limited Sector of Visibility

## Synopsis
A swarming behavior described in the paper:

[Dovrat, David, and Alfred M. Bruckstein. "On Gathering and Control of Unicycle A(ge)nts with Crude Sensing 
Capabilities. *IEEE Intelligent Systems* 6 (2017): 40-46.](http://ieeexplore.ieee.org/document/8267998/)

Causes the agents to aggregate to a cohesive behavioral pattern.

## Plugin Library Name

*libNeMALAteBehaviorUCSLSV*

## Subscribes to Topics

1. "direction_command"
1. "hlc_telemetry"
1. "operator_payload"

## Publishes to Topics

1. "velocity_command"

## License
[IIT](../../LICENSE)

## Extra Dependencies
 None

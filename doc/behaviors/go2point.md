# Go to Point

## Synopsis
Basically, it just goes to a point.

Having the destination and current position, this behavior publishes a velocity command that would bring the position towards the destination.

## Interactions and Dependencies

* [Localizer](./localizer.md) for local coordinates.
* [Path Follower](./pathFollower.md) for a destination. 
 
![Path Follower Application Internal Components Deployment Diagram](../images/deployment_path_follower.svg)

## Plugin Library Name

*NeMALAteBehaviorG2Wp*

## Subscribes to Topics

1. "position"
1. "destination"

## Publishes to Topics

1. "velocity_command"

## Parameters

1. "gain_on_distance" - how much to multiple the distance in order to get the speed.
1. "gain_on_yaw_diff" - how much to multiply the differences between the current yaw and the desired yaw in order to achieve the angular velocity.
1. "dampening" - when closer than 1 meter to the waypoint, how much to dampen the control signal.
The resulting magnitude of the control signal will be "gain_on_distance" divided by "dampening".

## License
[IIT](../../LICENSE)

## Extra Dependencies
None

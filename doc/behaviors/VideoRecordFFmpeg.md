# Video Recorder

## Synopsis
Records a video using [FFmpeg](https://ffmpeg.org/).

## Plugin Library Name

*NeMALAteBehaviorRecordVideoFFmpeg*

## Subscribes to Topics
None

## Publishes to Topics
None

## License
[IIT](../../LICENSE)

## Extra Dependencies
Uses the command line tool "ffmpeg", does not link or make any use of FFmpeg source code other than executing the "ffmpeg" installed by the end user on their machine.
Compliance to the FFmpeg license (either GPL or LGPL) is therfore delegated to the end user (sorry...). 
As this library does not link with any FFmpeg libraries or compile any FFmpeg source code, and though at this point I regret not putting the entire NeMALA project under GPL, the ultimate decision is that this library will share the same license with the rest of the NeMALA project, which is IIT for reasons I had at the time.

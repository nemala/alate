## Navigate the Docs ##
* [Back to the beginning](https://gitlab.com/nemala/alate)

For more about how to use behaviors, read on [here](../using.md#behaviors).

Once built and installed, a [configuration](../using.md#configure) JSON file with a *"behaviors"* node will instruct the startup [script](using.md#script) how to launch the required [behavior module](../components/bm.md) nodes.

# Template Behaviors

The [template application](../using.md#template) includes the following behaviors:

* **[Unicycle Agents with Crude Sensing over a Limited Sector of Visibility](ucslsv.md)**
* **[Video Recorder](VideoRecordFFmpeg.md)**

# Path Following

A [path following application](pathFollower.md) consists of the following behaviors:

* **[Go to Point](go2point.md)**
* **[Localizer - from GPS to local coordinates](localizer.md)**
* **[Path Follower - Follow a path of waypoints](pathFollower.md)**

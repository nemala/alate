# High Level Control 
The High Level Control Module (HLC) is an abstraction of the UAV platform.
The HLC subscribes to the [Mission Control Module](mc.md) (MCM) state topic
and the [Behavioral Module](bm.md)'s velocity command topic.
The HLC publishes its state, telemetry and error data.

![High Level Control Module Class Diagram](../images/diagram_class_hlc.svg) 

## High Level State
The HLC state machine abstracts and simplifies the autopilot's internal state machine
while providing a logical interface with the [Mission Control](mc.md). 
 
![High Level Control Module State Diagram](../images/diagram_state_hlc.svg)

Upon initialization, the HLC waits in *Init* state until the MCM enters its init state as well,
or for the autopilot to get ready for takeoff.
When both conditions occur, the HLC transitions to *Ready* state. 

An MCM transition to its taking-off state causes the HLC to transition to *Takeoff* as well.
If a timeout occurs before the autopilot manages to arm its motors, the HLC returns to the *Ready*
state to wait for another takeoff command from the MCM.

If the autopilot manages to arm, a transition to *Gaining Altitude* occurs,
where the autopilot attempts to gain enough altitude to be considered *Airborne*.
Failing to attain the desired altitude on time results in a transition to the *Landing* state,
where the autopilot attempts to land and disarm its motors when finished, resulting in a transition to the 
*Ready* state.

While *Airborne*, the HLC transitions to *Landing* or *Return to Launch* in response to the an MCM transition to 
its landing or return to launch states respectively.

The *Return to Launch* state transitions to *Ready* upon disarming motors.

Once armed, the HLC allows *Manual* override, until the overriding pilot disarms, at which point the HLC 
transitions back to *Ready*.
The manual override is carried out in the template implementation by changing the autopilot's flight mode to anything 
other than *GUIDED* or *LANDING*.

If at any point the autopilot shuts down, the state machine transitions to the unrecoverable state *LLC Down*.
Low battery transitions the state machine to the *Low Battery* state.

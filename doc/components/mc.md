# Mission Control
The Mission Control Module (MCM) synchronizes the tasks of other nodes in the system,
from a mission point of view.

The MCM subscribes to the [Operator Station Client](osc.md) (OSC) Operator Command topic, 
and the [High Level Control](hlc.md) (HLC) State topic, and publishes its own state as a topic.

![Mission Control Module Class Diagram](../images/diagram_class_mcm.svg)

## Mission State
At the heart of the MCM's operation is the *Mission State*.
The agent's mission state gives a system-wide view into what the agent is doing.  

![Mission Control Module State Diagram](../images/diagram_state_mcm.svg)
 
Upon initialization, the MCM waits in *Init* state for the HLC to finish
initializing and to report that it is ready; 
at which point the MCM changes state to *Standby*.

Once an OSC command triggers a transition to *Taking off*, the MCM remains in that state until 
either an OSC command aborts the take-off sequence or an HLC state transition
triggers a mission state transition to *Standby*, *Manual*, *Mission*, or *Landing*.

While performing a mission, The UAV may *Return To Launch* if required to do so by the operator,
or if the HLC reports a low battery state.
The UAV will also return to launch if the OSC fails to communicate with its server.

At any point in which the UAV's motors can be activated, a *Manual* override is possible,
triggered by a *Low Level Control (LLC)* event and communicated to the MCM by the HLC.
Recovery from the *Manual* state is possible as result of the HLC transitioning back to its *Ready* state,
meaning the UAV's motors are disabled.

If at any point the HLC reports that it is in its *Error* state, due to the (LLC) 
shutting down, the mission state machine transitions to an unrecoverable *Error* state.

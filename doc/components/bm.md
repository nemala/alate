# Behavioral Modules
The behavioral modules are where [Alate](https://gitlab.com/nemala/alate) realizes the framework’s inversion-of-control principle.

![Behavior Arbiter Module class diagram](../images/diagram_class_arbiter.svg)

An arbiter class has a set of behaviors, and the concrete user implementation decides upon which behavior will be 
active and when.
Each behavior must implement an activation / deactivation procedure for the arbiter’s use,
as well as public procedures for event-handlers to evoke.
Behaviors and their scheduling by the arbiter, along with custom event-handlers are therefore the building blocks 
left within the concrete user’s responsibility.

The [template application](../using.md#template) demonstrates how two independent behaviors can coexist within the framework.
A VideoRecordBehavior is set to being active when the [Mission Control](mc.md) Module's state is either taking off, 
performing a mission, 
landing, returning to launch, or manual override.
Independently, a swarming behavior becomes active, publishing velocity commands to the [High Level Control](hlc.md) 
Module only when the MCM is in its "mission" state.
The swarming behavior implemented as a template is described in the paper:

[Dovrat, David, and Alfred M. Bruckstein. "On Gathering and Control of Unicycle A(ge)nts with Crude Sensing 
Capabilities. *IEEE Intelligent Systems* 6 (2017): 40-46.](http://ieeexplore.ieee.org/document/8267998/)

The configuration file given as an argument to [alate.py](https://gitlab.com/nemala/alate/blob/master/config/developer_quick_start/alate.py) tells the behavior module 
how many nodes to run, which behaviors run on each node, where to find the plugin libraries for the 
behavior implementation, and when to activate / deactivate each behavior.
No need to recompile the framework to change the configuration, 
even when adding new behaviors. More on how to introduce new behaviors (including
sensors and payloads) [here](../using.md).

# Operator Station Client

The *Operator Station Client* (OSC) communicates the agent's state to the Operator Station Server (OSS) and receives 
orders from it.

![Operator Class Diagram](../images/diagram_class_osm.svg)

The OSC subscribes to the [High Level Control](hlc.md) Module's state, telemetry and error topics,
 as well as to the [Mission Control](mc.md) Module's (MCM) state topic.

The OSC publishes operator commands to the *Operator Command* topic, subscribed to by the MCM, 
and operator direction commands to the *Direction Command* topic subscribed to by the [Behavior](bm.md) Module.

The OSC also subscribes to the *Operator Feedback* topic and 
publishes to the *Operator Payload* topic. 
These topics are left for the Alate custom application developers to use as they see fit.

## Navigate the Docs ##
* [Back to the beginning](https://gitlab.com/nemala/alate)
* [The Template Application](using.md#template)
* [Configure](using.md#configure)
    * [alate.py](using.md#script)
    * [configuration files](using.md#config)
* [Extend](using.md#extend)
    * [Communication](using.md#communication)
    * [Autopilots](using.md#autopilots)
    * [Behaviors](using.md#behaviors)

# Using Alate

This section explains how to tailor Alate to a given project (the [Template Application](using.md#template) given as an example), and discusses configuration and extension points. 

We leave it to the custom application developer using Alate as a framework to add sensors, peer communication (we have the [operator station](https://gitlab.com/nemala/operator-station) covered),
and payloads, along with the rules that govern these components.

Alate's behavior mechanism lets the application developer add these capabilities and governing rules with relative ease by letting them add [behavior plugin](#behaviors) libraries, and letting the end user choose the timing in which these behaviors come into play using [Behavior Module Arbiter](./components/bm.md) (BMA) node [configuration](#config). 

## <a name="template"></a> The Template Application

As an example for how an application developer would use Alate as a framework, we present a template application with two behaviors:

* Video Recording.
* A swarming protocol described in
[Dovrat, David, and Alfred M. Bruckstein. "On Gathering and Control of Unicycle A(ge)nts with Crude Sensing 
Capabilities. *IEEE Intelligent Systems* 6 (2017): 40-46.](http://ieeexplore.ieee.org/document/8267998/)

More information about these behaviors can be found [here](doc/behaviors/index.md).

This application is also used as an example in the [Quick-Start Guide](doc/docker.md), where three very different platforms run the exact same swarming behavior, and two of the three run the same video recording behavior.
"The exact same" here means the exact same human-written code. The machine instruction-set is quite different between the ARM-based drones and AMD_x64-based simulation.
Yet the [ArduPilot](https://ardupilot.org/) and the [Tello](https://www.ryzerobotics.com/tello) applications are identical even in terms of compiled software. The differences in how the two applications interact with their varying autopilots and hardware peripherals come from setting up their docker containers with different composition files and running the [High Level Control](components/hlc.md) and [Behavioral Module Arbiter](components/bm.md) nodes with different [configuration]() files and behavior plugins.

This cross-platform cross-application-ness is the entire point of having a framework - we use the same nodes: [Mission Control](components/mc.md), [High Level Control](components/hlc.md), [Operator Station Client](components/osc.md), and [Behavioral Module Arbiter](components/bm.md), over and over; we code a new behavior once and configure it differently, as well as combine it with other behaviors, to form new applications without going over the design process for each.
The design is already implemented in the framework up until the point of behavior-interaction, which is left as a design degree of freedom for the custom application developer.

![Typical Deployment Diagram](images/deployment_diagram.svg)

When coding the template application, I started with writing the swarming behavior for [SITL](http://ardupilot.org/dev/docs/sitl-simulator-software-in-the-loop.html) on my trusty AMD_x64 arch workstation.
The [swarming behavior](http://ieeexplore.ieee.org/document/8267998/) needs to get the existence of other peer agents in a sector in front of the agent as input.
Its output is a velocity command to the drone autopilot.
Since I didn't have a way to readily extract the peer count from SITL using simulated cameras or whatnot, I chose instead to use the [operator station](https://gitlab.com/nemala/operator-station)'s payload topic and to calculate the geodesic to peers on the server in order to determine the existence of peers in a sector.
This way I could test the behavior in the comfort of my chair.

Later, I used the same code, compiled for a [Raspberry Pi](https://www.raspberrypi.org/), with a different configuration file (the Raspberry connects with the flight controller using a [serial port](https://ardupilot.org/dev/docs/raspberry-pi-via-mavlink.html) configuration) to go fly a group of quadrotors in the discomfort of the great outdoors...

In addition, I wanted to record the video from the drone's perspective. I did so by adding a behavior that records video and configured the behaviors so that the swarming protocol will kick in only when the quadcopters are airborne, while the video recording behavior remains active from the moment the agents received the signal to take-off and until they complete their landing sequence.

I chose to run the two behaviors on separate nodes, but I just as may have used one [BMA](components/bm.md) node for both.

## <a name="configure"></a> Configure

Alate nodes generally expect a proxy name and a configuration file as input to run.
The proxy name is required so that the specific node will know to which proxy it needs to subscribe for its own node management topic, and usually for other topic subscription as well.

### <a name="script"></a> alate.py
An example python [script](https://gitlab.com/nemala/alate/blob/master/config/developer_quick_start/alate.py) is available in the project's
 [developer quick-start](https://gitlab.com/nemala/alate/blob/master/config/developer_quick_start/) folder.
 
This script reads a configuration file with instructions on
where to run, how much to wait before running, what to run, and where to log.
The script also sets up a NeMALA logger.

Adding behaviors does not require changing this script, as the script reads the configuration file
and runs a new [BMA](components/bm.md) node for each child of the *behaviors* json node.
Adding other execution nodes, sensors for instance, is not only easy but also encouraged.

Notice that the example project uses just one
 [NeMALA Proxy](https://gitlab.com/nemala/core/blob/master/doc/components.md),
 so only one [logger](https://gitlab.com/nemala/tools/blob/master/doc/components.md#logger) 
is used. This can be changed of course by simply editing the alate.py script and 
accompanying configuration file.

### <a name="config"></a> Configuration Files
The [developer quick-start](https://gitlab.com/nemala/alate/blob/master/config/developer_quick_start/) folder includes three configuration files;
[uav.json](https://gitlab.com/nemala/alate/blob/master/config/developer_quick_start/uav.json) for running the [template application](using.md#template) on an [Arducopter](https://ardupilot.org/copter/) UAV,
[sitl.json](https://gitlab.com/nemala/alate/blob/master/config/developer_quick_start/sitl.json) for running the [template application](using.md#template) using [ArduCopter](http://ardupilot.org/copter/)'s 
[SITL](http://ardupilot.org/dev/docs/sitl-simulator-software-in-the-loop.html),
and 
[tello.json](https://gitlab.com/nemala/alate/blob/master/config/developer_quick_start/tello.json) for running the [template application](using.md#template) on a computer linked with a [Tello](https://www.ryzerobotics.com/tello) mini drone.

Any [NeMALA](https://gitlab.com/nemala/core) based project needs to define the following fields:

* **nodes**

    List of [Dispatcher](https://gitlab.com/nemala/core/blob/master/doc/components.md)
     nodes and their associated number.
    It is helpful yet not required that each NeMALA node has its own unique node number.
    
* **proxies**

    [Proxy](https://gitlab.com/nemala/core/blob/master/doc/components.md) configuration.
    You can have as many as you want, just make sure that the subscribers
    have access to their publisher via a proxy, and that each proxy has unique "subscribers",
    "publishers", and "logger" endpoints.
    
* **topics**

    List of topics and their associated number.
    Topic numbers must be unique unsigned integers.
    They have to be unique because [handlers](https://gitlab.com/nemala/core/blob/master/doc/components.md)
    cast different topics to different message classes. 
    Though it is theoretically possible to use the same topic number for two different types
    over two separate proxies, why would you?
    Doing so would create a coupling between a proxy and a subscriber,
    and that's bad. So don't do it.
       
The rest of the fields in the example configuration files are Alate specific:

* **autopilot**
    
    Mandatory autopilot configuration:
    
    * **type**
    
        A name recognized and used by the Autopilot Factory to generate the right
        type of autopilot.   
    
    * **altitude**
       
       The altitude to which the autopilot will ascend before entering "mission" mode.
    
    * **low_bat_voltage**
    
        The lower threshold for operational battery voltage.
        The HLC state changes to "Low Battery" if the voltage is lower than this value.
           
    [Dronekit](https://github.com/dronekit/dronekit-python) specific configuration:
    * **master**

        Where to find the Mavlink master. The example shows a serial connection to a Pixhawk.
        
    * **baud**
        
        The baud rate for the serial connection. Ignored for tcp connections.
         
    [Tello](https://github.com/dji-sdk/Tello-Python) specific configuration:
            
     * **network interface**
        The tello's wifi SSID. 
        
    * **enable_camera**
        Whether to enable the camera.
    
* **behaviors**

    Parameters for the different behaviors.
    Each child node under the *behaviors* node causes Alate to run a separate [BMA](components/bm.md)
    NeMALA node.
    
    [BMA](components/bm.md) parameters:
    * *node*
    
        The NeMALA node name to be used for the BMA node.
        The name resolves to a node ID in the *nodes* configuration section. 
    * *proxies*
    
        All proxies in this section are proxy names that can be resolved in the root child *proxies*.
        The *main* proxy is the proxy that the Alate node subscribes to for its service topic (pause, shut-down etc.).
        The *additional* proxies are an array of proxies the node subscribes to.
    * *behaviors*
        
        The BMA sets up a behavior for each child of *behaviors*.
        Each behavior must have a  *plugin_lib_name* with the name of the plugin library that behavior uses.
        In addition, each behavior must have an *activation_by_mcm_state* node filled out 
        to instruct the [Arbiter](components/bm.md) at which [MCM](components/mc.md) states the behavior should be
         active.
   
* **operator_station**

    [OSC](components/osc.md) parameters:
    * *server*
    
        Server's IP address. 
    * *port*
        
        Server listens to this port for OSC messages.
        
    * *allowed_timeouts*
    
        Number of timeouts before the OSC gives up and initiates a "Go Home" command to the MCM.
        This is helpful if your drone gets out of the Operator Station's range. 

* **deploy**

    used by the alate.py script.
    
    * *sleep_for_sec* 
     
        Amount of seconds to wait before running Alate.
        Useful for running on boot. See the [Developer Guide](start.md#autorun).
		
	*  *working_directory*
	
	    Where to run Alate, where the logs, videos etc. will be found after running.
    
## <a name="extend"></a>Extend

We aspire to make the [MCM](components/mc.md), [HLC](components/hlc.md), [OSC](components/osc.md), and [BMA](./components/bm.md) as useful and general as we can so that Alate-based custom applications wouldn't require any changes to these modules. 
However, we intentionally left extension *hotspots* in *Communication*, *Behavior* and *Autopilot* aspects of the Alate framework.
We did so by adding the *Operator Payload* and *Operator Feedback* topics to the OSC, along with associated fields in the messages to and from the server.
In the HLC, we made an *Autopilot* interface and an accompanying *Factory* mechanism so that new autopilots (and therefore new UAV platforms) can be readily integrated;
In the BMA, we created a plugin mechanism for new behaviors and the arbiter mechanism to generically control the activation of these behaviors.

### <a name="communication"></a>Communication
As demonstrated in the [template application](using.md#template), the Operator Station's messages can be extended with data for the behavior 
to consume without having to change anything in the [OSC](components/osc.md) or elsewhere outside of the behavior's scope.


### <a name="autopilots"></a>Autopilots
The [HLC](components/hlc.md) is an abstraction of the UAV platform.
The HLC's autopilot is the interface component between Alate and the actual physical platform, via the platform's autopilot API.

Two very different autopilots are demonstrated in the [template application](using.md#template), [Dronekit](../src/HighLevelControl/AutoPilots/AutopilotDronekit.py) and [Tello](../src/HighLevelControl/AutoPilots/AutopilotTello.py).
Both are derived from the abstract class [Autopilot](../src/HighLevelControl/AutoPilots/Autopilot.py), which implements required mechanisms and enforces the interface's implementation.
 
 ![Autopilot Extensions - Class Diagram](images/diagram_class_autopilot.svg)

 To add a new autopilot:
 
 1. Derive a class from [Autopilot.py](../src/HighLevelControl/AutoPilots/Autopilot.py) using the new autopilot's API.
 1. Add the new class to the [Autpilot Factory](../src/HighLevelControl/AutoPilots/AutopilotFactory.py)'s
  create() method.
 1. Add a new configuration json file with the autopilot section filled out to match the new autopilot's 
 required arguments.

### <a name="behaviors"></a>Behaviors
The reason Alate was written in the first place was to easily implement and test new swarm behaviors.

To add a new behavior:

1. Create a class that inherits from the abstract [behavior](../src/Behavior/Behaviors/Behavior.h) class.
1. Create a Factory class that inherits from the generic [Factory](../src/Behavior/Behaviors/Factory.h) class and creates your new behavior class.
1. If required, have your custom factory create publishers and handlers, and add them to the [BMA](./components/bm.md)'s node dispatcher.
1. Create a dynamic library (shared object) with your custom behavior and factory. Export a static function that creates your custom factory as the symbol *CreateBehavior*. Check out the [template application](using.md#template) to see for example how we did it with our CMakeLists.txt. 
1. Edit a configuration.json file according to the [configure](using.md#config) section and the examples provided by the [template application](using.md#template).

The above list might seem like a lot, but it actually isn't that complicated.
I'll try to make it clear by getting into the mechanics of it all.
Looking at [BehaviorNode.cpp](../src/Behavior/BehaviorNode.cpp), we see a [NeMALA Dispatcher](https://gitlab.com/nemala/core/blob/master/doc/components.md) being prepared for dispatching, just like any other [NeMALA](https://gitlab.com/nemala) execution node.

![Behavior Arbiter Module class diagram](images/diagram_class_arbiter.svg)

The unusual parts are the [Arbiter](components/bm.md) setup and the behavior's plugin mechanism.
The template application's swarming behavior's [Factory](../src/Behavior/Behaviors/UCSLSV/Factory.h) inherits from a generic [Factory](../src/Behavior/Behaviors/Factory.h) and is responsible for the construction and destruction of the [UCSLSV](../src/Behavior/Behaviors/UCSLSV/BehaviorUCSLSV.h) swarming behavior and its
accompanying handlers and publishers.
The [BMA](./components/bm.md) node's dispatcher (see class diagram above) subscribes to the mission control state, and whenever the state changes the dispatcher's handler invokes the arbiter to activate or deactivate its
behaviors accordingly.

This example showcases Alate's power - a few hundred lines of code in the swarming behavior module (written by the framework's intended users, the custom application developers, in this case, me) utilize a few thousand lines of code written by the framework developers (also me).
In addition, there is no need to recompile the Alate framework in order to add new behaviors; compiling a plugin library and changing the configuration file is all that is required.
This makes the Alate framework fit for deployment using [containers](docker.md), all that needs to be done is to map the docker volume to the configuration file and behavior plugin library's location.

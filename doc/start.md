## Navigate the Docs ##
* [Back to the beginning](https://gitlab.com/nemala/alate)
* [Setup](start.md#setup)
    * [Prerequisites](start.md#prerequisites)
    * [Install](start.md#install)
* [Run on UAV](start.md#autorun)
* [Run Simulation](start.md#simulation)
* [Going Deeper](start.md#further)
    * [The Template Application](using.md#template)
    * [Components](doc/components.md)
    * [Template Behaviors](doc/behaviors/index.md)
    * [Dockerfiles](https://gitlab.com/nemala/alate/-/tree/master/dockerfiles)

## <a name="setup"></a>Set Up
For the following discussion, the drone's on-board companion computer is referred to as the *Target* and the development computer is referred to as the *Host*.
In the examples given here, we use a [Raspberry Pi](https://www.raspberrypi.org/) as the target and a x86_64 machine 
running [Ubuntu](https://ubuntu.com/) as the Host.
If your setup is different, you'll probably have to adjust these instructions to fit.

### <a name="prerequisites"></a> Prerequisites
1. #### Install Boost on Target and Host
    1. Follow the instructions at [Boost](http://www.boost.org/doc/libs/1_64_0/more/getting_started/index.html). 
    Recommended steps:
        1. [Download](https://www.boost.org/users/history/version_1_64_0.html) and extract Boost.
        1. From the extracted folder run
        
            ```console
            user@hostname:/path_to_boost$ ./bootstrap.sh
            ```
        1. If needed, change the generated *project-config.jam* to fit your preferences.
        1. Compile Boost by running
        
            ```console
            user@hostname:/path_to_boost$ ./b2
            ```
        1. Install Boost by running
        
            ```console
            user@hostname:/path_to_boost$ sudo ./b2 install
            ```
    1. To cross-compile, which is recommended, follow the instructions [here](https://boostorg.github.io/build/manual/develop/index.html#bbv2.tasks.crosscompile)

1. #### Install NeMALA Core & Tools on Target and Host
    1. Follow the instructions at NeMALA [Core](https://gitlab.com/nemala/core) & [Tools](https://gitlab.com/nemala/tools).
     
1. #### Set up NeMALA Operator Station
    1. Follow instructions at NeMALA [Operator Station](https://gitlab.com/nemala/operator-station). 
    
1. #### Install ArduCopter on Host
    1. Follow the instructions at [ardupilot](http://ardupilot.org/dev/docs/setting-up-sitl-on-linux.html). Recommended steps:
        1. Clone latest ArduPilot master version.
            
            ```console
            user@hostname:~/$ git clone https://github.com/ArduPilot/ardupilot
            user@hostname:~/$ cd ardupilot
            user@hostname:~/ardupilot$ git submodule update --init --recursive
            ```
        1. Install additional ArduPilot prerequisites
            
            ```console
            user@hostname:~/ardupilot$ Tools/environment_install/install-prereqs-ubuntu.sh
            ```

        1. Depending if you opted for $PATH changes, you might want to log out and in for the changes to take effect.
        1. Wipe the virtual EEPROM from simulator and load parameters.
        
            ```console
            user@hostname:/~/ardupilot/ArduCopter$ ../Tools/autotest/sim_vehicle.py -w
            ```
        1. Once the simulator responds with 
            
            ```console
            STABILIZE> 
            ```
            you can shut it down with a keyboard interrupt (CTRL^C).
    1. Once installed, run sim_vehicle:
    
        ```console
        user@hostname:/~/ardupilot/ArduCopter$ sim_vehicle.py --out=tcpin:0.0.0.0:14552 --console --map -j4
        ```
    1. If you chose to install [FlightGear](http://www.flightgear.org/) (it's not required, but it's cool :)), run in another terminal:
    
        ```console
        user@hostname:/path_to_ardupilot/Tools/autotest$ fg_quad_view.sh
        ```
1. #### Install DroneKit to Target and Host</h5>
    1. Follow the instructions at [DroneKit](https://github.com/dronekit/dronekit-python/blob/master/docs/guide/quick_start.rst), since MAVLink and SITL were already installed on the Host along with ArduCopter, there is no need to install dronekit-sitl.
    
        ```console
        user@hostname:~/$ pip install dronekit
        ```
    
### <a name="install"></a> Install Alate to Target and Host.
We strongly recommend cross compiling for the Raspberry Pi.

1. Run [CMake](https://cmake.org/).
1. <a name="configure_cmake"></a>Set the *source* folder to the *project root* folder and the *build* folder to a separate appropriate folder.
1. Run the CMake *Configure* command.
1. Select the desired toolchain and makefile generators.
1. Fill in desired build type and install prefix, along with the location of the NeMALA Core library.
1. Run the CMake *Generate* command. At this point, the chosen *build* folder has all the make files needed for installing NeMALA Alate.
1. Go to the build folder and run the command:

    ```console
    user@hostname:/path_to_build_folder$ make
    ```
1. If there are no errors (there shouldn't be any):

    ```console
    user@hostname:/path_to_build_folder$ make install
    ```
1. Edit the configuration file to fit your needs, more on how to do it [here](using.md#configure).
1. Copy the installed files to the Target. My favorite tool for this is [rsync](https://linux.die.net/man/1/rsync).

## <a name="autorun"></a> Set Alate to Automatically Run on the Target
1. Setup NeMALA proxy as a systemd.service:

    * In order to get the proxy running on boot, run this in terminal:
    
        ```console
        pi@raspberry:/path_to_alate_root_folder/config$ sudo cp ./nemala_uav_proxy.service /lib/systemd/system/
        pi@raspberry:/path_to_alate_root_folder/config$ sudo systemctl daemon-reload
        pi@raspberry:/path_to_alate_root_folder/config$ sudo systemctl enable nemala_uav_proxy.service
        ```
    * To run/stop otherwise:
    
        ```console
        pi@raspberry:/anywhere$ sudo systemctl start nemala_uav_proxy.service
        pi@raspberry:/anywhere$ sudo systemctl stop nemala_uav_proxy.service
        ```
    * And to monitor:
    
        ```console
        pi@raspberry:/anywhere$ systemctl status nemala_uav_proxy.service
        ```
1. Setup Alate to run after boot using cron:

    * `crontab -e` then add the line:
    
        ```cron
        @reboot /path_to_where_alate_is_installed/alate.py /path_to_where_alate_is_installed/uav.json
        ```

## <a name="simulation"></a> Run Simulation

Once the [NeMALA](https://gitlab.com/nemala) [Operator Station](https://gitlab.com/nemala/operator-station) Server is up and running, run the following commands, each from its own terminal:

1. run SITL

    ```console
    user@hostname:/path_to_ArduCopter$ ../Tools/autotest/sim_vehicle.py --out=tcpin:0.0.0.0:14552 --console --map -j4
    ```
1. run NeMALA proxy

    ```console
    user@hostname:/path_to_install_folder$ nemala.py proxy uav ./sitl.json
    ```
1. run NeMALA Alate's [template application](using.md#template)

    ```console
    user@hostname:/path_to_install_folder$ ./alate.py sitl.json
    ```
    and the uav should be up and running, ready to handle 
    user commands from the [Operator Station](https://gitlab.com/nemala/operator-station).
 
1. To quit:

    1. run:
    
       ```console
       user@hostname:/anywhere$ nemala.py terminate [*] [*] ipc:///tmp/alate_publishers
       ```
       and wait for a clean shutdown of all sub-processes.
    1. Terminate all other running processes (proxy, logger and SITL processes) by keyboard interrupt (Ctrl^C) or
     kill commands.

## <a name="further"></a> Going Deeper

Now that you have Alate and its [template application](using.md#template) up and running, you are invited to learn more about the Alate [components](doc/components.md) and template [behaviors](doc/behaviors/index.md).

The source tree has a "[dockerfiles](https://gitlab.com/nemala/alate/-/tree/master/dockerfiles)" folder you might find interesting.
It holds the docker files I used to generate the images that are used in the [Quick-Start Guide](doc/docker.md).

If you feel that the Alate documentation is lacking, you have a feature you want to add, a bug that you want to report, or you just want to get involved, please [contact us](mailto:nemalate@gmail.com)!

## Navigate the Docs ##
* [Back to the beginning](https://gitlab.com/nemala/alate)
* [Operator Station](docker.md#server)
* [Ardupilot SITL swarm](docker.md#sitl)
    * [Setup](docker.md#setup)
    * [Run](docker.md#run) and [Customize](docker.md#customizing)
    * [Stop and Cleanup](docker.md#stop_clean)
* [Deploy to a fleet with Balena](docker.md#balena)

# Quick-Start Guide

In this section we demonstrate how to run Alate using [Docker](https://docs.docker.com/get-docker/) container composition.
The docker files used to create the containers can be found in the [dockerfile](https://gitlab.com/nemala/alate/-/tree/master/dockerfiles) folder.

## <a name="server"></a> Run the Alate Operator Station

The Operator Station is where we, the humans, are included in *"the loop"*.
Autonomy is great and all, but humans in general and myself in particular, enjoy some sense of control.
To exercise this control Alate has an [Operator Station](https://gitlab.com/nemala/operator-station) companion project.
With the Operator Station, a [GUI](https://gitlab.com/nemala/operator-station#api) is presented for us humans to enjoy (and the occasional robot, depending on your network),
and through which we can give orders to the Alate agents;
to enable non-humans to participate in the fun, an [API](https://gitlab.com/nemala/operator-station#api)
is also available.
We recommend running the server in a docker container,
see instructions [here](https://gitlab.com/nemala/operator-station#docker). 

 ![Operator Station GUI](images/osgui01.png)

For more information, visit [https://gitlab.com/nemala/operator-station](https://gitlab.com/nemala/operator-station).

## <a name="sitl"></a> Run a Simulation

Now that the server is up, we can run a swarm simulation that runs our agents on [Ardupilot](https://ardupilot.org/)'s [SITL](http://ardupilot.org/dev/docs/sitl-simulator-software-in-the-loop.html).

To run the simulation, we'll need to run a [Python](https://www.python.org/downloads/) script that invokes [Docker](https://docs.docker.com/get-docker/) CLI commands.


### <a name="setup"></a> Preparations

1. Create a work directory:

    ```console
    user@hostname:~$ mkdir alate_swarm && cd alate_swarm
    ```
    
1. Make a log directory:

    ```console
    user@hostname:~/alate_swarm$ mkdir logs
    ```
    
1. Copy Alate's swarm [docker-compose.yml](https://gitlab.com/nemala/alate/-/blob/master/config/swarm_simulation/docker-compose.yml)
to the work directory:

    ```console
    user@hostname:~/alate_swarm$ wget https://gitlab.com/nemala/alate/-/raw/master/config/swarm_simulation/docker-compose.yml
    ```
   
1. Copy Alate's [swarm.py](https://gitlab.com/nemala/alate/-/blob/master/config/swarm_simulation/swarm.py)
and [swarm.json](https://gitlab.com/nemala/alate/-/blob/master/config/swarm_simulation/swarm.json) to the work directory:

    ```console
    user@hostname:~/alate_swarm$ wget https://gitlab.com/nemala/alate/-/raw/master/config/swarm_simulation/swarm.py
    user@hostname:~/alate_swarm$ wget https://gitlab.com/nemala/alate/-/raw/master/config/swarm_simulation/swarm.json
    ``` 
1. Running the script:

    ```console
    user@hostname:~/alate_swarm$ swarm.py
    ```
   should show the following message:
   
    ```console
   Usage: swarm.py <Command> <Number of agents>
   Command:
       start   - run a swarm with <Number of agents> agents.
       plugin  - run a swarm with <Number of agents> agents, using a plugin behavior. See documentation.
       stop    - stop a running swarm with maximal agent number <Number of agents>.
       clean   - cleanup <Number of agents> after they all stopped.
    ```

### <a name="run"></a> Run
1. From the work directory:

    ```console
    user@hostname:~/alate_swarm$ swarm.py start 4
    ```
   will run a simulation with 4 agents (you can run as many agents as your computer can carry).

    ![Simulation](images/osgui02.png)

### <a name="customizing"></a> Customize
If you want to simulate [your own behaviors](using.md#behaviors), follow this example,
where we replace our [template application](using.md#template) with a [path following application](behaviors/pathFollower.md):
1. Create a bma container with your plugin libraries (see how we did it [here](../dockerfiles/bma.docker)).
1. Edit the services:bma:image entry in the docker-compose.yml file to your container's tag.
1. Edit or overwrite 'swarm.json' to include your new behavior.
	```console
	user@hostname:~/alate_swarm$ wget https://gitlab.com/nemala/alate/-/raw/version_0_8/config/swarm_simulation/pathfollow.json
	user@hostname:~/alate_swarm$ mv ./pathfollow.json ./swarm.json
	```
1. To test your custom behavior with 4 agents (for instance), Run:
	```console
	user@hostname:~/alate_swarm$ swarm.py plugin 4
	```

Don't forget to adapt this example to your own operating system, docker container, home directory, user, and application.

### <a name="stop_clean"></a> Stop Simulation and Cleanup

1. Running

    ```console
    user@hostname:~/alate_swarm$ swarm.py stop 4
    ```
    will stop all containers running Alate labeled from 0 to 3.
    You might have to wait a few seconds for the docker daemon to comply.
    
1. After stopping the containers, their underlying networks and volumes, as well as the containers themselves, can be removed:
 
     ```console
     user@hostname:~/alate_swarm$ swarm.py clean 4
     ``` 

**Important:** the number of agents for the stop and clean operations should be the number of agents used in the start or plugin option.

## <a name="balena"></a> Deploy to a Fleet

[Balena Cloud](https://www.balena.io/cloud/) is great for deploying container-based applications. We recommend it even if you have just one drone in your fleet.
This sections assumes you have a Balena Cloud account and access to that account's dashboard.
We here show how to deploy our [Dronekit](https://github.com/dronekit/dronekit-python) configuration on a fleet of [Raspberry Pi](https://www.raspberrypi.org/)s as an example.

1. Create a Balena Application and flash the generated image on an SD card.

1. To enable wifi, follow these [instructions](https://www.balena.io/docs/reference/OS/network/2.x/#wifi-setup).

1. From the balena dashboard, fleet configuration:
    1. Define DT parameters: "i2c_arm=off","spi=off","audio=off".
    1. Disable the rainbow splash screen.
    
1. In addition, if [serial communication interfaces](https://www.balena.io/docs/learn/develop/hardware/i2c-and-spi/#using-uart-or-serial-on-raspberry-pi-3) (ttyS0 for example) are required:
    1. Define DT overlays: "pi3-miniuart-bt-overlay"
    1. Enable UART.

1. To [enable the Raspberry Pi camera](https://www.balena.io/docs/learn/develop/hardware/i2c-and-spi/#raspberry-pi-camera-module):
    1. Define device GPU memory in megabytes to 128.
    1. Add custom configuration variable "RESIN_HOST_CONFIG_start_x" with value 1.

    ![Balena Fleet Configuration](images/balena01.png)

1. From your local workstation:
    1. Create a work directory:
        
        ```console
        user@hostname:~$ mkdir alate_balena && cd alate_balena
        ```
    
    1. Copy Alate's [drone-deployment](https://gitlab.com/nemala/alate/-/raw/master/config/drone_deploy) docker-compose and configuration files to the work directory:
        
        ```console
        user@hostname:~/alate_balena$ wget https://gitlab.com/nemala/alate/-/raw/master/config/drone_deploy/dronekit-compose.yml
        user@hostname:~/alate_balena$ wget https://gitlab.com/nemala/alate/-/raw/master/config/drone_deploy/dronekit.json
        ```
        
    1. Change the YAML file's name to docker-compose.yml
    
        ```console
        user@hostname:~/alate_balena$ cp dronekit-compose.yml docker-compose.yml
        ```
    
    1. Balena Push!
      
        ```console
        user@hostname:~/alate_balena$ balena push <your_balena_application_name>
        ```
        
1. In the balena device dashboard, use the terminal to login the Host OS.
    1. From the shell:
    `cd /var/lib/docker/volumes/<APP ID>_resin-data/_data`
    1. Write a configuration file to the balena host's persistent memory, e.g. `vi dronekit.json` or `cat > dronekit.json`.

    ![Balena Configuration Files](images/balena02.png)

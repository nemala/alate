## Navigate the Docs ##
* [Back to the beginning](https://gitlab.com/nemala/alate)
* [Mission Control](components/mc.md)
* [High Level Control](components/hlc.md)
* [Operator Station Client](components/osc.md)
* [Behavioral Modules](components/bm.md) 

# Components

 Each [Alate](https://gitlab.com/nemala/alate) component is a [NeMALA dispatcher](https://gitlab.com/nemala/core/blob/master/doc/components.md) node.
 Inter-node communication is handled by the dispatchers by topic messages being published and subscribed to.
 
 A typical deployment diagram generally looks like this:
 
 ![Deployment Diagram](images/deployment_diagram.svg)
 
 In this deployment setup, a [Mission Control](components/mc.md), a [High Level Control](components/hlc.md) and an [Operator Station Client](components/osc.md) nodes are shown and their topics are presented.
  
 Two [Behavioral Modules](components/bm.md) are also presented, and the possibility of having them intercommunicate (the "peer" topic) is also shown.
 While every [Behavioral Module](components/bm.md) should subscribe to the MCM state topic (required for the [Arbiter](components/bm.md) to function), subscribing (and publishing) to other topics is optional.
 

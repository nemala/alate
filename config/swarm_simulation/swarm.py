#!/usr/bin/env python3

###################################################################################
# File created by David Dovrat, Dec 23, 2019.
# For NeMALA Alate, under that project's license.
# The code in this file is provided "as is" and comes with no warranty whatsoever.
###################################################################################

import sys
import os
import shlex, subprocess
import time
import json
import yaml
from datetime import datetime

class SwarmLauncher:
    def __init__(self, option=None, configuration=None, composer=None):
        self._configuration = configuration
        self._composer = composer
        if ('start' == option):
            self._function = self.Start
        elif ('plugin' == option):
            self._function = self.Plugin
        elif ('stop' == option):
            self._function = self.Stop
        elif ('clean' == option):
            self._function = self.Clean
        else:
            self._function = self.Usage

    def Run(self, agents=0):
        self._function(agents)

    def Usage(self, agents=0):
        print("Usage: " + sys.argv[0] + ' <Command> <Number of agents>')
        print("Command:")
        print("    start   - run a swarm with <Number of agents> agents.")
        print("    plugin  - run a swarm with <Number of agents> agents, using a plugin behavior. See documentation.")
        print("    stop    - stop a running swarm with maximal agent number <Number of agents>.")
        print("    clean   - cleanup <Number of agents> after they all stopped.")

    def Start(self, agents=0):
        dt = datetime.now()
        logsdir = 'logs/' + dt.strftime('%Y_%m_%d__%H_%M_%S/')
        os.makedirs(logsdir)
        for agent in range(agents):
            agentstr = str(agent)
            configdir = 'config' + agentstr + '/'
            if not os.path.exists(configdir):
                os.makedirs(configdir)
            agentdir = logsdir + str(agent) + '/'
            os.makedirs(agentdir)
            agent_compose_file = configdir + 'swarm-compose.yml'
            agent_config_file = configdir + 'swarm.json'
            alatelogfd = open(agentdir + 'alate.log', 'a')
            sitllogfd = open(agentdir + 'sitl.log', 'a')
            sitl_container_name = "alate" + agentstr + "_sitl"
            mav_network_name = 'alate' + agentstr + '_mav'
            #agent_communication = 'communication' + agentstr
            #for node in self._composer['services']:
            #    self._composer['services'][node]['volumes'][0] = agent_communication +':/tmp'
            self._composer['networks']['mav']['external']['name'] = mav_network_name
            self._composer['services']['hlc']['volumes'][1] = "${PWD}/" + configdir + ":/home/nemala/alate/config:rw"
            with open(agent_compose_file, 'w') as file:
                yaml.dump(self._composer, file)

            self._configuration['autopilot']['master'] = "tcp:" + sitl_container_name + ":14552"
            with open(agent_config_file, 'w') as file:
                json.dump(self._configuration, file)

            # -------------------------------------- Setup ------------------------------------------------
            command_line = 'docker network create ' + mav_network_name
            #print(command_line)
            subprocess.Popen(shlex.split(command_line), stderr=sitllogfd, stdout=sitllogfd)
            time.sleep(1)
            # -------------------------------------- SITL ------------------------------------------------- 
            command_line = 'docker run --rm --network ' + mav_network_name + ' --name ' + sitl_container_name + \
                            ' -it davidovrat/arducopter-sitl --no-rebuild --out=tcpin:0.0.0.0:14552'
            #print(command_line)
            #print('--------------------------------')
            #print(shlex.split(command_line))
            subprocess.Popen(shlex.split(command_line), stderr=sitllogfd, stdout=sitllogfd)
            time.sleep(1)

            # -------------------------------------- ALATE ------------------------------------------------- 
            command_line = 'docker-compose -p alate' + agentstr + ' -f ' + agent_compose_file + ' up'
            #print(command_line)
            #print('--------------------------------')
            #print(shlex.split(command_line))
            subprocess.Popen(shlex.split(command_line), stderr=alatelogfd, stdout=alatelogfd)
            time.sleep(1)
            #print('=======================================================================================')


    def Plugin(self, agents=0):
        self._composer['services']['bma']['command'][0] = list(self._configuration['behaviors'].keys())[0]
        self.Start(agents)

    def Stop(self, agents=0):
        commands={}
        commands['stop_containers'] = 'docker stop -t 2'
        for agent in range(agents):
            agentstr = str(agent)
            commands['stop_containers'] = commands['stop_containers'] + ' alate' + agentstr + '_' + 'sitl'
            for node in self._composer['services']:
                container = ' alate' + agentstr + '_' + node + '_1'
                commands['stop_containers'] = commands['stop_containers'] + container

        for command in commands:
            command_line = commands[command]
            print(command_line)
            subprocess.Popen(shlex.split(command_line))

    def Clean(self, agents=0):
        commands={}
        commands['rm_containers'] = 'docker rm'
        commands['rm_volumes'] = 'docker volume rm'
        commands['rm_networks'] = 'docker network rm'
        commands['rm_files'] = 'rm -r '
        for agent in range(agents):
            agentstr = str(agent)
            for node in self._composer['services']:
                container = ' alate' + agentstr + '_' + node + '_1'
                commands['rm_containers'] = commands['rm_containers'] + container
            for volume in  ['communication']:
                commands['rm_volumes'] = commands['rm_volumes'] + ' alate' + agentstr + '_' + volume
            for network in  ['default', 'mav']:
                commands['rm_networks'] = commands['rm_networks'] + ' alate' + agentstr + '_' + network
            commands['rm_files'] = commands['rm_files'] + r' ./config' + agentstr

        for command in commands:
            command_line = commands[command]
            print(command_line)
            subprocess.Popen(shlex.split(command_line))
            time.sleep(1)



if __name__ == "__main__":
    success = True
    agents=0
    if (3 != len(sys.argv)):
        success = False

    if success:
        try:
            agents = int(sys.argv[2])
        except:
            success = False

    if success:
        try:
            with open(r'swarm.json', 'r') as file:
                configuration = json.load(file)
        except (RuntimeError, KeyError):
            success = False
            print(sys.argv[0] + " couldn't read configuration file - 'swarm.json'.")

    if success:
        try:
            with open(r'docker-compose.yml', 'r') as file:
                composer = yaml.load(file)
        except (RuntimeError, KeyError):
            success = False
            print(sys.argv[0] + " couldn't read compose file - 'docker-compose.yml'.") 

    if success:
        launcher = SwarmLauncher(sys.argv[1], configuration=configuration, composer=composer)
    else:
        launcher = SwarmLauncher()

    launcher.Run(agents)


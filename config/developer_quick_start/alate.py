#!/usr/bin/env python

###################################################################################
# File created by David Dovrat, Sep 3, 2019.
# For NeMALA Alate, under that project's license.
# The code in this file is provided "as is" and comes with no warranty whatsoever.
###################################################################################

import sys
import os
import shlex, subprocess
import time
import json
from datetime import datetime

if __name__ == "__main__":
    success = True
    if (2 != len(sys.argv)):
        print("Error: " + sys.argv[0] + ' requires a configuration file. See documentation.')
    else:
        try:
            configuration = json.load(open(sys.argv[1], 'r'))
        except (RuntimeError, KeyError):
            success = False
        if not success:
            print(sys.argv[0] + " couldn't read configuration file.")
        else:
            workdir = configuration['deploy']['working_directory']
            os.chdir(workdir)
            time.sleep(configuration['deploy']['sleep_for_sec'])
            dt = datetime.now()
            logsdir = 'logs/' + dt.strftime('%Y_%m_%d__%H_%M_%S/')
            os.makedirs(logsdir)
            os.chdir(logsdir)
            subprocess.Popen(shlex.split('nemala.py log ' + configuration['proxies']['uav']['logger']))
            os.chdir(workdir)
            for node in ['mc', 'hlc', 'osc']:
                os.chdir(logsdir)
                logfd = open(node + '.log', 'a')
                os.chdir(workdir)
                subprocess.Popen(shlex.split('./' + node + ' uav ' + sys.argv[1]), stderr=logfd, stdout=logfd)
            for node in configuration['behaviors']:
                os.chdir(logsdir)
                logfd = open(node + '.log', 'a')
                os.chdir(workdir)
                subprocess.Popen(shlex.split('./bma ' + node + ' ' + sys.argv[1]), stderr=logfd, stdout=logfd)

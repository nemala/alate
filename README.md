## Contents ##
* [Quick-Start Guide](doc/docker.md)
* [User Guide](doc/using.md)
* [Developer Guide](doc/start.md)
* [Components](doc/components.md)
    * [Mission Control](doc/components/mc.md)
    * [High Level Control](doc/components/hlc.md)
    * [Operator Station Client](doc/components/osc.md)
    * [Behavioral Modules](doc/components/bm.md)
* [Behaviors](doc/behaviors/index.md)

# Welcome to NeMALA Alate! #
NeMALA Alate is a framework for multi-agent UAV systems.
It was designed as a set of modules that can be moved around and altered according to anyone's unknown future needs,
with an emphasis on making it as easy as possible for future users to write new behaviors and incorporate new autopilots, sensors and payloads.

To demonstrate the framework's versatility and ease of use, we prepared some docker containers, check out how to use them in the [Quick-Start Guide](doc/docker.md).

To use the framework in your own projects, check out the [User Guide](doc/using.md).

To learn more about our thought process in creating Alate, read our paper, [Dovrat D. and Bruckstein A.M. "AntAlate - A Multi-Agent Autonomy Framework", *Frontiers in Robotics and AI* (2021): 264.](https://doi.org/10.3389/frobt.2021.719496).
This is also the paper you should cite if you use NeMALA Alate in an academic project.

To contribute to the Alate Framework, start by reading the [Developer Guide](doc/start.md).

We tested Alate on the following autopilot-companion computer configurations:
* [Arducopter](https://ardupilot.org/copter/) supported [autopilots](https://ardupilot.org/copter/docs/common-autopilots.html#common-autopilots), using [Dronekit](https://github.com/dronekit/dronekit-python) accompanied by a [Raspberry Pi](https://www.raspberrypi.org/).
* [SITL](http://ardupilot.org/dev/docs/sitl-simulator-software-in-the-loop.html) (simulation), using [Dronekit](https://github.com/dronekit/dronekit-python), on Linux machines.
* [Tello](https://github.com/dji-sdk/Tello-Python) accompanied by any computer running Linux with a wifi interface. 

Alate includes an example [Template Application](doc/using.md#template) with two behaviors:
* Video Recording.
* A swarming protocol described in
[Dovrat D. and  Bruckstein A.M. "On Gathering and Control of Unicycle A(ge)nts with Crude Sensing Capabilities", *IEEE Intelligent Systems* 6 (2017): 40-46.](http://doi.org/10.1109/mis.2017.4531231)

More information about these behaviors can be found [here](doc/behaviors/index.md).

Any questions? Suggestions? Want to join the swarm?
Feel free to do it the GitLab way (fork this repo, request access etc.) or [contact us](mailto:nemalate@gmail.com) directly! 

## Dependencies ##
| Dependency                                                            | Minimal Version   | License
| ------------                                                          | ---------------   | -------
| [Boost](https://www.boost.org/)                                       | 1.71.0            | [Boost Software License](https://www.boost.org/users/license.html)
| [Dronekit](https://github.com/dronekit/dronekit-python)               | 2.9.2             | [Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)
| [NeMALA Core](https://gitlab.com/nemala/core)                         | 1.0               | [NeMALA IIT](LICENSE)
| [NeMALA Tools](https://gitlab.com/nemala/tools)                       | 1.0               | [NeMALA IIT](LICENSE)
